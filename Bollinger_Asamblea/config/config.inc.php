<?php
// Constantes
// Clases de participantes
define ('A','Accionista');
define ('R','Delegado');
define ('V','Visitante');
//define ('',70500000);

// Estado de la asamblea
define ('ABIERTO',0);
define ('CERRADO',1);

// Ruta de las clases de Bollinger I (Asamblea)
define ('PATH_BOLLINGER1', './');

// Rutas del sistema
define ('PATH_ACTIONS','controlador/actions/');
define ('PATH_FORMS','controlador/forms/');
define ('PATH_PHRAME','../lib/phrame/');
define ('PATH_VISTAS','index.php?view=vistas/');
define ('NOMBRE_SISTEMA','Bollinger :: Sistema de informaci&oacute;n de accionistas');
define ('NOMBRE_EMPRESA', 'FONDO DE EMPLEADOS DE LA SOCIEDAD PORTUARIA REGIONAL DE BUENAVENTURA S.A.');

// MVC Phrame (Modelo - Vista - Controlador)
include(PATH_PHRAME . 'include.php');
include ('config/mappings.php');
include ('config/require.php');

// Pear
require_once ('../lib/PEAR.php');
require_once ('../lib/Pager.php');
require_once ('../lib/Util.php');
require_once ('../lib/DataObject.php');

// Fachadas
require_once ('../modelo/Fachada_accionistas.php');

$go_map = new BollingerMap;

// Configuracion para DataObject
$options = &PEAR::getStaticProperty('DB_DataObject','options');
$options = array(
    'database'         => 'mysql://bollingerfondo:f0nd0spbun@localhost/bollingerfondo',
    'schema_location'  => '../modelo',
    'class_location'   => '../modelo',
    'require_prefix'   => '',
    'class_prefix'     => '',
);

// Parametros de paginas
define ('LIMIT',20);     // Numero de filas por pagina
define ('MAXPAGES', 10); // Numero de paginas a mostrar por pagina

// Perfiles del sistema
define ('ACCIONISTAS', 1);
define ('DELEGADOS', 2);
define ('ASAMBLEA', 4);
define ('SESION', 8);
define ('TEMAS', 16);
define ('VOTAR', 32);
define ('REPORTES', 64);
define ('USUARIOS', 128);

$asamblea = new Asamblea;
$asamblea->get(0);
$_SESSION['asamblea'] = $asamblea->toArray();

$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto",
"Septiembre","Octubre","Noviembre","Diciembre");

$dias = array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "S&aacute;bado");

?>