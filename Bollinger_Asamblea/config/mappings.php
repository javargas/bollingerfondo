<?php
/* Clase mapping bollinger */

/**
 *	Bollinger mapping
 *
 * @author		John A. Vargas
 * @since		2004-03-07
 * @package		Controlador
 */
class BollingerMap extends MappingManager
{
	/**
 	 *	constructor function
 	 *	@return	vosid
 	 */
 	function BollingerMap()
 	{
 		$this->_SetOptions();
 	
 		$this->_AddForm('form', 'Forma');
 		$this->_AddForm('login', 'loginForm');
 		$this->_AddForm('ingresar', 'ingresarForm');
 		$this->_AddForm('votar', 'votarForm');
 		$this->_AddForm('seleccionar_tema', 'seleccionar_temaForm');
 		$this->_AddForm('crear_usuario', 'crear_usuarioForm');
 		$this->_AddForm('crear_accionista', 'crear_accionistaForm');
 		$this->_AddForm('modificar_accionista', 'modificar_accionistaForm');
 		$this->_AddForm('delegar', 'delegarForm');
		
		// ACCIONES
			/*** Eliminar tema ***/ 
 		$this->_AddMapping('eliminar_tema', 'eliminar_tema',
		                   'index.php', 'form');
 		$this->_AddForward('eliminar_tema', 'lista',
						   'phrame.php?action=listar_temas');
						   
			/*** Generacion de carnet ***/ 
 		$this->_AddMapping('quitar_registro', 'quitar_registro',
		                   'index.php', 'form');
 		$this->_AddForward('quitar_registro', 'ver_accionista',
						   'phrame.php?action=ver_accionista');
						   
			/*** Generacion de carnet ***/ 
 		$this->_AddMapping('generar_carnet', 'generar_carnet',
		                   'index.php', 'form');
 		$this->_AddForward('generar_carnet', 'imprimir_carnet',
						   'index.php?view=vistas/sesion/imprimir_carnet.php');
 		$this->_AddForward('generar_carnet', 'error_carnet',
						   'index.php?view=vistas/sesion/error_carnet.php');
		
 		$this->_AddMapping('eliminar_usuario', 'eliminar_usuario',
		                   'index.php?view=vistas/admin.html&modulo=usuarios&dialogo=buscar_usuarios&', 'form');
 		$this->_AddForward('eliminar_usuario', 'listar',
						   'index.php?view=vistas/admin.html&dialogo=buscar_usuarios');
						   
 		$this->_AddMapping('mostrar_pastel', 'mostrar_pastel',
		                   'index.php?view=vistas/index.html', 'form');
 		$this->_AddForward('mostrar_pastel', 'ver_sesion',
						   'index.php?view=vistas/admin.html');
						   
 		$this->_AddMapping('mostrar_sesion', 'mostrar_sesion',
		                   'index.php?view=vistas/index.html', 'form');
 		$this->_AddForward('mostrar_sesion', 'ver_sesion',
						   'index.php?view=vistas/admin.html');
						   
 		$this->_AddMapping('ver_representados', 'ver_representados',
		                   'index.php?view=vistas/index.html', 'form');
 		$this->_AddForward('ver_representados', 'lista',
						   'index.php?view=vistas/admin.html&dialogo=lista');
						   
 		$this->_AddMapping('imprimir_representados', 'imprimir_representados',
		                   'index.php?view=vistas/index.html', 'form');
 		$this->_AddForward('imprimir_representados', 'lista',
						   'index.php?view=vistas/dialogos/lista_imprimir.php');
						   
 		$this->_AddMapping('Delegar', 'delegar_acciones',
		                   'index.php?view=vistas/admin.html&dialogo=mostrar_delegacion', 'delegar', 1);
 		$this->_AddForward('Delegar', 'resultado',
						   'phrame.php?action=ver_representados');
						   
 		$this->_AddMapping('buscar_accionistas', 'buscar_accionistas',
		                   'index.php?view=vistas/index.html', 'form');
 		$this->_AddForward('buscar_accionistas', 'accionista',
						   'index.php?view=vistas/buscar_accionistas.html');
 		$this->_AddForward('buscar_accionistas', 'representante',
						   'index.php?view=vistas/buscar_representantes.html');
						   
 		$this->_AddMapping('ingresar_sistema', 'ingresar_sistema',
		                   'index.php?view=vistas/login.html', 'login',1);
 		$this->_AddForward('ingresar_sistema', 'listar',
						   'index.php?view=vistas/admin.html');
						   
 		$this->_AddMapping('salir_sistema', 'salir_sistema',
		                   'index.php?view=vistas/login.html', 'form');
 		$this->_AddForward('salir_sistema', 'salir',
						   'index.php?view=vistas/salir.html',1);
 		$this->_AddForward('salir_sistema', 'login',
						   'index.php?view=vistas/login.html',1);
		
			/** Cargar archivo de accionistas **/			
 		$this->_AddMapping('cargar_archivo_accionistas', 'cargar_archivo_accionistas',
		                   'index.php', 'form');
 		$this->_AddForward('cargar_archivo_accionistas', 'cargar',
						   'index.php?view=vistas/admin.html&modulo=accionistas&dialogo=cargar_accionistas');
						   
			/** Buscar accionistas, representantes **/   
 		$this->_AddMapping('buscar', 'buscar',
		                   'index.php?view=vistas/index.html', 'form');
 		$this->_AddForward('buscar', 'listar',
						   PATH_VISTAS . 'admin.html&dialogo=lista');
 		$this->_AddForward('buscar', 'buscar',
						   PATH_VISTAS . 'admin.html&dialogo=buscar');
 		$this->_AddForward('buscar', 'listar_registro',
						   PATH_VISTAS . 'admin.html&modulo=asamblea&dialogo=lista');
						   
			/** Ver detalle del accionista **/			   
 		$this->_AddMapping('ver_accionista', 'mostrar_accionistas',
		                   'index.php?view=vistas/admin.html&dialogo=lista_accionista.html', 'form');
 		$this->_AddForward('ver_accionista', 'detalle',
						   PATH_VISTAS . 'admin.html&dialogo=detalle_accionista');
						   
			/** Actualizar un accionista **/			
 		$this->_AddMapping('modificar_accionista', 'modificar_accionistas',
		                   'index.php?view=vistas/admin.html&dialogo=detalle_accionista', 'modificar_accionista');
 		$this->_AddForward('modificar_accionista', 'detalle',
						   PATH_VISTAS . 'admin.html&dialogo=detalle_accionista');
						   
 		$this->_AddMapping('mostrar_delegacion', 'mostrar_delegacion',
		                   'index.php?view=vistas/admin.html&dialogo=nuevo_accionista', 'form');
 		$this->_AddForward('mostrar_delegacion', 'delegar',
						   'index.php?view=vistas/admin.html&dialogo=mostrar_delegacion');
						   
 		$this->_AddMapping('adicionar_accionista', 'adicionar_accionista',
		                   'index.php?view=vistas/admin.html&dialogo=nuevo_accionista', 'crear_accionista', 1);
 		$this->_AddForward('adicionar_accionista', 'listar',
						   'index.php?view=vistas/admin.html&modulo=accionistas&dialogo=buscar&clase=A');
						   
 		$this->_AddMapping('mostrar_asamblea', 'mostrar_asamblea',
		                   'index.php?view=vistas/admin.html&dialogo=nuevo_accionista', 'form');
 		$this->_AddForward('mostrar_asamblea', 'detalle',
						   'index.php?view=vistas/admin.html');
						   
 		$this->_AddMapping('adicionar_representante', 'adicionar_representante',
		                   'index.php?view=vistas/admin.html&dialogo=nuevo_representante', 'crear_accionista',1);
 		$this->_AddForward('adicionar_representante', 'mostrar_delegacion',
						   'phrame.php?action=mostrar_delegacion');
		
				/** Registrar ingreso a una sesion **/				   
 		$this->_AddMapping('registrar_ingreso', 'registrar_ingreso',
		                   'index.php?view=vistas/admin.html&dialogo=registrar_ingreso', 'ingresar', 1);
 		$this->_AddForward('registrar_ingreso', 'exito',
						   'index.php?view=vistas/admin.html&dialogo=registrar_ingreso');
		
				/** Registrar salida de una sesion **/				   
 		$this->_AddMapping('registrar_salida', 'registrar_salida',
		                   'index.php?view=vistas/admin.html&dialogo=registrar_salida', 'ingresar',1);
 		$this->_AddForward('registrar_salida', 'exito',
						   'index.php?view=vistas/admin.html&dialogo=registrar_salida');
						   
				/** Mostrar listado de temas **/				   
 		$this->_AddMapping('listar_temas', 'listar_temas',
		                   'index.php?view=vistas/admin.html', 'form');
 		$this->_AddForward('listar_temas', 'lista',
						   'index.php?view=vistas/admin.html&modulo=temas&dialogo=lista_temas');
						   
				/** Creacion de un nuevo tema **/				   
 		$this->_AddMapping('crear_tema', 'crear_tema',
		                   'index.php?view=vistas/admin.html&dialogo=crear_tema', 'form');
 		$this->_AddForward('crear_tema', 'exito',
						   'phrame.php?action=listar_temas');
						   
				/** Crear opciones para el tema **/				   
 		$this->_AddMapping('crear_opciones', 'crear_opciones',
		                   'index.php?view=vistas/crear_opciones.html', 'form');
 		$this->_AddForward('crear_opciones', 'exito',
						   'index.php?view=vistas/temas/crear_opciones.html');
						   
				/** Inicia la votaci�n pidiendo selecci�n de un tema **/				   
 		$this->_AddMapping('seleccionar_temas', 'seleccionar_temas',
		                   'index.php?view=vistas/admin.html', 'form');
 		$this->_AddForward('seleccionar_temas', 'seleccionar',
						   'index.php?view=vistas/admin.html&dialogo=seleccionar_tema');
						   
				/** Cierra una votaci�n **/				   
 		$this->_AddMapping('cerrar_votacion', 'cerrar_votacion',
		                   'index.php?view=vistas/admin.html', 'form');
 		$this->_AddForward('cerrar_votacion', 'seleccionar',
						   'phrame.php?action=mostrar_temas');
						   
				/** Crear opciones para el tema **/				   
 		$this->_AddMapping('mostrar_temas', 'mostrar_temas',
		                   'index.php?view=vistas/admin.html', 'form');
 		$this->_AddForward('mostrar_temas', 'seleccionar',
						   'index.php?view=vistas/admin.html&dialogo=seleccionar_tema');
 		$this->_AddForward('mostrar_temas', 'pedir_codigo',
						   'index.php?view=vistas/admin.html&dialogo=ingresar_codigo');
						   
				/** Crear opciones para el tema **/				   
 		$this->_AddMapping('seleccionar_tema', 'seleccionar_temas',
		                   'phrame.php?action=mostrar_temas', 'seleccionar_tema', 1);
 		$this->_AddForward('seleccionar_tema', 'pedir_codigo',
						   'index.php?view=vistas/admin.html&dialogo=ingresar_codigo');
 		$this->_AddForward('seleccionar_tema', 'imprimir_resultados',
						   'index.php?view=vistas/dialogos/imprimir_resultados.php');
 		$this->_AddForward('seleccionar_tema', 'imprimir_resumen',
						   'index.php?view=vistas/dialogos/imprimir_resumen.php');
		
				/** Registrar ingreso a una sesion **/				   
 		$this->_AddMapping('ingresar_codigo', 'ingresar_codigo',
		                   'index.php?view=vistas/admin.html&dialogo=ingresar_codigo', 'votar', 1);
 		$this->_AddForward('ingresar_codigo', 'mostrar_opciones',
						   'index.php?view=vistas/admin.html&dialogo=mostrar_opciones');
		
				/** Ver opciones **/				   
 		$this->_AddMapping('ver_opciones', 'ver_opciones',
		                   'index.php?view=vistas/admin.html&dialogo=lista_temas', 'form');
 		$this->_AddForward('ver_opciones', 'lista_ver',
						   'index.php?view=vistas/dialogos/lista_opciones.html');
 		$this->_AddForward('ver_opciones', 'lista',
						   'index.php?view=vistas/admin.html&dialogo=lista_opciones');
		
				/** Ver opciones **/				   
 		$this->_AddMapping('ver_resultados', 'ver_resultados',
		                   'index.php?view=vistas/index.html', 'form');
 		$this->_AddForward('ver_resultados', 'ver_sesion',
						   'index.php?view=vistas/admin.html');
		
				/** Ver opciones **/				   
 		$this->_AddMapping('votar', 'votar',
		                   'index.php?view=vistas/admin.html&dialogo=mostrar_opciones', 'form');
 		$this->_AddForward('votar', 'ingresar_codigo',
						   'index.php?view=vistas/admin.html&dialogo=ingresar_codigo');
		
				/** Ver opciones **/				   
 		$this->_AddMapping('entrar_reportes', 'entrar_reportes',
		                   'index.php?view=vistas/admin.html&dialogo=mostrar_opciones', 'form');
 		$this->_AddForward('entrar_reportes', 'login',
						   'index.php?view=vistas/admin.html&modulo=reportes&dialogo=login');
 		$this->_AddForward('entrar_reportes', 'buscar',
						   'index.php?view=vistas/admin.html&modulo=reportes&dialogo=buscar_reportes');
		
				/** Ver opciones **/				   
 		$this->_AddMapping('ver_reporte', 'ver_reporte',
		                   'index.php?view=vistas/admin.html&dialogo=mostrar_opciones', 'form');
 		$this->_AddForward('ver_reporte', 'ver',
						   'index.php?view=vistas/admin.html&modulo=reportes&dialogo=ver_reporte');
		
				/** Borrar accionistas **/				   
 		$this->_AddMapping('borrar_accionistas', 'borrar_accionistas',
		                   'index.php?view=vistas/admin.html&dialogo=listar', 'form');
 		$this->_AddForward('borrar_accionistas', 'buscar',
						   'index.php?view=vistas/admin.html&dialogo=buscar');
		
				/** Cerrar asamblea **/				   
 		$this->_AddMapping('cerrar_asamblea', 'cerrar_asamblea',
		                   'index.php?view=vistas/admin.html&modulo=asamblea', 'form');
		
				/** Crear asamblea **/				   
 		$this->_AddMapping('crear_asamblea', 'crear_asamblea',
		                   'index.php?view=vistas/admin.html&modulo=asamblea', 'form');
		
				/** Crear asamblea **/				   
 		$this->_AddMapping('cambiar_asamblea', 'cambiar_asamblea',
		                   'index.php?view=vistas/admin.html&modulo=asamblea', 'form');
		
				/** Crear Usuarios **/				   
 		$this->_AddMapping('crear_usuario', 'crear_usuario',
		                   'index.php?view=vistas/admin.html&modulo=usuarios&dialogo=crear_usuario', 'crear_usuario', 1);
 		$this->_AddForward('crear_usuario', 'modificar',
						   'phrame.php?action=mostrar_usuario');
		
				/** Crear Usuarios **/				   
 		$this->_AddMapping('mostrar_usuario', 'mostrar_usuario',
		                   'index.php?view=vistas/admin.html&modulo=usuarios&dialogo=crear_usuario', 'crear_usuario');
 		$this->_AddForward('mostrar_usuario', 'ver_usuario',
						   'index.php?view=vistas/admin.html&modulo=usuarios&dialogo=modificar_usuario');
		
				/** Crear Usuarios **/				   
 		$this->_AddMapping('buscar_usuarios', 'buscar_usuarios',
		                   'index.php?view=vistas/admin.html&modulo=usuarios&dialogo=crear_usuario', 'crear_usuario');
 		$this->_AddForward('buscar_usuarios', 'buscar',
						   'index.php?view=vistas/admin.html&modulo=usuarios&dialogo=buscar_usuarios');
		
				/** Crear Usuarios **/				   
 		$this->_AddMapping('modificar_usuario', 'modificar_usuario',
		                   'index.php?view=vistas/admin.html&modulo=usuarios&dialogo=modificar_usuario', 'crear_usuario');
 		$this->_AddForward('modificar_usuario', 'ver_usuario',
						   'index.php?view=vistas/admin.html&modulo=usuarios&dialogo=modificar_usuario');
		
				/** Crear Usuarios **/				   
 		$this->_AddMapping('ver_faltantes', 'ver_faltantes',
		                   'index.php?view=vistas/admin.html&dialogo=ingresar_codigo', 'crear_usuario');
 		$this->_AddForward('ver_faltantes', 'lista',
							'index.php?view=vistas/dialogos/lista_no_votos.html');
		
				/** Guardar descripcioon de opciones **/				   
 		$this->_AddMapping('describir_opcion', 'describir_opcion',
		                   'index.php?view=vistas/admin.html&dialogo=ingresar_codigo', 'crear_usuario');
 		$this->_AddForward('describir_opcion', 'lista',
						   'phrame.php?action=ver_opciones');
		
				/** Cancelar delegacion **/				   
 		$this->_AddMapping('Cancelar', 'cancelar_delegacion',
		                   'index.php?view=vistas/admin.html&dialogo=ingresar_codigo', 'form');
 		$this->_AddForward('Cancelar', 'lista',
						   'index.php?view=vistas/admin.html&modulo=representantes&dialogo=buscar&clase=R');
						  
				/** Reportes **/
 		$this->_AddMapping('reporte_asistentes', 'reporte_asistentes',
		                   'index.php', 'form');
 		$this->_AddMapping('reporte_poderes', 'reporte_poderes',
		                   'index.php', 'form');
 		$this->_AddMapping('reporte_presentes', 'reporte_presentes',
		                   'index.php', 'form');
 		$this->_AddMapping('reporte_nopresentes', 'reporte_nopresentes',
		                   'index.php', 'form');
	}
 }
?>