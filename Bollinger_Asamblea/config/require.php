<?
	/*** Modelo ***/
require_once ('../modelo/Accionista.php');
require_once ('../modelo/Representante.php');
require_once ('../modelo/Asistencia.php');
require_once ('../modelo/Asamblea.php');
require_once ('../modelo/Asistencia.php');
require_once ('../modelo/Hsesion.php');
require_once ('../modelo/Sesion.php');
require_once ('../modelo/Parametro.php');
require_once ('../modelo/Tema.php');
require_once ('../modelo/Temadet.php');
require_once ('../modelo/Usuarioasam.php');
require_once ('../modelo/Votacion.php');
require_once ('../modelo/Votantes.php');
require_once ('../modelo/Parametro.php');
require_once ('../modelo/Votantes.php');
require_once ('../modelo/Historico_asamblea.php');

	/*** Controlador ***/	
// Acciones

// Accionistas
require_once (PATH_ACTIONS . 'accionistas/cargar_archivo_accionistas.php');
require_once (PATH_ACTIONS . 'accionistas/mostrar_accionistas.php');
require_once (PATH_ACTIONS . 'accionistas/modificar_accionistas.php');
require_once (PATH_ACTIONS . 'accionistas/adicionar_accionistas.php');
require_once (PATH_ACTIONS . 'accionistas/adicionar_representante.php');
require_once (PATH_ACTIONS . 'accionistas/buscar_accionistas.php');
require_once (PATH_ACTIONS . 'accionistas/borrar_accionistas.php');
require_once (PATH_ACTIONS . 'accionistas/cancelar_delegacion.php');
require_once (PATH_ACTIONS . 'accionistas/ver_representados.php');
require_once (PATH_ACTIONS . 'accionistas/mostrar_delegacion.php');
require_once (PATH_ACTIONS . 'accionistas/delegar_acciones.php');
require_once (PATH_ACTIONS . 'accionistas/imprimir_representados.php');

// Sistema
require_once (PATH_ACTIONS . 'sistema/ingresar_sistema.php');
require_once (PATH_ACTIONS . 'sistema/salir_sistema.php');
require_once (PATH_ACTIONS . 'sistema/crear_usuario.php');
require_once (PATH_ACTIONS . 'sistema/mostrar_usuario.php');
require_once (PATH_ACTIONS . 'sistema/buscar_usuarios.php');
require_once (PATH_ACTIONS . 'sistema/modificar_usuario.php');
require_once (PATH_ACTIONS . 'sistema/eliminar_usuario.php');

// Tema
require_once (PATH_ACTIONS . 'tema/listar_temas.php');
require_once (PATH_ACTIONS . 'tema/crear_tema.php');
require_once (PATH_ACTIONS . 'tema/crear_opciones.php');
require_once (PATH_ACTIONS . 'tema/mostrar_temas.php');
require_once (PATH_ACTIONS . 'tema/ver_opciones.php');
require_once (PATH_ACTIONS . 'tema/seleccionar_tema.php');
require_once (PATH_ACTIONS . 'tema/describir_opcion.php');
require_once (PATH_ACTIONS . 'tema/eliminar_tema.php');

// Votacion
require_once (PATH_ACTIONS . 'votacion/votar.php');
require_once (PATH_ACTIONS . 'votacion/ver_resultados.php');
require_once (PATH_ACTIONS . 'votacion/cerrar_votacion.php');
require_once (PATH_ACTIONS . 'votacion/ver_faltantes.php');

// Sesion
require_once (PATH_ACTIONS . 'sesion/quitar_registro.php');
require_once (PATH_ACTIONS . 'sesion/mostrar_sesion.php');
require_once (PATH_ACTIONS . 'sesion/mostrar_pastel.php');
require_once (PATH_ACTIONS . 'sesion/registrar_ingreso.php');
require_once (PATH_ACTIONS . 'sesion/registrar_salida.php');
require_once (PATH_ACTIONS . 'sesion/generar_carnet.php');

// Asamblea
require_once (PATH_ACTIONS . 'asamblea/mostrar_asamblea.php');
require_once (PATH_ACTIONS . 'asamblea/ingresar_codigo.php');
require_once (PATH_ACTIONS . 'asamblea/cerrar_asamblea.php');
require_once (PATH_ACTIONS . 'asamblea/crear_asamblea.php');
require_once (PATH_ACTIONS . 'asamblea/cambiar_asamblea.php');

require_once (PATH_ACTIONS . 'buscar.php');
require_once (PATH_ACTIONS . 'entrar_reportes.php');
require_once (PATH_ACTIONS . 'ver_reporte.php');

// Reportes
require_once (PATH_ACTIONS . 'reportes/reporte_asistentes.php');
require_once (PATH_ACTIONS . 'reportes/reportes_poderes.php');
require_once (PATH_ACTIONS . 'reportes/reporte_nopresentes.php');
require_once (PATH_ACTIONS . 'reportes/reporte_presentes.php');

// Formas
require_once (PATH_FORMS . 'form.php');
require_once (PATH_FORMS . 'loginForm.php');
require_once (PATH_FORMS . 'ingresar.php');
require_once (PATH_FORMS . 'votar.php');
require_once (PATH_FORMS . 'seleccionar_tema.php');
require_once (PATH_FORMS . 'crear_usuario.php');
require_once (PATH_FORMS . 'crear_accionista.php');
require_once (PATH_FORMS . 'modificar_accionista.php');
require_once (PATH_FORMS . 'delegar.php');

?>