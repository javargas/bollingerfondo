<?php
/*
 * Adicionar un accionistas
 *
 * @author	John A. Vargas
 */
class adicionar_accionista extends Action
{
	/**
	 * Adicionar un accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
		$accionista = new Accionista;
		
		Fachada_accionistas::adicionar_accionista($actionForm->get('documento'),
											   $actionForm->get('tipo_documento'),
											   $actionForm->get('nombre'),
											   $actionForm->get('direccion'),
											   $actionForm->get('telefono'),
											   $actionForm->get('fax'),
											   'A', // Clase accionista
											   $actionForm->get('cantidad'),
											   $actionForm->get('nombre_representante_legal'),
											   $actionForm->get('cedula_representante_legal'),
											   $actionForm->get('razon_social'),
											   $actionForm->get('nit'));	
	
		$_SESSION['mensaje'] = "Accionista adicionado con �xito";
	
		$actionForward = $actionMapping->get('listar');
		return $actionForward;
	}
}
?>
