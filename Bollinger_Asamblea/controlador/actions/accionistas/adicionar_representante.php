<?php
/*
 * Adicionar un representantes
 *
 * @author	John A. Vargas
 */
class adicionar_representante extends Action
{
	/**
	 * Adicionar un accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
		$id_representante = Fachada_accionistas::adicionar_accionista($actionForm->get('documento'),
											   $actionForm->get('tipo_documento'),
											   $actionForm->get('nombre'),
											   $actionForm->get('direccion'),
											   $actionForm->get('telefono'),
											   $actionForm->get('fax'),
											   'R', // Clase respresentante
											   null
											   );	
			Fachada_accionistas::modificar_accionistas($id_representante,
													   $actionForm->get('documento'),
													   $actionForm->get('nombre'),
													   $actionForm->get('direccion'),
													   $actionForm->get('telefono'),
													   $actionForm->get('fax'),
													   $actionForm->get('estado'),
													   $actionForm->get('codigo'),
													   $actionForm->get('cantidad'),
													   $actionForm->get('nombre_representante_legal'),
													   $actionForm->get('cedula_representante_legal'),
													   $actionForm->get('razon_social'),
													   $actionForm->get('nit'));	
			
		$actionForward = $actionMapping->get('mostrar_delegacion');
		$path = $actionForward->getPath();
		$actionForward->setPath($path . '&representante_id=' . $id_representante);
		
		$_SESSION['accion'] = "delegar";
		$_SESSION['representante_id'] = $id_representante;
		
		return $actionForward;
	}
}
?>
