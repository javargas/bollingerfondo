<?php
/*
 * Eliminar accionistas y/o quitar delegaciones
 *
 * @author	John A. Vargas <javargas@gmail.com>
 * @date Marzo 1 de 2004
 * @modified Marzo 12 de 2006
 * @package Controlador
 * @subpackage Acciones
 */
class borrar_accionistas extends Action
{
	/**
	 * Ejecutar la accion
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm)
	{
		$mensaje = "";
	
		// Eliminar accionistas seleccionados
		if ($actionForm->get('eliminar')) { 
			$eliminar = $actionForm->get('eliminar');
			foreach ($eliminar as $k => $v) {
				// Quitar delegaciones del accionista y/o representante
				$representacion = new Representante;
				$representacion->whereAdd("accionista_id = '$k'");
				$representacion->whereAdd("representante = '$k'", 'OR');
				$representacion->delete(DB_DATAOBJECT_WHEREADD_ONLY);
				
				// Borrar el accionista y/o representante
				$accionista = new Accionista;
				$accionista->get($k);
				$accionista->delete(); 
			}
			
			$mensaje = "Los accionistas se eliminaron con &eacute;xito!!\\n";
		}
		
		$actionForward = $actionMapping->get('buscar');
		
		// Quitar delegaciones seleccionadas
		if ($actionForm->get('quitar_delegacion')) {
		
			$eliminar = $actionForm->get('quitar_delegacion'); 
			$representante_id = $actionForm->get('representante_id'); 
		
			$sesion = new Hsesion;
			$sesion->selectAdd();
			$sesion->selectAdd('MAX(h_ingreso) AS ingreso, accionista_id, estado');
			$sesion->whereAdd("asamblea_id = '" . $_SESSION['asamblea']['asamblea_id'] . "'");
			$sesion->whereAdd("accionista_id = '{$representante_id}'");
			$sesion->groupBy('accionista_id');
			
			if (!$sesion->find(true) || $sesion->estado != 'P') { //echo $sesion->print_query(); exit;

				foreach ($eliminar as $k => $v) {
					// Se elimina de la tabla representante
					$representacion = new Representante;
					$representacion->whereAdd("asamblea_id = '{$_SESSION[asamblea][asamblea_id]}'");
					$representacion->whereAdd("representante = '{$v}'");
					$representacion->whereAdd("accionista_id = '{$k}'");
					$representacion->delete(DB_DATAOBJECT_WHEREADD_ONLY);// print_r($representacion); exit;
					$representante_id = $v;
					
					// Se elimina de la tabla asistencia (Para liberar el codigo de barras)
					
					$represent = new Representante;
					$represent->whereAdd("asamblea_id = '{$_SESSION[asamblea][asamblea_id]}'");
					$represent->whereAdd("representante = '{$v}'");
	
					if (!$represent->find(true)) {
						
						$asistencia = new Asistencia;
						$asistencia->whereAdd("asamblea_id = '{$_SESSION[asamblea][asamblea_id]}'");
						$asistencia->whereAdd("accionista_id = '{$v}'");
						$asistencia->delete(DB_DATAOBJECT_WHEREADD_ONLY);
					}
     
     
                         // Si el representante no representa nadie mas entonces borrarlo del sistema
                    $representante = new Representante;
                    $representante->representante = $representante_id;

                    if (!$representante->find(true)) {
                        $delegado = new Accionista;
                        $delegado->get($representante_id);
			if ($delegado->clase != 'A') {
                           $delegado->delete();
			}
                    }
                    // Fin de eliminar el representante que se queda sin delegados del sistema

				}
				$mensaje .= "Las delegaciones se quitaron con &eacute;xito!!";
				$actionForward->setPath('phrame.php?action=ver_representados&representante_id=' . $representante_id);
			} else {			
				$mensaje .= "El delegado esta presente en la sesion. No se pueden quitar delegaciones!!";
				$actionForward->setPath('phrame.php?action=ver_representados&representante_id=' . $representante_id);
			}
		} 
		
		$_SESSION['mensaje'] = $mensaje;
		
		return $actionForward;
	}
}
?>
