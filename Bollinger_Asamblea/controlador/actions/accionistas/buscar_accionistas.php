<?php
/* CLASE: buscar accionistas */

/*
 * buscar_accionistas
 *
 * @author	John A. Vargas
 * @package Controlador
 * @subpackage Acciones
 */
class buscar_accionistas extends Action
{
	/**
	 * Hace un listado de los accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm)
	{		
	    $from = ($actionForm->get('from')) ? $actionForm->get('from') : 0; 
		$clase = $actionForm->get('clase');
		$delegar = false;//($clase == 'A');  Comentado por requerimiento de mostrar asi esten delegados
		// Documento de pendientes de abril 11 de 2008
		$result = Fachada_accionistas::listar_accionistas($clase,
														  $from,
														  $actionForm->get('order'),
														  $actionForm->get('nombre'),
														  $delegar, // delegar
														  $actionForm->get('documento'),
														  $actionForm->get('codigo'),
                                                          'asamblea');
	
		$_SESSION['lista_nombres'] = $result['lista'];
		$_SESSION['data'] = $result['data'];
		
		if ($delegar && $actionForm->get('documento') != '') {
			$delegado = new Representante;
			$accionista = new Accionista;
			$delegado->joinAdd($accionista);
			$delegado->whereAdd("accionista.documento = '".$actionForm->get('documento')."'");
			$delegado->find(true);//print_r($delegado); exit;
			
			$rep = new Accionista;
			$rep->get($delegado->representante);
			$_SESSION['representado_por'] = $rep->nombre;
		}
		
		if ($actionForm->get('clase') == 'A')
			$actionForward = $actionMapping->get('accionista');
		else 
			$actionForward = $actionMapping->get('representante');
			
		return $actionForward;
	}
}
?>
