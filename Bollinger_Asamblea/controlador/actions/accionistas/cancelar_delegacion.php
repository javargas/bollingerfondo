<?php
/*
 * Eliminar accionistas
 *
 * @author	John A. Vargas <javargas@gmail.com>
 * @date Marzo 1 de 2004
 * @package Controlador
 * @subpackage Acciones
 */
class cancelar_delegacion extends Action
{
	/**
	 * Ejecutar la acci�n
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm)
	{
		$accionista = new Accionista;
		$accionista->get($actionForm->get('representante'));
		 
		// Se borra la informacion del representante (delegado)
		$representacion = new Representante;
		if (!$representacion->get('representante', $accionista->accionista_id) && $accionista->clase != 'A') {
			$accionista->delete();
				
			// borrar la informacion de la asistencia creada para el delegado
			$asistencia = new Asistencia; 
			$asistencia->get('accionista_id', $accionista->accionista_id);
			$asistencia->delete();
		}

		unset($_SESSION['accion']);
		unset($_SESSION['representante_id']);
		unset($_SESSION['representante']);
		
		return $actionMapping->get('lista');
	}
}
?>
