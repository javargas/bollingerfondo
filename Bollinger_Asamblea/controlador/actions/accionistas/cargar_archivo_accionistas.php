<?php
/*
 * Cargar archivo de accionistas
 *
 * @author	John A. Vargas
 * @date 2011-03-06
 */
class cargar_archivo_accionistas extends Action
{
	/**
	 * Adicionar un accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
        
        // Validar que no exista una asamblea abierta para hacer carga de asociados
        if (isset($_SESSION['asamblea'])) {
            $mensaje = "Para realizar carga de asociados debe estar la asamblea cerrada.";
        } else {

            $archivo = file($_FILES['datos_archivo']['tmp_name']);
            array_shift($archivo);
            $mensaje = "N&uacute;mero de registros en el archivo a cargar: " . count($archivo);
            //echo "<pre>"; print_r($archivo); echo "</pre>"; die();

            // Preparar el query para insercion de archivos.
            $query = "INSERT INTO accionista (nombre, direccion, telefono, email, documento, tipo_documento) VALUES ";

            // Verificar que el archivo tenga el formato correcto.
            $inserts = array();

            // Por cada linea del archivo, insertar un nuevo registro.
            foreach ($archivo as $k => $linea) {
                $campos = array_map('trim', explode(';', $linea));

                // Tamano de campos de la linea
                /*
                if (sizeof($campos) != 6) {
                  $mensaje .= "<li>El tama&ntilde;o de los campos no es correcto, deben haber 6 campos por registro. Linea $k </li>";
                }
                */
                //Ordenas los campos
                $camposOrd[0] = $campos[1] . ' ' . $campos[2];
                $camposOrd[1] = $campos[3];
                $camposOrd[2] = $campos[4];
                $camposOrd[3] = $campos[6];
                $camposOrd[4] = $campos[0];
                $camposOrd[5] = 'cc';

                $inserts[] = "('".implode("','", $camposOrd)."') ";      
            }

            $query .= implode(',', $inserts);
            //echo $query; 

            $accionista = new Accionista;

            // Limpiar la tabla de accionistas
            $accionista->whereAdd("1 = 1");
            $accionista->delete(DB_DATAOBJECT_WHEREADD_ONLY);

            $result = $accionista->_query($query); 
            //echo "<pre>"; print_r($result); echo "</pre>"; die();

            $query = "UPDATE accionista SET cantidad = 1, clase = 'A'";
            $accionista->_query($query);
        }

		// Mostrar mensaje de exito de la insercion de archivos
        if ($result = sizeof($archivo)) {		  
            $_SESSION['respuesta']['accion_mensaje'] = $mensaje . "<br>Los asociados fueron cargados con exito.";
        } else {
            $_SESSION['respuesta']['accion_mensaje'] = $mensaje . "<br>Ocurri&oacute; un error al cargar lo asociados, por favor verifique el archivo de carga. Gracias.";
        }    
	
		$actionForward = $actionMapping->get('cargar');
		return $actionForward;
	}
}
?>
