<?php
/*
 * Adicionar un accionistas
 *
 * @author	John A. Vargas
 */
class delegar_acciones extends Action
{
	/**
	 * Adicionar un accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) 
	{  //print_r($actionForm); exit();
		// Respuesta de la accion
		$_SESSION['respuesta'] = array();	
		
		$representante = new Accionista;
		$lista_accionistas = $actionForm->get('accionista');
		
		/* si se pide validacion de codigo (autorizacion) */
		/*
		if ($actionForm->get('accion') == 'validar_codigo') {
			$asistencia = new Asistencia; 
			$asistencia->get('accionista_id', $actionForm->get('accionista'));
			if ($asistencia->codbarra_id == $actionForm->get('codigo')) {		
				$accionista = new Accionista;
				$accionista->get($actionForm->get('accionista'));
				$representante->get($actionForm->get('representante'));
				
				// Validar que no exista representacion para el accionista
				$objRepres = new Representante;
				if ($objRepres->get('accionista_id', $accionista->accionista_id)) {
					
					// Respuesta de la accion
					$_SESSION['respuesta']['accion_mensaje'] = 'Error el accionista ya tiene una representaci&oacute;n creada.';
					$_SESSION['respuesta']['tipo_mensaje'] = 'error';
		
					$actionForward = $actionMapping->get('resultado');
					$actionForward->setPath('phrame.php?action=mostrar_delegacion&representante_id='.$actionForm->get('representante'));
					return $actionForward;
				} else {					
					$representacion = new Representante;
					$representacion->asamblea_id = $_SESSION['asamblea']['asamblea_id'];
					$representacion->accionista_id = $accionista->accionista_id;
					$representacion->representante = $representante->accionista_id;
					$representacion->cantidad = $accionista->cantidad;
					$representacion->insert();
				}
		
				$actionForward = $actionMapping->get('resultado');
				$path = $actionForward->getPath();
				$actionForward->setPath($path . '&representante_id=' . $actionForm->get('representante'));
				
				unset($_SESSION['accion']);
				unset($_SESSION['representante_id']);
				
				return $actionForward;
			} else {
				// Respuesta de la accion
				$_SESSION['respuesta']['accion_mensaje'] = 'Error: El c&oacute;digo no corresponde.';
				$_SESSION['respuesta']['tipo_mensaje'] = 'error';
				
				$actionForward = $actionMapping->get('resultado');
				$actionForward->setPath('phrame.php?action=mostrar_delegacion&representante_id='.$actionForm->get('representante'));
				return $actionForward;
			}
		}
		*/
		
		// Validar que solo pueda representar un asociado.
		$representacionAs = new Representante;
    $representacionAs->representante = $actionForm->get('representante');
    $representacionAs->find();
    
    if ($representacionAs->N > 0) {
          // Respuesta de la accion
          $_SESSION['respuesta']['accion_mensaje'] = 'Ya se esta representando un asociado. \n';
          $_SESSION['respuesta']['tipo_mensaje'] = 'error';
      
    } else {
  		
  		
  		// Crear representacion a cada accionista seleccionado.
  		if (is_array($lista_accionistas)) {
  			// Por si hay accionistas seleccionados varias veces
  			$lista_accionistas = array_unique($lista_accionistas);
  			
  			foreach ($lista_accionistas as $accionista_delegar) {
  				$accionista = new Accionista;
  				$accionista->get($accionista_delegar);
  				$representante->get($actionForm->get('representante'));
  								
  				// Validar que no exista una representacion para el accionista
  				$objRepres = new Representante;
  				if ($objRepres->get('accionista_id', $accionista->accionista_id)) {
  				
  					// Respuesta de la accion
  					$_SESSION['respuesta']['accion_mensaje'] = 'Error el accionista ya tiene una representaci&oacute;n creada. \n';
  					$_SESSION['respuesta']['tipo_mensaje'] = 'error';
  					
  					$actionForward = $actionMapping->get('resultado');
  					$actionForward->setPath('phrame.php?action=mostrar_delegacion&representante_id='.$actionForm->get('representante'));
  					return $actionForward;
  				} else {					
  					$representacion = new Representante;
  					$representacion->asamblea_id = $_SESSION['asamblea']['asamblea_id'];
  					$representacion->accionista_id = $accionista->accionista_id;
  					$representacion->representante = $representante->accionista_id;
  					$representacion->cantidad = $accionista->cantidad;
  					
  					$representacion->insert();
  			     //print_r($representacion);		
  					// Actualizar la cantidad del representante
  					/* $representante->cantidad += $accionista->cantidad;
  					$representante->update(); */
  				} 
          
          break;
  			}
  		}
  	}
	
		$actionForward = $actionMapping->get('resultado');
		$path = $actionForward->getPath();
		$actionForward->setPath($path . '&representante_id=' . $actionForm->get('representante'));
		
		unset($_SESSION['accion']);
		unset($_SESSION['representante_id']);
		
		return $actionForward;
	}
}
?>
