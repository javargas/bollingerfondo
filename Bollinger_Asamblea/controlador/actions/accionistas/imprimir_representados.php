<?php
/*
 * Lista los accionistas
 *
 * @author	John A. Vargas
 */
class imprimir_representados extends Action
{
	/**
	 * Hace un listado de los accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm)
	{			
	    $from = ($actionForm->get('from')) ? $actionForm->get('from') : 0; 
			
		$lista_representados = array();
		// si no es representado la participacion de �l
		$representacion = new Representante;
		$representacion->accionista_id = $actionForm->get('representante_id');
		if (!$representacion->find(true)) {
			$accionista = new Accionista;
			$accionista->get($actionForm->get('representante_id'));
			if ($accionista->clase != 'R')
				array_push($lista_representados, get_object_vars ($accionista));
		}
		$accionista = new Accionista;
		$representacion = new Representante;
		
		$representacion->representante = $actionForm->get('representante_id');
		$representacion->joinAdd($accionista);
		$representacion->find(); 
		
		if ($actionForm->get('order'))
			$representacion->orderBy($order);
		$representacion->find(); //print_r($representacion);
		
		while ($representacion->fetch()) {
			array_push($lista_representados, get_object_vars ($representacion));
		}
		

		$_SESSION['lista_imprimir'] = $lista_representados;
		$accionista = new Accionista;
		$accionista->get($actionForm->get('representante_id'));
		$_SESSION['representante_imprimir'] = $accionista->toArray();
		
		$actionForward = $actionMapping->get('lista');

		return $actionForward;
	}
}
?>
