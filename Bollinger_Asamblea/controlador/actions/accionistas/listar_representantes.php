<?php
/*
 * Lista los representantes
 *
 * @author	John A. Vargas
 */
class listar_representantes extends Action
{
	/**
	 * Hace un listado de los representantes
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
	    $from = ($actionForm->get('from')) ? $actionForm->get('from') : 0; 
			
		$result = Fachada_accionistas::listar_accionistas('R',$from);
		$_SESSION['modulo'] = "representantes";
		$_SESSION['lista_representantes'] = $result['lista'];
		$_SESSION['data'] = $result['data'];
	
		$actionForward = $actionMapping->get('index');
		return $actionForward;
	}
}
?>
