<?php
/*
 * Modifica un accionista
 *
 * @author	John A. Vargas
 * @date Marzo 1 de 2004
 */
class modificar_accionistas extends Action
{
	/**
	 * Modifica un accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) 
	{
		// Respuesta de la accion
		$_SESSION['respuesta'] = array();	
	
		if ($actionForm->validate()) {
			Fachada_accionistas::modificar_accionistas($actionForm->get('accionista_id'),
													   $actionForm->get('documento'),
													   $actionForm->get('nombre'),
													   $actionForm->get('direccion'),
													   $actionForm->get('telefono'),
													   $actionForm->get('fax'),
													   $actionForm->get('estado'),
													   $actionForm->get('codigo'),
													   $actionForm->get('cantidad'),
													   $actionForm->get('nombre_representante_legal'),
													   $actionForm->get('cedula_representante_legal'),
													   $actionForm->get('razon_social'),
													   $actionForm->get('nit'),
													   $actionForm->get('ciudad'),
													   $actionForm->get('empresa'),												   													   $actionForm->get('email'),
													   $actionForm->get('referencia'),												   													   $actionForm->get('folio'),
													   $actionForm->get('libro'),
													   $actionForm->get('representar'));	
			
			$accion_mensaje = "El accionista fue actualizado con &eacute;xito!!"; 
			$tipo_mensaje = "info";
		} else {
			$accion_mensaje = "Error: Ya existe un accionista con el c&oacute;digo ";
			$accion_mensaje .= "<b>".$actionForm->get('codigo')."</b>";
			$tipo_mensaje = "error";
		}

		// Ejecutar la accion de mmostrar accionista		
		$forward = mostrar_accionistas::perform($actionMapping, $actionForm);
		
		// Respuesta de la accion
		$_SESSION['respuesta']['accion_mensaje'] = $accion_mensaje;
		$_SESSION['respuesta']['tipo_mensaje'] = $tipo_mensaje;
		$_SESSION['respuesta']['ancho_ventana'] = 400;
		$_SESSION['respuesta']['alto_ventana'] = 320;

		return $forward;
	}
}
?>
