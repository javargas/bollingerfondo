<?php
/*
 * Muestra un accionista
 *
 * @author John A. Vargas
 * @date Marzo 1 de 2004
 */
class mostrar_accionistas extends Action
{
	/**
	 * Muestra un accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) 
	{  
		// Respuesta de la accion
		$_SESSION['respuesta'] = array();	
	
		$accionista = new Accionista;
		
		unset($_SESSION['representante_id']);
		unset($_SESSION['representante']);
		$accionista->get($actionForm->get('accionista_id'));

        if ($accionista->clase == 'A') {
            $representacion = new Representante;
            $representacion->accionista_id = $accionista->accionista_id;
            $representacion->asamblea_id = $_SESSION['asamblea']['asamblea_id']; 

            if ($representacion->find(true)) { 
                $representante = new Accionista;
                $representante->get($representacion->representante);
                $_SESSION['representante'] = $representante->toArray();
            } else
                unset($_SESSION['representante']);
        }

        // Los representados por el accionista
        $representacion = new Representante;
        $representacion->representante = $accionista->accionista_id;
        $_SESSION['representados'] = $representacion->find(true);

        // La participacion de los que representa
        $representacion = new Representante;
        $representacion->selectAdd();
        $representacion->representante = $accionista->accionista_id;
        $representacion->selectAdd("SUM(cantidad) as total_cantidad");
        $representacion->find(true); 
        $total_cantidad = $representacion->total_cantidad;

        $_SESSION['cantidad_representada'] = $total_cantidad;
	
        if (isset($_SESSION['asamblea']['asamblea_id']) && !empty($_SESSION['asamblea']['asamblea_id'])) {
            $asistencia = new Asistencia;		
            $asistencia->whereAdd("asamblea_id = '".$_SESSION['asamblea']['asamblea_id']."'");
            $asistencia->whereAdd("accionista_id = '{$accionista->accionista_id}'");
            $asistencia->find(true);        
            $_SESSION['codigo'] = $asistencia->codbarra_id;
        }
        
		$_SESSION['accionista'] = get_object_vars($accionista);
        //print "<pre>Accionista: "; print_r($_SESSION['accionista']); print "</pre>"; die();
		
		$actionForward = $actionMapping->get('detalle');
		return $actionForward;
	}
}
?>