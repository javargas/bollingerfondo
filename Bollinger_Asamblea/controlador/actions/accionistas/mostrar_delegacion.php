<?php
/*
 * Adicionar un accionistas
 *
 * @author	John A. Vargas
 */
class mostrar_delegacion extends Action
{
	/**
	 * Adicionar un accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm)
	{
		if ($actionForm->get('accionista_id')) {
			$accionista = new Accionista;
			$accionista->get($actionForm->get('accionista_id'));
			$_SESSION['accionista'] = $accionista->toArray();
			$_SESSION['accionista_id'] = $actionForm->get('accionista_id');
		}
		
		if ($actionForm->get('representante_id')) $representante_id = $actionForm->get('representante_id');
		elseif (isset($_SESSION['representante_id'])) $representante_id = $_SESSION['representante_id'];
		
		if ($representante_id) { echo "Obteniendo la info del accionista representante...";
			$accionista = new Accionista;
			$accionista->get($representante_id);
			$_SESSION['representante'] = $accionista->toArray();
			$_SESSION['representante_id'] = $representante_id;
		}
        
		$actionForward = $actionMapping->get('delegar');
		return $actionForward;
	}
}
?>