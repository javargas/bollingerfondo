<?php
/*
 * Lista los accionistas
 *
 * @author	John A. Vargas
 */
class ver_representados extends Action
{
	/**
	 * Hace un listado de los accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm)
	{			
	    $from = ($actionForm->get('from')) ? $actionForm->get('from') : 0; 
	
		$lista_representados = array();
		// si no es representado la participacion de �l
		$representacion = new Representante;
		$representacion->accionista_id = $actionForm->get('representante_id');
		if (!$representacion->find(true)) {
			$accionista = new Accionista;
			$accionista->get($actionForm->get('representante_id'));
			if ($accionista->clase != 'R')
				array_push($lista_representados, get_object_vars ($accionista));
		}
		
		$accionista = new Accionista;
		$representacion = new Representante;
		
		$representacion->representante = $actionForm->get('representante_id');
		$representacion->joinAdd($accionista);//print_r($representacion); exit();
		$representacion->find(); 
	    $data = DB_Pager::getData($from, LIMIT, $representacion->N, MAXPAGES);
		
		if ($actionForm->get('order'))
			$representacion->orderBy($order);
		$representacion->limit($from, LIMIT);
		$representacion->find(); //print_r($representacion);
				

		
		while ($representacion->fetch()) {
			array_push($lista_representados, get_object_vars ($representacion));
		}
					
		$representante = new Accionista;
		$asistencia = new Asistencia;
		$representante->get($actionForm->get('representante_id'));
		$asistencia->get('accionista_id', $representante->accionista_id);

		$_SESSION['lista'] = $lista_representados;
		$_SESSION['data'] = $data;
		$_SESSION['representados'] = true;
		$_SESSION['representante_id'] = $actionForm->get('representante_id');
		$_SESSION['nombre_representante'] = $representante->nombre;
		$_SESSION['codigo_representante'] = $asistencia->codbarra_id;
		//$_SESSION['accionista'] = $representante->toArray();
		
		$actionForward = $actionMapping->get('lista');

		return $actionForward;
	}
}
?>
