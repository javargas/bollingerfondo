<?php
/*
 * Cambiar asamblea
 *
 * @author	John A. Vargas
 * @date Marzo 1 de 2004
 */
class cambiar_asamblea extends Action
{
	/**
	 * Ejecutar acci�n
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) 
	{
		$asamblea = new Asamblea;
		$asamblea->get($_SESSION['asamblea']['asamblea_id']);
		$asamblea->nombre = $actionForm->get('nombre');
		$asamblea->update();
		header("Location: ".$actionMapping->getInput());
		
		return $actionForward;
	}
}
?>
