<?php
/*
 * Cerrar Asamblea
 *
 * @author John A. Vargas <javargas@gmail.com>
 * @date Marzo 29 de 2004
 */
class cerrar_asamblea extends Action
{
	/**
	 * Ejecutar accion
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) 
	{
		 $asamblea_tmp = new Asamblea;
		 $asamblea_tmp->find(true);		
		 
		 // Hacer un backup de la base de datos
         self::backup_tables('localhost','bollingerfondo','f0nd0spbun','bollingerfondo');
		
		/* Antes de empezar se saca un historico de
		 * las cantidades de acciones del agno inmediatamente anterior
		 */
 		 $historico_asamblea = new Historico_asamblea;
		 $agno = date("Y");
		 $query = "INSERT INTO historico_asamblea (accionista_id, cantidad, agno, asamblea_id) 
		 SELECT accionista_id, cantidad, '{$agno}', {$_SESSION[asamblea][asamblea_id]} FROM accionista WHERE clase = 'A'"; 
		 //echo $query; exit;
		 $historico_asamblea->query($query);
     
		
		$asamblea =  new Asamblea;
		
		// Marcar la asamblea en estado cerrado.
		if ($asamblea->get('estado',ABIERTO)) {
			$asamblea->estado = CERRADO;
		//	$asamblea->dividendos = $actionForm->get('dividendos');
		//	$asamblea->acta = $actionForm->get('acta');
			$asamblea->fecha = date("Y-m-d");
			$r = $asamblea->update();
			
			// Pasar la hsesion de la asamblea a sesion.
			$sesion = new Sesion;
			$query = "INSERT INTO sesion SELECT * FROM hsesion where asamblea_id = '".$_SESSION['asamblea']['asamblea_id']."'";
			$sesion->query($query);
			
			// Se borra los registros de hsesion
			$sesion = new Hsesion;
			$sesion->whereAdd("asamblea_id = '{$asamblea->asamblea_id}'");
			$sesion->delete(DB_DATAOBJECT_WHEREADD_ONLY);
						
			// Se borra los registros de la asistencia			
			/*$asistencia = new Asistencia;
			$asistencia->whereAdd("asamblea_id = '{$asamblea->asamblea_id}'");
			$asistencia->delete(DB_DATAOBJECT_WHEREADD_ONLY);*/
			
			// Se eliminan los temas de la asamblea
			/*$tema = new Temadet;
			$tema->whereAdd("asamblea_id = '{$asamblea->asamblea_id}'");
			$tema->delete(DB_DATAOBJECT_WHEREADD_ONLY);*/
			
			// Se limpia los datos de la votacion
			/*$votacion = new Votacion;
			$votacion->whereAdd("asamblea_id = '{$asamblea->asamblea_id}'");
			$votacion->delete(DB_DATAOBJECT_WHEREADD_ONLY);*/
			
			// Se limpian los datos de los votantes
			/*$votantes = new Votantes;
			$votantes->whereAdd("asamblea_id = '{$asamblea->asamblea_id}'");
			$votantes->delete(DB_DATAOBJECT_WHEREADD_ONLY);*/
			
			// Se limpian los datos de la delegacion
			/*$representante = new Representante;
			$representante->whereAdd("asamblea_id = '{$asamblea->asamblea_id}'");
			$representante->delete(DB_DATAOBJECT_WHEREADD_ONLY);*/
			
			// Se borran los delegados de la asamblea (Para fondo de empleados no existen delegados)
			/*$accionista = new Accionista;
			$accionista->whereAdd("clase = 'R'");
			$accionista->delete(DB_DATAOBJECT_WHEREADD_ONLY);*/
		}
			
        $_SESSION['asamblea'] = null;
        unset($_SESSION['asamblea']);	
        
		header("Location: ".$actionMapping->getInput());
        die();
	}        

    /* backup the db OR just a table */
    public static function backup_tables($host,$user,$pass,$name,$tables = '*')
    {
        $link = mysql_connect($host,$user,$pass);
        mysql_select_db($name,$link);

        //get all of the tables
        if($tables == '*')
        {
            $tables = array();
            $result = mysql_query('SHOW TABLES');
            while($row = mysql_fetch_row($result))
            {
                $tables[] = $row[0];
            }
        }
        else
        {
            $tables = is_array($tables) ? $tables : explode(',',$tables);
        }
        $return = '';
        //cycle through
        foreach($tables as $table)
        {
            $result = mysql_query('SELECT * FROM '.$table);
            $num_fields = mysql_num_fields($result);

            $return.= 'DROP TABLE '.$table.';';
            $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
            $return.= "\n\n".$row2[1].";\n\n";

            for ($i = 0; $i < $num_fields; $i++) 
            {
                while($row = mysql_fetch_row($result))
                {
                    $return.= 'INSERT INTO '.$table.' VALUES(';
                    for($j=0; $j<$num_fields; $j++) 
                    {
                        $row[$j] = addslashes($row[$j]);
                        $row[$j] = ereg_replace("\n","\\n",$row[$j]);
                        if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                        if ($j<($num_fields-1)) { $return.= ','; }
                    }
                    $return.= ");\n";
                }
            }
            $return.="\n\n\n";
        }

        //save file        
        if (!is_dir("../CopiasDB")) mkdir ("../CopiasDB");

        mkdir("../CopiasDB/bollingerfondo-".date("Ymd"));
        $ruta = realpath("../CopiasDB/bollingerfondo-".date("Ymd"));
        
        $handle = fopen($ruta . '/db-backup-'.date("Y-m-d").'.sql','w+');
        fwrite($handle,$return);
        fclose($handle);
    }
}
?>
