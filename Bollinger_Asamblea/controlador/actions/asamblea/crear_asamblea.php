<?php
/*
 * Crear Asamblea
 *
 * @author	John A. Vargas
 * @date Marzo 1 de 2004
 */
class crear_asamblea extends Action
{
	/**
	 * Ejecutar acci�n
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm)
	{
		if (!isset($_SESSION['asamblea']) || $_SESSION['asamblea'] == null) {
			$asamblea = new Asamblea;
			$asamblea->nombre = $actionForm->get('nombre');	
			$asamblea->estado = ABIERTO;
			$asamblea->insert();            
			$_SESSION['asamblea'] = $asamblea->toArray();			
		}			
        
		header("Location: ".$actionMapping->getInput());
		exit();
		//return $actionForward;
	}
}
?>
