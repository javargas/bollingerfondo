<?php
/*
 * Adicionar un accionistas
 *
 * @author	John A. Vargas
 */
class ingresar_codigo extends Action
{
	/**
	 * Adicionar un accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
		$asistencia = new Asistencia;
		$asistencia->get('codbarra_id', $actionForm->get('codigo'));
		
		
		$lista_accionista = new ArrayList();
			
		/** En el caso en que no sea representado **/	
		$representante = new Representante;	
		$accionista = new Accionista;
		if (!$representante->get('accionista_id',$asistencia->accionista_id)) {
			$accionista->get($asistencia->accionista_id);
			if ($accionista->clase != 'R')
				$lista_accionista->add($accionista->toArray());
		}
		
		/** Seleccionar a mis representados **/
		$representante = new Representante;
		$accionista = new Accionista;
		$representante->representante = $asistencia->accionista_id;
		$accionista->joinAdd($representante);
//		$accionista->selectAs();
		$accionista->find(); 
		while ($accionista->fetch())
			$lista_accionista->add($accionista->toArray());

		$rep = new Accionista;
		$rep->get($representante->representante);
		$_SESSION['representante'] = $rep->toArray();
		$_SESSION['lista_accionista'] = $lista_accionista->toArray();
		
		$actionForward = $actionMapping->get('mostrar_opciones');
		return $actionForward;
	}
}
?>
