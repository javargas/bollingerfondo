<?php
/*
 * Muestra los datos de la asamblea
 *
 * @author	John A. Vargas
 * @date Marzo 1 de 2004
 * @package Controlador
 * @subpackage Acciones
 */
class mostrar_asamblea extends Action
{
	/**
	 * Muestra la asamblea
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) { 
		$asamblea = new Asamblea;
		
		if($asamblea->get('estado', ABIERTO))			
			$_SESSION['asamblea'] = $asamblea->toArray();
			
		$_SESSION['modulo'] = "asamblea";
	
		$actionForward = $actionMapping->get('detalle');
        
		return $actionForward;
	}
}
?>
