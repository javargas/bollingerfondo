<?php
/* CLASE: buscar accionistas */

/*
 * buscar_accionistas
 *
 * @author	John A. Vargas
 * @package Controlador
 * @subpackage Acciones
 */
class buscar extends Action
{
  /**
   * Hace un listado de los accionistas
   *
   * @access	public
   * @param	ActionMapping	$actionMapping
   * @param	ActionForm		$actionForm
   * @return	ActionForward
   */
  function perform($actionMapping, $actionForm)
  {	
    $clase = $actionForm->get('clase');
    $from = $actionForm->get('from');
    $from = ($from) ? $from : 0; 
    $result = Fachada_accionistas::listar_accionistas($clase,
						      $from,
						      $actionForm->get('order'),
						      $actionForm->get('nombre'),
						      false, // delegar
						      $actionForm->get('documento'),
						      $actionForm->get('codigo'),
						      $actionForm->get('seccion')
						      );
														
    // Datos para pasar a la vista por medio de la sesion
    if ($clase == 'A')
      $_SESSION['modulo'] =  "accionistas";
    if ($clase == 'R')
      $_SESSION['modulo'] =  "representantes";
    $_SESSION['lista'] = $result['lista'];
		
    if ($result['data']) {
      $_SESSION['data'] = $result['data'];
			
      if ($actionForm->get('modulo') == "asamblea") 
		$actionForward = $actionMapping->get('listar_registro');
      else
		$actionForward = $actionMapping->get('listar');	
		
    } else {
      $actionForward = $actionMapping->get('buscar');
      $actionForward->setPath($actionForward->getPath() . "&clase=$clase");
      $_SESSION['mensaje'] = "No se encontraron registros!!";
    }
	
    return $actionForward;
  }
}
?>