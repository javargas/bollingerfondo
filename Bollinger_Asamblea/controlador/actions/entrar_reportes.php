<?php
/*
 * Adicionar un accionistas
 *
 * @author	John A. Vargas
 */
class entrar_reportes extends Action
{
	/**
	 * Adicionar un accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
		$usuario = new UsuarioAsam;
		
		$usuario->login = $actionForm->get('login');
		$usuario->passwd = md5($actionForm->get('passwd'));
		if($usuario->find(true)) {
			$tema = new Tema;
			$tema->find();
			
			$lista_temas = new ArrayList();
			while ($tema->fetch())
				$lista_temas->add($tema->toArray());
		
			$_SESSION['lista_temas'] = $lista_temas->toArray();
			$_SESSION['reportes'] = 1;
			$actionForward = $actionMapping->get('buscar');
		} else { 
			$_SESSION['mensaje'] = "Error!, Posiblemente escribio mal la contraseña";
			$actionForward = $actionMapping->get('login');
		}
		
		return $actionForward;
	}
}
?>
