<?php
/*
 * Muestra los datos de la asamblea
 *
 * @author	John A. Vargas
 * @date Marzo 1 de 2004
 * @package Controlador
 * @subpackage Acciones
 */
class mostrar_sesion extends Action
{
	/**
	 * Muestra la asamblea
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
		$contado = false;
		
		/* Num de accionistas y cantidad de acciones de presentes */
		$sesion = new Hsesion;
		$sesion->estado = 'P';
		$sesion->selectAdd();
		$sesion->selectAdd('MAX(h_ingreso) as ingreso, accionista_id, estado');
		$sesion->groupBy('accionista_id');
		$sesion->find();
		$num_presentes = 0; $acciones_presentes = 0;
		while ($sesion->fetch()) { 
			$representante = new Representante;
			$representante->accionista_id = $sesion->accionista_id;
			if (!$representante->find(true)) {
				$accionista = new Accionista;
				$accionista->get($sesion->accionista_id); 
				if ($accionista->clase == 'A') { 
					$acciones_presentes += $accionista->cantidad;
					$num_presentes++;
					$contado = true;
				}
			}
			// La participacion de los que representa
			$representacion = new Representante;
			$representacion->selectAdd();
			$representacion->representante = $sesion->accionista_id;
			$representacion->selectAdd("COUNT(accionista_id) AS num_accionistas, SUM(cantidad) as total_cantidad");
			$representacion->groupBy('representante');
			if ($representacion->find(true)) {
				$acciones_presentes += $representacion->total_cantidad;
				if (!$contado) $num_presentes++;//= $representacion->num_accionistas;
			}
			$contado = false;
		}
		
		/* Num de accionistas y cantidad de acciones de ausentes (Estan en asistencia pero no en sesion) */
		$sesion = new Hsesion;
		$asistencia = new Asistencia;
		$asistencia->joinAdd($sesion, 'LEFT');
		$asistencia->whereAdd("hsesion.accionista_id IS NULL");
		$asistencia->selectAs();
		$asistencia->find();
		$num_ausentes = 0; $acciones_ausentes = 0;
		while ($asistencia->fetch()) { 
			$representante = new Representante;
			$representante->accionista_id = $asistencia->accionista_id;
			if (!$representante->find(true)) {
				$accionista = new Accionista;
				$accionista->get($asistencia->accionista_id); 
				if ($accionista->clase == 'A') {
					$acciones_ausentes += $accionista->cantidad;
					$num_ausentes++;// echo $accionista->cantidad;
					$contado = true;// echo "accionista==>$accionista->accionista_id<br>";
				}
			}
			// La participacion de los que representa
			$representacion = new Representante;
			$representacion->selectAdd();
			$representacion->representante = $asistencia->accionista_id;
			$representacion->selectAdd("COUNT(accionista_id) AS num_accionistas, SUM(cantidad) as total_cantidad");
			$representacion->groupBy('representante');
			if ($representacion->find(true)) { //$representacion->print_query();
				$acciones_ausentes += $representacion->total_cantidad;
				if (!$contado) $num_ausentes++;  //= $representacion->num_accionistas;
			}
			$contado = false;
		}  //echo "Total: $acciones_ausentes"; exit();
		
		/* Num de accionistas y cantidad de acciones de ausentes (Estan hsesion pero han salido) */
		$sesion = new Hsesion;
		$sesion->groupBy('accionista_id');
		$sesion->selectAdd();
		$sesion->selectAdd('MAX(consecutivo), accionista_id');
		$sesion->find(); 
		$num_retirados = 0; $acciones_retirados = 0;
		while ($sesion->fetch()) { 
			$p_sesion = new Hsesion;
			$p_sesion->estado = 'P';
			$p_sesion->accionista_id = $sesion->accionista_id;
			$representante = new Representante;
			$representante->accionista_id = $sesion->accionista_id;
			$esta = $p_sesion->find(true);
			if (!$representante->find(true) && !$esta) {
				$accionista = new Accionista; 
				$accionista->get($sesion->accionista_id);
				$acciones_retirados += $accionista->cantidad;
				$num_retirados++;
				$contado = true;
			}
			// La participacion de los que representa
			if (!$esta) {
				$representacion = new Representante;
				$representacion->selectAdd();
				$representacion->representante = $sesion->accionista_id;
				$representacion->selectAdd("COUNT(accionista_id) AS num_accionistas, SUM(cantidad) as total_cantidad");
				$representacion->groupBy('representante');
				if ($representacion->find(true)) {
					$acciones_retirados += $representacion->total_cantidad;
					if (!$contado) $num_retirados++;//= $representacion->num_accionistas;
				}
				$contado = false;
			}
		}
			
		// Total de acciones de los accionistas no representados que no se registraron	
		$asistencia = new Asistencia;
		$accionista = new Accionista;
		$representante = new Representante;
		$accionista->clase = 'A';
		$accionista->joinAdd($asistencia, 'LEFT');
		$accionista->joinAdd($representante, 'LEFT');
		$accionista->whereAdd('asistencia.accionista_id IS NULL');
		$accionista->whereAdd('representante.accionista_id IS NULL');
		$accionista->selectAdd();
		$accionista->selectAdd('SUM(accionista.cantidad) AS total');
		$accionista->find(true);
		$cantidad = $accionista->total; 
		
		// Total de acciones de los representantes que no se registraron 
		$representante->joinAdd($asistencia, 'LEFT');
		$representante->whereAdd("asistencia.accionista_id IS NULL");
		$representante->selectAdd('SUM(representante.cantidad) AS total');
		$representante->groupBy('representante.asamblea_id');
		$representante->find(true); 
		$cantidad += $representante->total;

		// N�mero de accionistas no representados que no se registraron	
		$asistencia = new Asistencia;
		$accionista = new Accionista;
		$representante = new Representante;
		$accionista->clase = 'A';
		$accionista->joinAdd($asistencia, 'LEFT');
		$accionista->joinAdd($representante, 'LEFT');
		$accionista->whereAdd('asistencia.accionista_id IS NULL');
		//$accionista->whereAdd('representante.accionista_id IS NULL');
		$accionista->find(); //$accionista->print_query(); print_r($accionista);
		$num_accionistas = $accionista->N; 
		
		// N�mero de representantes que no se registraron 
		$representante = new Representante;
		$asistencia = new Asistencia;
		$representante->joinAdd($asistencia, 'LEFT');
		$representante->whereAdd("asistencia.accionista_id IS NULL");
		$representante->find();
		//$num_accionistas += $representante->N;
		
		$accionista = new Accionista;
		$accionista->clase = 'A';
	
		$num_accionistas = 	$accionista->count() - ($num_presentes + $num_retirados + $num_ausentes);
		
		$_SESSION['presentes'] = array('num_personas' => $num_presentes,
								       'cantidad' => $acciones_presentes);
									   
		$_SESSION['retirados'] = array('num_personas' => $num_retirados,
								       'cantidad' => $acciones_retirados);
									   
		$_SESSION['ausentes'] = array('num_personas' => $num_ausentes,
								       'cantidad' => $acciones_ausentes);
									   
		$_SESSION['no_registrados'] = array('num_personas' => $num_accionistas,
								       'cantidad' => $cantidad);
		
		$_SESSION['modulo'] = "sesion";/*
echo "Presentes: $num_presentes - $acciones_presentes<br>
			  Retirados: $num_retirados - $acciones_retirados<br>
			  Ausentes: $num_ausentes - $acciones_ausentes<br>
			  No registrado: $num_accionistas - $cantidad<br>";
		echo "<br>TOTAL: ".($acciones_presentes + $acciones_ausentes + $cantidad + $acciones_retirados);
		exit(); */
		$actionForward = $actionMapping->get('ver_sesion');
		return $actionForward;
	}
}
?>
