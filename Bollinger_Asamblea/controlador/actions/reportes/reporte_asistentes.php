<?php
/*
 * Reporte de asistentes
 *
 * @author	John A. Vargas <javargas@gmail.com>
 *          http://javargas.googlepages.com
 * @date 2004-03-03
 * @modified 2006-10-08
 */
class reporte_asistentes extends Action
{
	/**
	 * Ejecutar accion.
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm)
	{
		// Obtener los accionistas (Representados)
		$asistencia = new Asistencia;
		$accionista = new Accionista;
        $asamblea = new Asamblea;
        
        if (isset($_SESSION['asamblea']['asamblea_id']) && !empty($_SESSION['asamblea']['asamblea_id'])) {
            $asamblea_id = $_SESSION['asamblea']['asamblea_id'];
        }        
        
        if ($actionForm->get('asamblea_id') != '') {
            $asamblea_id = $actionForm->get('asamblea_id');
        }
        $asamblea->get($asamblea_id);
        $asistencia->asamblea_id = $asamblea_id;
        
		$accionista->clase = 'A';
		$accionista->joinAdd($asistencia);
		$accionista->orderBy('nombre');
		$accionista->find();
		
		$lista_accionistas = array();
		while ($accionista->fetch()) {
            $representacion = new Representante;
            $representacion->representante = $accionista->accionista_id;
            $representacion->asamblea_id = $asamblea_id;
            $representacion->selectAdd();
            $representacion->selectAdd('SUM(cantidad) AS total');
            $representacion->find(true); //print_r($representacion);

            $representado = new Representante;
            $representado->accionista_id = $accionista->accionista_id;
            if (!$representado->find(true))
                $accionista->cantidad += $representacion->total;
            else
                $accionista->cantidad = $representacion->total;
                
            $lista_accionistas[] = clone $accionista;
		} 
		
		// Obtener los representantes (Delegados)
		$delegado = new Accionista;
		$delegado->clase = 'R';
		$delegado->joinAdd($asistencia);
        $delegado->orderBy('nombre');
		$delegado->find();
		
		$lista_representantes = array();
		while ($delegado->fetch()) { //print_r($delegado);
            $representacion = new Representante;
            $representacion->representante = $delegado->accionista_id;
            $representacion->asamblea_id = $asamblea_id;
            $representacion->selectAdd();
            $representacion->selectAdd('SUM(cantidad) AS total');
            $representacion->find(true);// print_r($representacion);

            $delegado->cantidad = $representacion->total;
			$lista_representantes[] = clone $delegado;
		}
		//exit;
		// Incluir la vista que genera el documento.
		include('vistas/reportes/ver_reporte_asistencias.php');
	}
}
?>