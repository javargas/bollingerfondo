<?php
/*
 * Reporte de asistentes

 2.	Clasificar en dos grupos.  Un grupo para los accionistas que se registraron y nunca ingresaron a la asamblea, donde se detalle: el accionista, identidad, cantidad de acciones que representa y la participaci�n (porcentaje) de las acciones que representa, el total de estas acciones y total de la participaci�n

i.	Otro grupo para los accionista que ingresaron a la asamblea, pero en el momento de la votaci�n est�n retirados, donde se detalle el accionista, identidad, cantidad de acciones que representa y la participaci�n (porcentaje) de las acciones que representa, el total de estas acciones y total de la participaci�n

ii.	Al final debe totalizar, cuantos accionistas nunca ingresaron, cuantos accionistas est�n retirados, total acciones y total participaci�n de los dos grupos.

3.	Debe salir impreso al lado de la fecha la hora y el usuario que lo genero

 *
 * @author	John A. Vargas <javargas@gmail.com>
 * @date 2008-03-13
 * @Modified 2010-03-26
 */
class reporte_nopresentes extends Action
{
	/**
	 * Ejecutar accion.
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm)
	{
		$contado = false;
			
		/* Num de accionistas y cantidad de acciones de ausentes (Estan en asistencia pero no en sesion) */
		$sesion = new Hsesion;
		$asistencia = new Asistencia;
		$asistencia->joinAdd($sesion, 'LEFT');
		$asistencia->whereAdd("hsesion.accionista_id IS NULL");
		$asistencia->whereAdd("asistencia.asamblea_id = '" . $_SESSION['asamblea']['asamblea_id'] . "'");
		$asistencia->selectAs();
		$asistencia->find();
		$num_ausentes = 0; $acciones_ausentes = 0;
		$num_representados_ausentes = 0;
		
		$lista_nuncaingresados = array();
		
		while ($asistencia->fetch()) { 
			$representante = new Representante;
			$representante->accionista_id = $asistencia->accionista_id;
			if (!$representante->find(true)) {
				$accionista = new Accionista;
				$accionista->get($asistencia->accionista_id); 
				if ($accionista->clase == 'A') {
					$lista_nuncaingresados[$accionista->nombre] = $accionista->toArray();
					$contado = true;// echo "accionista==>$accionista->accionista_id<br>";
				}
			}
			
			// La participacion de los que representa
			
			$representacion = new Representante;
			$representacion->representante = $asistencia->accionista_id;
			$representacion->find();
			if (!$contado && $representacion->N) {
				$acciones = 0;
				while ($representacion->fetch()) { 
					$accionista_r = new Accionista;
					$accionista_r->get($representacion->accionista_id);
					$acciones += $accionista_r->cantidad;
				}
				
				$accionista = new Accionista;
				$accionista->get($asistencia->accionista_id); 
				$accionista->cantidad = $acciones;
			
				$lista_nuncaingresados[$accionista->nombre] = $accionista->toArray();
			}
			$contado = false;
		}  //echo "Total: $acciones_ausentes"; exit();
		
		/* Num de accionistas y cantidad de acciones de ausentes (Estan hsesion pero han salido) */
		$sesion = new Hsesion;
		$sesion->groupBy('accionista_id');
		$sesion->selectAdd();
		$sesion->selectAdd('MAX(consecutivo), accionista_id');
		$sesion->whereAdd("asamblea_id = '" . $_SESSION['asamblea']['asamblea_id'] . "'");
		$sesion->find(); 
		
		$lista_retirados = array();
		while ($sesion->fetch()) { 
			$contado = false;
			$acciones = 0;
			$p_sesion = new Hsesion;
			$p_sesion->estado = 'P';
			$p_sesion->accionista_id = $sesion->accionista_id;
			$representante = new Representante;
			$representante->accionista_id = $sesion->accionista_id;
			$esta = $p_sesion->find(true);
			
			// El accionista registrado en la sesion	
			$accionista = new Accionista;
			$accionista->get($sesion->accionista_id); 
						
			// La participacion de los que representa
			if (!$esta) {
				$representacion = new Representante;
				$representacion->representante = $sesion->accionista_id;
				$representacion->find();
				if ($representacion->N) {
					
					while ($representacion->fetch()) { 
						$accionista_r = new Accionista;
						$accionista_r->get($representacion->accionista_id);
						$acciones += $accionista_r->cantidad;
					}
				}
			}
			
			if (!$representante->find(true) && !$esta && $accionista->clase == 'A') {
				$acciones += $accionista->cantidad;
			}
			
			if (!$esta) {
				$accionista->cantidad = $acciones;
				$lista_retirados[$accionista->nombre] = $accionista->toArray();
			}
		}
		
        // Para que quede ordenado por nombre
        ksort($lista_nuncaingresados);
        ksort($lista_retirados);

		// Incluir la vista que genera el documento.
		include('vistas/reportes/ver_reporte_nopresentes.php');
	}
}
?>
