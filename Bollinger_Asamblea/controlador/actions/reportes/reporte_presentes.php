<?php
/*
 * Reporte de asistentes
 *
 * @author	John A. Vargas <javargas@gmail.com>
 * @date 2008-03-13
 */
class reporte_presentes extends Action
{
	/**
	 * Ejecutar accion.
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm)
	{
        global $_DB_DATAOBJECT;
		
		/* Num de accionistas y cantidad de acciones de presentes */
		$sesion = new Hsesion;
		$sesion->selectAdd();
		$sesion->selectAdd('MAX(h_ingreso) AS ingreso, accionista_id, estado');
		$sesion->whereAdd("asamblea_id = '" . $_SESSION['asamblea']['asamblea_id'] . "'");
		$sesion->whereAdd("estado = 'P'"); // Estado P: Presente
		$sesion->groupBy('accionista_id'); 
		$sesion->find(); //echo $sesion->print_query(); exit;

			$_DB_DATAOBJECT['LINKS'][$sesion->_database]['representante']['representante'] = 'accionista:accionista_id';
			$_DB_DATAOBJECT['LINKS'][$sesion->_database]['accionista']['accionista_id'] = 'representante:representante';
			unset($_DB_DATAOBJECT['LINKS'][$sesion->_database]['representante']['accionista_id']);
		
		$lista_presentes = array();		
		$lista_noorden = array();
		while ($sesion->fetch()) { 

			$representacion = new Representante;
			$accionista = new Accionista;
			
			$accionista->accionista_id = $sesion->accionista_id;
			$accionista->joinAdd($representacion, 'LEFT');
			$accionista->selectAs();
			$data_select = $accionista->_query['data_select'];
			$accionista->selectAdd("SUM(representante.cantidad) AS cantidad_representada");
			$accionista->groupBy(implode(",", array_keys($accionista->table())));
			//$accionista->orderBy('accionista.nombre');
			$accionista->find(true);
			
			$accionista->cantidad += $accionista->cantidad_representada;
      
			$lista_presentes[$accionista->nombre] = $accionista->toArray();
			//$lista_noorden[$accionista->nombre] = $accionista->toArray();

		} // echo "Acciones presentes {$acciones_presentes}"; print_r($lista_presentes);  		exit;
        ksort($lista_presentes);
		//arsort($lista_noorden);print_r($lista_noorden); exit;
		//$lista_presentes = array();
		//foreach ($lista_noorden as $v) $lista_presentes[] = $v;

		// Incluir la vista que genera el documento.
		include('vistas/reportes/ver_reporte_presentes.php');
	}
}
?>