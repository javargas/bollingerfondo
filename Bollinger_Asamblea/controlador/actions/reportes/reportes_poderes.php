<?php
/*
 * Reporte de poderes
 *
 * @author	John A. Vargas <jhonvarg@dotgeek.org>
 */
class reporte_poderes extends Action
{
	/**
	 * Ejecutar acci�n.
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm)
	{
		$representacion = new Representante;		
		$representacion->find();
        $asamblea = new Asamblea;
                
        if (isset($_SESSION['asamblea']['asamblea_id']) && !empty($_SESSION['asamblea']['asamblea_id'])) {
            $asamblea_id = $_SESSION['asamblea']['asamblea_id'];
        }        
        
        if ($actionForm->get('asamblea_id') != '') {
            $asamblea_id = $actionForm->get('asamblea_id');
        }
        $asamblea->get($asamblea_id);
		
		$lista_delegados = array();
		while ($representacion->fetch()) {			
		
			$afiliado = new Accionista;
			$afiliado->get($representacion->accionista_id);
			
			$asis = new Asistencia;
			if (!$asis->get('accionista_id', $representacion->representante))
				continue;
		
			if (!isset($lista_delegados[$representacion->representante])) {
				$delegado = new Accionista;
				$asistencia = new Asistencia;                
                $asistencia->asamblea_id = $asamblea_id;
				$delegado->joinAdd($asistencia);
				$delegado->accionista_id = $representacion->representante;
				$delegado->find(true);
				
				$lista_delegados[$representacion->representante] = array('delegado' => $delegado,
																   'afiliados' => array($afiliado));
																   
				$delegacion = new Representante;
                $delegacion->asamblea_id = $asamblea_id;                
                $delegacion->accionista_id = $representacion->representante;
				if (!$delegacion->find(true) && $delegado->clase == 'A')
					array_unshift($lista_delegados[$representacion->representante]['afiliados'], $delegado);
			} else {
				$lista_delegados[$representacion->representante]['afiliados'][] = clone $afiliado;
			}			
		}

		include('vistas/reportes/ver_reporte_poderes.php');
	}
}
?>
