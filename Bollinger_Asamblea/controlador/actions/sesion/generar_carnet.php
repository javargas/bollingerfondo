<?php
/*
 * Generar el carnet del accionista o representante
 *
 * @author John A. Vargas <javargas@gmail.com>
 * @date   2007-11-11 
 */
class generar_carnet extends Action
{
	/**
	 * Ejecutar la accion
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) 
	{
		// Al generar el carnet marcar en el atributo enrepresentacion
		// de un accionista que su carnet ya fue generado.
		// Asi la unica forma de regenerar el carnet es con un usuario con el perfil adecuado
		$accionista = new Accionista;
		$accionista->get($actionForm->get('accionista_id'));
		$_SESSION['user']['perfil'] *= 1;
		if ($accionista->enrepresentacion == 1) { 
			// Validar el perfil
			if ($_SESSION['usuario']['perfil'] & 16384) {
				$actionForward = $actionMapping->get('imprimir_carnet');
				$actionForward->setPath($actionForward->getPath() . '&codigo=' . $_REQUEST['codigo']);
			} else {
				$actionForward = $actionMapping->get('error_carnet');
				$actionForward->setPath($actionForward->getPath() . '&nombre_codigo=' . $accionista->nombre);
			}
			// si es primera ves que genera el carnet, marcar esto en la D.B.
		} else {		
			$accionista->enrepresentacion = 1;
			$accionista->update(); 
			$actionForward = $actionMapping->get('imprimir_carnet');
			$actionForward->setPath($actionForward->getPath() . '&codigo=' . $_REQUEST['codigo']);
		}
		
		return $actionForward;
	}
}
?>
