<?php
require_once ("../lib/jpgraph/jpgraph.php");
require_once ("../lib/jpgraph/jpgraph_bar.php");

/*
 * Muestra los datos de la asamblea
 *
 * @author	John A. Vargas
 * @date Marzo 1 de 2004
 * @package Controlador
 * @subpackage Acciones
 */
class mostrar_pastel extends Action
{
	/**
	 * Muestra la asamblea
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
		
		// Some data
		$data = $_SESSION['data']; 
	
		// Create the graph and setup the basic parameters 
		$graph = new Graph(580,270,'auto');	
		$graph->img->SetMargin(80,30,30,40);
		$graph->SetScale("textint");
		$graph->SetShadow();
		$graph->SetFrame(false); // No border around the graph
		
		// Add some grace to the top so that the scale doesn't
		// end exactly at the max value. 
		// Since we are using integer scale the gace gets intervalled
		// to adding integer values.
		// For example grace 10 to 100 will add 1 to max, 101-200 adds 2
		// and so on...
		$graph->yaxis->scale->SetGrace(30);
		$graph->yaxis->SetLabelFormatCallback('number_format');
		$graph->yaxis->setTitle("Num Asociados");
		
		// Setup X-axis labels
		$a = array('Asistentes', 'Asi. con poder', 'Registrados', 'Reg. con poder');
		
		$graph->xaxis->SetTickLabels($a);
		$graph->xaxis->SetFont(FF_FONT2, FS_NORMAL, 15);
		
		// Setup graph title ands fonts
		$graph->title->Set("Quorum");
									  
		// Create a bar pot
		//print_r($data); exit;
//		$data = array( 51055618, 55169235, 87540388, 19234759 );
   //       echo "<pre>"; print_r($data); echo "</pre>";
		foreach ($data as $k => $v) $data[$k] = str_replace( ',', '.', Util::participacion($v));
		
     //   echo "<pre>"; print_r($data); echo "</pre>"; die();
		$bplot = new BarPlot($data);
//		$bplot->SetFormat("");
		//$bplot->SetFillGradient(array("blue", "yellow", "green", "red"), "black", 1);        
        $bplot->SetFillGradient("#4B0082","white",GRAD_LEFT_REFLECTION);
		$bplot->SetWidth(0.5);
		$bplot->SetShadow();
		
		$graph->Add($bplot);
		// Setup the values that are displayed on top of each bar
		$bplot->value->Show();
		// Must use TTF fonts if we want text at an arbitrary angle
		$bplot->value->SetFont(FF_ARIAL,FS_BOLD, 15);
		$bplot->value->SetAngle(45);
		$bplot->value->SetFormat('%0.2f');
		// Black color for positive values and darkred for negative values
		$bplot->value->SetColor("black");
		
		// Finally stroke the graph
		$graph->Stroke();
	}
}
?>