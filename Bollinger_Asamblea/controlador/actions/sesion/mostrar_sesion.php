<?php
/*
 * Muestra los datos de la asamblea
 *
 * @date Marzo 1 de 2004
 * @package Controlador
 * @subpackage Acciones
 * @author	John A. Vargas <javargas@gmail.com>
 */
class mostrar_sesion extends Action
{
	/**
	 * Muestra la asamblea
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
		$contado = false;
		
		/* Num de accionistas y cantidad de acciones de presentes */
		$sesion = new Hsesion;
		$sesion->estado = 'P';
		$sesion->selectAdd();
		$sesion->selectAdd('MAX(h_ingreso) AS ingreso, accionista_id, estado');
		$sesion->whereAdd("asamblea_id = '" . $_SESSION['asamblea']['asamblea_id'] . "'");
		$sesion->groupBy('accionista_id');
		$sesion->find(); //echo $sesion->print_query(); exit;
		$num_presentes = 0; $acciones_presentes = 0;
		$num_representados_presentes = 0;
		
		while ($sesion->fetch()) { 
			// Si no es representado se cuentan el accionista
			$representante = new Representante;
			$representante->accionista_id = $sesion->accionista_id; 
			if (!$representante->find(true)) {
				$accionista = new Accionista;
				$accionista->get($sesion->accionista_id); //print_r($accionista);
				if ($accionista->clase == 'A' && $accionista->cantidad > 0) {
					$acciones_presentes += $accionista->cantidad;
					$num_presentes++;
					$contado = true;
				}
			}

			// La participacion de los que representa
			$representacion = new Representante;
			$representacion->representante = $sesion->accionista_id;
			$representacion->find();
			if (!$contado && $representacion->N) $num_presentes++;
			$num_representados_presentes += $representacion->N;

			$contado = false;
		} // echo "Acciones presentes {$acciones_presentes}"; exit;
		
		/* Num de accionistas y cantidad de acciones de ausentes (Estan en asistencia pero no en sesion) */
		$sesion = new Hsesion;
		$asistencia = new Asistencia;
		$asistencia->joinAdd($sesion, 'LEFT');
		$asistencia->whereAdd("hsesion.accionista_id IS NULL");
		$asistencia->whereAdd("asistencia.asamblea_id = '" . $_SESSION['asamblea']['asamblea_id'] . "'");
		$asistencia->selectAs();
		$asistencia->find();
		$num_ausentes = 0; $acciones_ausentes = 0;
		$num_representados_ausentes = 0;
		while ($asistencia->fetch()) { 
			$representante = new Representante;
			$representante->accionista_id = $asistencia->accionista_id;
			if (!$representante->find(true)) {
				$accionista = new Accionista;
				$accionista->get($asistencia->accionista_id); 
				if ($accionista->clase == 'A' && $accionista->cantidad > 0) {
					$acciones_ausentes += $accionista->cantidad;
					$num_ausentes++;// echo $accionista->cantidad;
					$contado = true;// echo "accionista==>$accionista->accionista_id<br>";
					//$num_representados_ausentes++;
				}
			}
			// La participacion de los que representa
			$representacion = new Representante;
			$representacion->representante = $asistencia->accionista_id;
			$representacion->find();
			if (!$contado && $representacion->N) $num_ausentes++;  
			$num_representados_ausentes += $representacion->N;
            
			$contado = false;  
		}  //echo "Total: $acciones_ausentes"; exit();
		
		/* Num de accionistas y cantidad de acciones de ausentes (Estan hsesion pero han salido) */
		$sesion = new Hsesion;
		$sesion->groupBy('accionista_id');
		$sesion->selectAdd();
		$sesion->selectAdd('MAX(consecutivo), accionista_id');
		$sesion->whereAdd("asamblea_id = '" . $_SESSION['asamblea']['asamblea_id'] . "'");
		$sesion->find(); 
		$num_retirados = 0; $acciones_retirados = 0;
		$num_representados_retirados = 0;
		
		while ($sesion->fetch()) { 
			$contado = false;
			$p_sesion = new Hsesion;
			$p_sesion->estado = 'P';
			$p_sesion->accionista_id = $sesion->accionista_id;
			$representante = new Representante;
			$representante->accionista_id = $sesion->accionista_id;
			$esta = $p_sesion->find(true); // Indica si esta presente
			
			// Si no esta presente y no esta siendo representado
			if (!$representante->find(true) && !$esta) {
				$accionista = new Accionista; 
				$accionista->get($sesion->accionista_id);
				
				// Se suman la cantidad del accionista no representado
				if ($accionista->clase == 'A' && $accionista->cantidad > 0) {
					$acciones_retirados += $accionista->cantidad;
					$num_retirados++;
					$num_representados_retirados++;
					$contado = true;
				}
			}
			
			// La participacion de los que representa            
			if (!$esta) {
				$representacion = new Representante;
				$representacion->representante = $sesion->accionista_id;
				$representacion->find();
				
				// Si no ha sido contado y representa algunos lo contamos
				if (!$contado && $representacion->N) $num_retirados++;
					$num_representados_retirados += $representacion->N;  
				
				while ($representacion->fetch()) { 
					$accionista_r = new Accionista;
					$accionista_r->get($representacion->accionista_id);
					$acciones_retirados += $accionista_r->cantidad;
				}
			}
		}
			
		// Total de acciones de los accionistas no representados que no se registraron	
		$accionista = new Accionista;
        $strQuery = "SELECT a.*, SUM(a.cantidad) AS total 
                     FROM accionista a
                     LEFT JOIN representante r 
                     ON r.accionista_id = a.accionista_id 
                     WHERE r.accionista_id IS NULL 
                     AND a.accionista_id NOT IN 
                     (SELECT accionista_id 
                      FROM asistencia 
                      WHERE asamblea_id  = '".$_SESSION['asamblea']['asamblea_id']."'
                     )
                     
                    ";
        $accionista->query($strQuery);
        $accionista->fetch();
		//$accionista->f(true); //print_r($accionista); exit;
		$num_accionistas = $accionista->total; 
        //echo "<pre>"; print_r($accionista); echo "</pre>"; die();
		 
		
        // Cantidad de representantes con poder        
		$query = "SELECT COUNT(r.accionista_id) num_representantes, COUNT(a.accionista_id) as num_representados 
                FROM accionista r
                INNER JOIN representante a 
                ON a.representante = r.accionista_id
                WHERE a.asamblea_id = '".$_SESSION['asamblea']['asamblea_id']."'";
        $accionista->query($query); //echo "<pre>"; print_r($accionista); echo "</pre>";
        $accionista->fetch();
        $num_accionistas_rep = $accionista->num_representantes; 
        
		//$num_accionistas = 	$accionista->count() - ($num_presentes + $num_retirados + $num_ausentes);
		
		$_SESSION['presentes'] = array('num_personas' => $num_presentes,
								       
									   'representados' => $num_representados_presentes);
									   
		$_SESSION['retirados'] = array('num_personas' => $num_retirados,
								       
									   'representados' => $num_representados_retirados);
									   
		$_SESSION['ausentes'] = array('num_personas' => $num_ausentes,
								       
									   'representados' => $num_representados_ausentes);
									   
		$_SESSION['no_registrados'] = array('num_personas' => $num_accionistas,
								       
									   'representados' => $num_accionistas);
									   
		$_SESSION['representantes'] = array('num_personas' => $num_accionistas_rep,
								       
									   'representados' => $num_accionistas_rep);
		
		$_SESSION['modulo'] = "sesion";
        /*
        echo "Presentes: $num_presentes - $acciones_presentes<br>
			  Retirados: $num_retirados - $acciones_retirados<br>
			  Ausentes: $num_ausentes - $acciones_ausentes<br>
			  No registrado: $num_accionistas - <br>";
		exit(); */
         
		$actionForward = $actionMapping->get('ver_sesion');
		return $actionForward;
	}
}
?>
