<?php
/*
 * Quita el codigo (Desregistra el accionista)
 *
 * @date 2007-11-18
 * @package Controlador
 * @subpackage Acciones
 * @author	John A. Vargas <javargas@gmail.com>
 */
class quitar_registro extends Action
{
	/**
	 * Muestra la asamblea
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
		$sesion = new Hsesion;
		$sesion->whereAdd("accionista_id = '".$actionForm->get('accionista_id')."'");
		$sesion->delete(DB_DATAOBJECT_WHEREADD_ONLY);
		
		$asistencia = new Asistencia;
		$asistencia->whereAdd("accionista_id = '".$actionForm->get('accionista_id')."'");
		$asistencia->delete(DB_DATAOBJECT_WHEREADD_ONLY);
		
		$forward = $actionMapping->get('ver_accionista');
		$forward->setPath($forward->getPath() . "&accionista_id=".$actionForm->get('accionista_id'));
	
		return $forward;
	}
}
?>
