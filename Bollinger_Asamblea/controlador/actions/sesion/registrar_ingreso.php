<?php
/*
 * Adicionar un accionistas
 *
 * @author	John A. Vargas
 */
class registrar_ingreso extends Action
{
	/**
	 * Adicionar un accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
		$asistencia = new Asistencia;		
        $asistencia->whereAdd("asamblea_id = '".$_SESSION['asamblea']['asamblea_id']."'");
        $asistencia->whereAdd("codbarra_id = '".$actionForm->get('codigo')."'");
        $asistencia->find(true);       

		$sesion = new Hsesion;
		$sesion->asamblea_id = $asistencia->asamblea_id;
		$sesion->accionista_id = $asistencia->accionista_id;
		$sesion->estado = 'P';
		if ($sesion->find(true)) {
			$_SESSION['mensaje_codigo'] = 'Lo siento el accionista o representante ya ha ingresado';
			$_SESSION['tipo_mensaje'] = 'error';
		} else {
			$asistencia->consecutivo++;
			$asistencia->update();
			$sesion = new Hsesion;
			$sesion->asamblea_id = $asistencia->asamblea_id;
			$sesion->accionista_id = $asistencia->accionista_id;
			$sesion->consecutivo = $asistencia->consecutivo;
			$sesion->f_ingreso = date("Y-m-d");
			$sesion->h_ingreso = date("h:i:s a");
			$sesion->estado = 'P';
			$sesion->insert();
			$_SESSION['mensaje_codigo'] = 'El ingreso se registr&oacute; con &eacute;xito';
			$_SESSION['tipo_mensaje'] = 'info';
		}	
		$actionForward = $actionMapping->get('exito');
		
		return $actionForward;
	}
}
?>
