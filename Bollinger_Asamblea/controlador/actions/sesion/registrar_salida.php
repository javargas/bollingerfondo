<?php
/*
 * Adicionar un accionistas
 *
 * @author	John A. Vargas
 */
class registrar_salida extends Action
{
	/**
	 * Adicionar un accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
		$asistencia = new Asistencia;		
        $asistencia->whereAdd("asamblea_id = '".$_SESSION['asamblea']['asamblea_id']."'");
        $asistencia->whereAdd("codbarra_id = '".$actionForm->get('codigo')."'");
        $asistencia->find(true);    

		$sesion = new Hsesion;
		$sesion->asamblea_id = $asistencia->asamblea_id;
		$sesion->accionista_id = $asistencia->accionista_id;
		$sesion->consecutivo = $asistencia->consecutivo; 
		if ($sesion->find(true)) {
			if ($sesion->estado == 'A') {
				$_SESSION['mensaje_codigo'] = 'Lo siento el accionista o representante ya ha salido';
			$_SESSION['tipo_mensaje'] = 'error';
			} else {
				$sesion->f_salida = date("Y-m-d");
				$sesion->h_salida = date("h:i:s a");
				$sesion->estado = 'A';
				$sesion->update();
				$_SESSION['mensaje_codigo'] = 'La salida se registr&oacute; con &eacute;xito';
				$_SESSION['tipo_mensaje'] = 'info';
			}
		} else {
			$_SESSION['mensaje_codigo'] = 'Lo siento el accionista o representante no ha ingresado';
			$_SESSION['tipo_mensaje'] = 'error';

		}
			
		$actionForward = $actionMapping->get('exito');
		
		return $actionForward;
	}
}
?>
