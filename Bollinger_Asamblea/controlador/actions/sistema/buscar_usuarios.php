<?php
/*
 * Buscar los usuarios del sistema
 *
 * @author	John A. Vargas
 */
class buscar_usuarios extends Action
{
	/**
	 * Busca usuarios del sistema
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm)
	{
		$usuario = new Usuarioasam;
		$login = $actionForm->get('login');
		$nombre = $actionForm->get('nombre');
		
		if ($login != '') 
			$usuario->whereAdd(" LOWER(login) LIKE LOWER('%$login%') ");
			
		if ($nombre != '') 
			$usuario->whereAdd(" LOWER(nombre) LIKE LOWER('%$nombre%') ");
			
	    $data = DB_Pager::getData($from, LIMIT, $usuario->count(), MAXPAGES);
		
		$usuario->orderBy('nombre');
		
		$usuario->limit($from, LIMIT);
		$usuario->find();
		$lista_usuarios = new ArrayList;
		while ($usuario->fetch())
			$lista_usuarios->add($usuario->toArray());
		
		$_SESSION['lista_usuarios'] = $lista_usuarios->toArray();
		$_SESSION['data'] = $data;
		
		$actionForward = $actionMapping->get('buscar');
		
		return $actionForward;
	}
}
?>
