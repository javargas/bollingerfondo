<?php
/*
 * Crear usuarios
 *
 * @author	John A. Vargas
 */
class crear_usuario extends Action
{
	/**
	 * Crea un nuevo usuario
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm)
	{
		$usuario = new Usuarioasam;
		$usuario->nombre = $actionForm->get('nombre');
		$usuario->login = $actionForm->get('login');
		$usuario->passwd = md5($actionForm->get('passwd'));		
		
		$perfiles = $actionForm->get('perfil');
		$usuario->perfil = array_sum($perfiles);
		$usuario->insert(); 
	
		$_SESSION['mensaje'] = "El usuario se cre� con �xito";
	
		$actionForward = $actionMapping->get('modificar');
		$path = $actionForward->getPath();
		$actionForward->setPath($path . '&id_usuario=' . $usuario->id_usuario);
		
		return $actionForward;
	}
}
?>
