<?php
/*
 * Adicionar un accionistas
 *
 * @author	John A. Vargas
 */
class eliminar_usuario extends Action
{
	/**
	 * Adicionar un accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
		$usuario = new Usuarioasam;
		
		$usuario->get($actionForm->get('id_usuario'));
		$usuario->delete();
		$_SESSION['mensaje'] = "El usuario se elimin� con �xito";
	
		$actionForward = $actionMapping->get('listar');
		return $actionForward;
	}
}
?>
