<?php
/*
 * Adicionar un accionistas
 *
 * @author	John A. Vargas
 */
class ingresar_sistema extends Action
{
	/**
	 * Adicionar un accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
		$usuario = new Usuarioasam;
		
		$usuario->get('login', $actionForm->get('login'));
		$_SESSION['usuario'] = $usuario->toArray();
			
		$parametro = new Parametro;
		$parametro->get(1);

		$_SESSION['parametro'] = $parametro->toArray();
		
		if(($asamblea = Asamblea::staticGet('estado', ABIERT0)))
			$_SESSION['asamblea'] = $asamblea->toArray();
		
		$actionForward = $actionMapping->get('listar');
		
		if (($tema = Tema::staticGet('estado', 'votacion'))) {
			$_SESSION['modulo'] = 'votar';
			$actionForward->setPath('phrame.php?action=seleccionar_tema&tema_id=' . $tema->tema_id);
		}
		
		return $actionForward;
	}
}
?>
