<?php
/*
 * Crear usuarios
 *
 * @author	John A. Vargas
 */
class modificar_usuario extends Action
{
	/**
	 * Crea un nuevo usuario
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm)
	{
		$usr = new Usuarioasam;
		$usr->get($actionForm->get('id_usuario'));
		$usr->nombre = $actionForm->get('nombre');
		$usr->login = $actionForm->get('login');
		if ($actionForm->get('passwd') != '')
			$usr->passwd = md5($actionForm->get('passwd'));		
		
		$perfiles = $actionForm->get('perfil');
		$usr->perfil = (empty($perfiles)) ? 0 : array_sum($perfiles);
		
		if ($usr->update()) {
			$_SESSION['respuesta']['accion_mensaje'] = "El usuario se modific&oacute; con &eacute;xito";
			$_SESSION['respuesta']['tipo_mensaje'] = "info";
		} else {
			$_SESSION['respuesta']['accion_mensaje'] = "El usuario NO se pudo modificar<p>Por favor contacte al administrador.</p>";
			$_SESSION['respuesta']['tipo_mensaje'] = "error";
		}
		
		$usr = new Usuarioasam;
		$usr->get($actionForm->get('id_usuario'));
		$_SESSION['user'] = $usr->toArray();
		
		if ($_SESSION['user']['id_usuario'] == $_SESSION['usuario']['id_usuario']) {
			$_SESSION['usuario'] = $usr->toArray();			
		}
	
		$actionForward = $actionMapping->get('ver_usuario');
		$path = $actionForward->getPath();
		$actionForward->setPath($path . '&id_usuario=' . $usr->id_usuario);
		
		return $actionForward;
	}
}
?>
