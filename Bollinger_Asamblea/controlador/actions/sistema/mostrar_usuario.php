<?php
/*
 * Crear usuarios
 *
 * @author	John A. Vargas
 */
class mostrar_usuario extends Action
{
	/**
	 * Crea un nuevo usuario
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm)
	{
		$usr = new Usuarioasam;
		$usr->get($actionForm->get('id_usuario'));
	
		$_SESSION['user'] = $usr->toArray();
		$actionForward = $actionMapping->get('ver_usuario');
		
		return $actionForward;
	}
}
?>
