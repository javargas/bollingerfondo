<?php
/*
 * Adicionar un accionistas
 *
 * @author	John A. Vargas
 */
class salir_sistema extends Action
{
	/**
	 * Adicionar un accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
		$actionForward = $actionMapping->get($actionForm->get('go'));
		
		return $actionForward;
	}
}
?>
