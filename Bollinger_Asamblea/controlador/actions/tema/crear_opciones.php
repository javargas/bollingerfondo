<?php
/*
 * Adicionar un accionistas
 *
 * @author	John A. Vargas
 */
class crear_opciones extends Action
{
	/**
	 * Adicionar un accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
		$opciones = $actionForm->get('opcion');
		$tema_id = $actionForm->get('tema_id');
		
		// Se borra la votacion consginada para el tema
		// Votacion es lo mismo que las opiones del tema
		$votacion = new Votacion;
		$votacion->whereAdd("tema_id = '$tema_id'");
		$votacion->delete(DB_DATAOBJECT_WHEREADD_ONLY); 
		
		foreach ($opciones as $opc) {
			$votacion = new Votacion;		
			$votacion->tema_id = $tema_id;
			$votacion->nombre = $opc;
			$votacion->asamblea_id = $_SESSION['asamblea']['asamblea_id'];
			$votacion->quorump = 0;
			$votacion->votos = 0;
			
			$votacion->insert()	;
		}
		
		$_SESSION['mensaje_opc'] = "Las opciones se adicionaron con �xito";
		$actionForward = $actionMapping->get('exito');
		return $actionForward;
	}
}
?>
