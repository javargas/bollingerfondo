<?php
/*
 * Adicionar un accionistas
 *
 * @author	John A. Vargas
 */
class crear_tema extends Action
{
	/**
	 * Adicionar un accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
		$tema = new Tema;		
		$tema->nombre = $actionForm->get('nombre');
		$tema->quorum = $actionForm->get('quorum');
		$tema->curules = $actionForm->get('curules');
		$tema->insert();  
        //echo "<pre>"; print_r($tema);  echo "</pre>"; exit;
			
		$actionForward = $actionMapping->get('exito');
		
		return $actionForward;
	}
}
?>
