<?php
/*
 * Adicionar un accionistas
 *
 * @author	John A. Vargas
 */
class describir_opcion extends Action
{
	/**
	 * Adicionar un accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
		$votacion = new Votacion;
		$votacion->candidato_id = $actionForm->get('candidato_id');
		$votacion->asamblea_id = $_SESSION['asamblea']['asamblea_id'];
		$votacion->tema_id = $_SESSION['tema']['tema_id'];
		$votacion->find(true);
		$votacion->descripcion = $actionForm->get('descripcion');
		$votacion->update();

		$actionForward = $actionMapping->get('lista');
		$path = $actionForward->getPath();
		$actionForward->setPath($path . '&tema_id=' . $_SESSION['tema']['tema_id']);
		
		return $actionForward;
	}
}
?>
