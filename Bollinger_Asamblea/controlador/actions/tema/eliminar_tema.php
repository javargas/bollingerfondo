<?php
/*
 * Eliminar un tema de la DB
 *
 * @author	John A. Vargas
 */
class eliminar_tema extends Action
{
	/**
	 * Adicionar un accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) 
	{
		$tema = new Tema;		
		$tema->get($actionForm->get('id_tema'));
		
		// Eliminar temadet
		$temadet = new Temadet;
		$temadet->whereAdd("tema_id = '{$tema->tema_id}'");
		$temadet->delete(DB_DATAOBJECT_WHEREADD_ONLY); 
		
		// Eliminar votantes
		$votantes = new Votantes;
		$votantes->whereAdd("tema_id = '{$tema->tema_id}'");
		$votantes->delete(DB_DATAOBJECT_WHEREADD_ONLY);
		
		// Eliminar votacion
		$votacion = new Votacion;
		$votacion->whereAdd("tema_id = '{$tema->tema_id}'");
		$votacion->delete(DB_DATAOBJECT_WHEREADD_ONLY);
	
		$tema->delete(); 
		
		return $actionMapping->get('lista');
	}
}
?>
