<?php
/*
 * Mostrar listado de temas
 *
 * @author	John A. Vargas
 * @date Marzo 1 de 2004
 */
class listar_temas extends Action
{
	/**
	 * Listar los temas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
		$tema = new Tema;
		$tema->find();
		
		$lista_temas = array();
		while ($tema->fetch())
			array_push($lista_temas, $tema->toArray());

		$_SESSION['lista'] = $lista_temas;
		$_SESSION['modulo'] = "temas";
		$actionForward = $actionMapping->get('lista');

		return $actionForward;
	}
}
?>
