<?php
/*
 * Adicionar un accionistas
 *
 * @author	John A. Vargas
 */
class mostrar_temas extends Action
{
	/**
	 * Adicionar un accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
	
		if (!isset($_SESSION['accion']) || $_SESSION['accion'] != 'votar') {
			$tema = new Tema;
			
			if ($actionForm->get('go') == '') {
				$tema->whereAdd("estado <> 'cerrado'");
			}
		
			//$tema->orderBy('tema_id DESC');
   
            $tema->orderBy('tema_id');
				
			$tema->find(); //print_r($tema); exit;
			
			$lista_temas = array();
			while ($tema->fetch()) {
				// Opciones por temas (candidatos)				
				$votacion = new Votacion;
				$votacion->tema_id = $tema->tema_id;
				$votacion->orderBy('candidato_id');
				$votacion->find();

				if ($votacion->N) {
					array_push($lista_temas, $tema->toArray());
				}	//			print_r($votacion); 
			}
				//exit;
			$_SESSION['lista'] = $lista_temas;
			$_SESSION['modulo'] = "votar";
			
			$actionForward = $actionMapping->get('seleccionar');
		} else
			$actionForward = $actionMapping->get('pedir_codigo');
			
		//print_r($actionForward); exit;
		return $actionForward;
	}
}
?>
