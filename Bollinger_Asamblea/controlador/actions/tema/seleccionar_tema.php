<?php
/*
 * Adicionar un accionistas
 *
 * @author	John A. Vargas
 */
class seleccionar_temas extends Action
{
	/**
	 * Adicionar un accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
		global $_DB_DATAOBJECT;
        
        $asamblea = new Asamblea;
        if (isset($_SESSION['asamblea']['asamblea_id']) && !empty($_SESSION['asamblea']['asamblea_id'])) {
            $asamblea_id = $_SESSION['asamblea']['asamblea_id'];
        }        
        
        if ($actionForm->get('asamblea_id') != '') {
            $asamblea_id = $actionForm->get('asamblea_id');
        }

        $asamblea->get($asamblea_id);
		$tema = new Tema;
		$tema->get($actionForm->get('tema_id'));
		
		if ($actionForm->get('go') == '') {
			$tema->estado = 'votacion';
			$tema->update();
		}
		
		$_SESSION['tema'] = $tema->toArray();
		$_SESSION['asamblea_tmp'] = $asamblea->toArray();

		// Opciones por temas (candidatos)				
		$votacion = new Votacion;
		$votacion->tema_id = $actionForm->get('tema_id');
        $votacion->asamblea_id = $asamblea_id;
		$votacion->orderBy('candidato_id');
		$votacion->find(); 
		$lista_opciones = new ArrayList();

		while ($votacion->fetch()) {
            	/*	$representante->selectAdd("SUM(cantidad) AS total_cantidad");

                    $_DB_DATAOBJECT['LINKS'][$tema->_database]['votantes']['accionista_id'] = 'accionista:accionista_id';
                    $_DB_DATAOBJECT['LINKS'][$tema->_database]['accionista']['accionista_id'] = 'votantes:accionista_id';
                    $_DB_DATAOBJECT['LINKS'][$tema->_database]['votantes']['candidato_id'] = 'votacion:candidato_id';
                    $_DB_DATAOBJECT['LINKS'][$tema->_database]['votacion']['candidato_id'] = 'votantes:candidato_id';
                */
                    $accionista = new Accionista;
                    $vott = new Votacion;
                    $votantes = new Votantes;
                    $votantes->asamblea_id = $asamblea_id;
                    $votantes->joinAdd($vott);
                    $votantes->whereAdd("votantes.candidato_id = '".$votacion->candidato_id."'");
                    $accionista->joinAdd($votantes);
                    $accionista->selectAdd();
                    $accionista->selectAdd("votantes.candidato_id");
                    $accionista->selectAdd("sum(accionista.cantidad) AS cantidad");
                    $accionista->groupBy("votantes.candidato_id");
                    //$vot->query($query);
                    $accionista->find(true);
                    //print_r($accionista);

                    $votacion->votos = $accionista->cantidad;
                    $votacion->update();
			$lista_opciones->add($votacion->toArray());
        }
			//exit;
		$_SESSION['lista_opciones'] = $lista_opciones->toArray();
		
		if ($actionForm->get('go') == '') {
			$_SESSION['accion'] = 'votar';
		
			$actionForward = $actionMapping->get('pedir_codigo');
		} else {
		
			/********************************************************************************************* 
			 *  Calculo del total de acciones representadas por los votantes 
			 *	Basado en el metodo de la accion ver_resultados, con algunas mejoras
			 *	
			 *	@author John A. Vargas <javargas@gmail.com>
			 *	@date 2008-03-17
			 *********************************************************************************************/					
				$representante = new Representante;
				$vot = new Votantes;
				$representante->selectAdd();
				$representante->selectAdd("SUM(cantidad) AS total_cantidad");
				
				$_DB_DATAOBJECT['LINKS'][$tema->_database]['votantes']['accionista_id'] = 'representante:accionista_id';
				$_DB_DATAOBJECT['LINKS'][$tema->_database]['representante']['accionista_id'] = 'votantes:accionista_id';
				
				
				$representante->joinAdd($vot);
				
				$representante->whereAdd("votantes.asamblea_id = '".$asamblea_id."'");
				$representante->whereAdd("tema_id = '".$tema->tema_id."'");
				$representante->whereAdd("representante.asamblea_id = '".$asamblea_id."'"); 
				
				$representante->find(true);
			//print_r($representante); exit;
			$_SESSION['total_acciones_representadas'] = $representante->total_cantidad;;
			/***********************************************************************************************/
		
			$actionForward = $actionMapping->get($actionForm->get('go'));
		}
	
		return $actionForward;
	}
}
?>
