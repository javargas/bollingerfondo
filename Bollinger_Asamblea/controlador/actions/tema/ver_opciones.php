<?php
/*
 * Mostrar listado de Opciones
 *
 * @author	John A. Vargas
 * @date Marzo 1 de 2004
 */
class ver_opciones extends Action
{
	/**
	 * Listar los temas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
		$tema = new Tema;
		$tema->get($actionForm->get('tema_id'));
	
		$votacion = new Votacion;
		$votaacion->asamblea_id = $_SESSION['asamblea']['asamblea_id'];
		$votacion->tema_id = $actionForm->get('tema_id');
		//$votacion->orderBy('nombre');
		$votacion->find(); 
		
		$lista = new ArrayList();
		while ($votacion->fetch())
			$lista->add($votacion->toArray());
			
		$_SESSION['lista']	= $lista->toArray();
		$_SESSION['tema'] = $tema->toArray();
		
		if ($actionForm->get('go') == '') {
			$actionForward = $actionMapping->get('lista');
		} else {
			$actionForward = $actionMapping->get($actionForm->get('go'));
		}

		return $actionForward;
	}
}
?>
