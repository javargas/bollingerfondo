<?php
/* CLASE: buscar accionistas */

/*
 * buscar_accionistas
 *
 * @author	John A. Vargas
 * @package Controlador
 * @subpackage Acciones
 */
class ver_reporte extends Action
{
	/**
	 * Hace un listado de los accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm)
	{		
		$from = $actionForm->get('from');
		$from = ($from) ? $from : 0;
		$order = $actionForm->get('order');
	
		$votacion = new Votacion;
		$accionista = new Accionista;
		$votantes = new Votantes;
		$tema = new Tema;
		
		// Join para busqueda
		$tema->tema_id = $actionForm->get('tema_id');
		$votantes->joinAdd($votacion);
		$votantes->joinAdd($accionista);
		$votantes->joinAdd($tema);
		
		$votantes->selectAs();
		$votantes->selectAs(array('nombre'), 'vot_%s', 'votacion');
		$votantes->selectAs(array('nombre', 'cantidad'), 'acc_%s', 'accionista');
		$votantes->selectAs(array('nombre'), 'tem_%s', 'tema');
//		$votantes->orderBy("votantes.candidato_id");

		if ($actionForm->get('go') == '') {
			$votantes->find();
			
			// Datos para paginacion de resultados
			$data = DB_Pager::getData($from, LIMIT, $votantes->N, MAXPAGES);
			
			if (!$order) $order = 'acc_nombre ASC';
			
			$votantes->orderBy($order);
			$votantes->limit($from, LIMIT); 
		} else {
			$votantes->orderBy('vot_nombre, acc_nombre');
		}
		
		$votantes->find();  //echo $votantes->print_query(); exit;
		
		$lista = new ArrayList();
		while ($votantes->fetch()) {
			$lista->add($votantes);
		}

		$_SESSION['lista'] = $lista->toArray();
		$_SESSION['data'] = $data;
		$actionForward = $actionMapping->get('ver');
		
		if ($actionForm->get('go') == 'imprimir') {
			include("vistas/reportes/reporte_votantes_imprimir.php");
			exit();
		}
		
		return $actionForward;
	}
}
?>
