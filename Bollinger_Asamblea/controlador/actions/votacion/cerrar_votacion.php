<?php
/*
 * Adicionar un accionistas
 *
 * @author	John A. Vargas
 */
class cerrar_votacion extends Action
{
	/**
	 * Adicionar un accionistas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
		unset($_SESSION['accion']);
		unset($_SESSION['lista_opciones']);
		
		$tema = new Tema;// print_r($_SESSION);
		$tema->get($_SESSION['tema']['tema_id']);
		$tema->estado = 'cerrado'; 
		$tema->update(); 
		unset($_SESSION['tema']);
		
		$actionForward = $actionMapping->get('seleccionar');
		
		/* Pasar los que esta en hsesion a sesion */
		$sesion = new Sesion;
		$sesion->whereAdd("tema_id = '{$tema->tema_id}'");
		$sesion->whereAdd("asamblea_id = '".$_SESSION['asamblea']['asamblea_id']."'");
		$sesion->detele(DB_DATAOBJECT_WHEREADD_ONLY);
		
		$qry = "INSERT INTO sesion 
				SELECT *, {$tema->tema_id} 
				FROM hsesion 
				WHERE hsesion.asamblea_id = '".$_SESSION['asamblea']['asamblea_id']."'";
		
		$sesion->query($qry);
		
		return $actionForward;
	}
}
?>
