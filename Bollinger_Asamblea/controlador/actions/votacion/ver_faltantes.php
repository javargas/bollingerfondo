<?php
/*
 * Muestra los datos de la asamblea
 *
 * @author	
 * @date Marzo 1 de 2004
 * @package Controlador
 * @subpackage Acciones
 */
class ver_faltantes extends Action
{
	/**
	 * Muestra la asamblea
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm)
	{
		$objTema = new Tema;
		$sesion = new Hsesion;
        
        if (isset($_SESSION['asamblea']['asamblea_id']) && !empty($_SESSION['asamblea']['asamblea_id'])) {
            $asamblea_id = $_SESSION['asamblea']['asamblea_id'];
        }        
        
		// al ser solicitud de reportes se asigna un tema en sesion
		if ($actionForm->get('tema_id') != '' && $actionForm->get('go') == 'reportes') {
			$objTema->get($actionForm->get('tema_id'));
		} else {
			$objTema->get($_SESSION['tema']['tema_id']);			
		}
		
		// Lista de faltantes de accionistas presentes
		$sesion->estado = 'P';
        $sesion->asamblea_id = $asamblea_id;
		$sesion->find();
		
		$lista_accionistas = array();
		while ($sesion->fetch()) {
			$votantes = new Votantes;
            $votantes->asamblea_id = $asamblea_id;
			$votantes->accionista_id = $sesion->accionista_id;
			$votantes->tema_id = $objTema->tema_id;
			
			if (!$votantes->find(true)) {
				$representante = new Representante;                
                $representante->asamblea_id = $asamblea_id;
				$representante->whereAdd("representante = '{$sesion->accionista_id}'");
				$representante->selectAdd();
				$representante->selectAdd("SUM(cantidad) AS cantidad");
				$representante->groupBy("representante");

				$accionista = new Accionista;
				$accionista->get($sesion->accionista_id);
				
				// Si esta representando a alguien...
				if ($representante->find(true)) {
				//	$accionista->cantidad = $representante->cantidad;
					$nrepresentante = new Representante;
					
					// si no es representado..
					if (!$nrepresentante->get('accionista_id',  $sesion->accionista_id)) {
					   if ($accionista->clase == 'A')
							$accionista->cantidad += $representante->cantidad;
						else
							$accionista->cantidad = $representante->cantidad;
					}
//echo "represnentadno......."; print_r($accionista);
					// Esto es porque el que esta en votacion es el accionista y no el representante
					// cojo el primer accionista para ver si no esta como votante.
					$repr = new Representante;
					$repr->get('representante', $sesion->accionista_id);
					
					$votantes2 = new Votantes;
					$votantes2->accionista_id = $repr->accionista_id;
					$votantes2->tema_id = $objTema->tema_id;
                    $votantes2->asamblea_id = $asamblea_id;

					if (!$votantes2->find(true)) { //print_r($accionista);
						//array_push($lista_accionistas, $accionista->toArray());
                        $lista_accionistas[$accionista->nombre] = $accionista->toArray();
					}
					
				} else { 					
					//array_push($lista_accionistas, $accionista->toArray());
                    $lista_accionistas[$accionista->nombre] = $accionista->toArray();
				} 
			} else { 
				$representante = new Representante;    
                $representante->asamblea_id = $asamblea_id;
				$representante->whereAdd("representante = '{$sesion->accionista_id}'");

				if ($representante->find(true)) {
					$votantes2 = new Votantes;
					$votantes2->accionista_id = $representante->accionista_id;
					$votantes2->tema_id = $objTema->tema_id;
                    $votantes2->asamblea_id = $asamblea_id;
					if (!$votantes2->find(true)) {
						//array_push($lista_accionistas, $accionista->toArray());
                        $lista_accionistas[$accionista->nombre] = $accionista->toArray();
					}
				}	
			}		
		} //exit;

        // Ordenarlo por el nombre (es la clave de cada posicion)
        ksort($lista_accionistas);

		$_SESSION['lista_faltantes'] = $lista_accionistas;
		$actionForward = $actionMapping->get('lista');
		
		return $actionForward;
	}
}
?>
