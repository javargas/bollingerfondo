<?php
require_once ("../lib/jpgraph/jpgraph.php");
require_once ("../lib/jpgraph/jpgraph_bar.php");

/*
 * Muestra los datos de la asamblea
 *
 * @author	John A. Vargas
 * @date Marzo 1 de 2004
 * @package Controlador
 * @subpackage Acciones
 */
class ver_resultados extends Action
{
	/**
	 * Muestra la asamblea
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) 
	{
		$total_acciones_representadas = 0;
	
		// Opciones a elegir en la votacion
		$datos_y = new ArrayList();
		$datos_x = new ArrayList();  
		foreach ($_SESSION['lista_opciones'] as $k => $opc) {
			$votantes = new Votantes;
			$votantes->candidato_id = $opc['candidato_id'];
			$votantes->selectAdd();
			$votantes->selectAdd("SUM(quorump) AS sum_quorum"); 
			$votantes->find(true);
			
			$datos_y->add($votantes->sum_quorum);
			$datos_x->add($opc['nombre']);
			
			$vot = new Votantes;
			$vot->tema_id = $_SESSION['tema']['tema_id'];
			$vot->asamblea_id = $_SESSION['asamblea']['asamblea_id'];
			$vot->candidato_id = $opc['candidato_id'];
			$vot->find();
			
			while ($vot->fetch()) {		
				$representacion = new Representante;
				if ($representacion->get('accionista_id', $vot->accionista_id)) {
					$total_acciones_representadas += $representacion->cantidad;
				} 
			} 
			
		}
		
		$_SESSION['total_acciones_representadas'] = $total_acciones_representadas;

		$datay = $datos_y->toArray();
		$datax = $datos_x->toArray();
		
		// Size of graph
		$width=500; 
		$height=550;
		
		// Set the basic parameters of the graph 
		$graph = new Graph($width,$height,'auto');
		$graph->SetScale("textlin");
		
		// No frame around the image
		$graph->SetFrame(false);
		
		// Rotate graph 90 degrees and set margin
		$graph->Set90AndMargin(100,20,50,30);
		
		// Set white margin color
		$graph->SetMarginColor('white');
		
		// Use a box around the plot area
		$graph->SetBox();
		
		// Use a gradient to fill the plot area
		$graph->SetBackgroundGradient('white','lightblue',GRAD_HOR,BGRAD_PLOT);
		
		// Setup title
		$graph->title->Set("Resultados de Votacion");
		$graph->title->SetFont(FF_VERDANA,FS_BOLD,11);
		$graph->subtitle->Set("(Tema: ".$_SESSION['tema']['nombre'].")");
		
		// Setup X-axis
		$graph->xaxis->SetTickLabels($datax);
		$graph->xaxis->SetFont(FF_VERDANA,FS_NORMAL, 8);
		
		// Some extra margin looks nicer
		$graph->xaxis->SetLabelMargin(5);
		
		// Label align for X-axis
		$graph->xaxis->SetLabelAlign('right','center');
		
		// Add some grace to y-axis so the bars doesn't go
		// all the way to the end of the plot area
		$graph->yaxis->scale->SetGrace(20);
		
		// We don't want to display Y-axis
		$graph->yaxis->Hide(); //print_r($graph->yaxis);print_r($graph->yaxis); exit;
		
		// Now create a bar pot
		$bplot = new BarPlot($datay);
		$bplot->SetShadow();
		
		//You can change the width of the bars if you like
		//$bplot->SetWidth(0.5);
		
		// Set gradient fill for bars
		$bplot->SetFillGradient('darkred','yellow',GRAD_HOR);
		
		// We want to display the value of each bar at the top
		$bplot->value->Show();
		$bplot->value->SetFont(FF_ARIAL,FS_BOLD, 15);
		//$bplot->value->SetAlign('left','center');
		$bplot->value->SetColor("white");
		$bplot->value->SetFormat('%.1f');
		$bplot->SetValuePos('max');
		
		// Add the bar to the graph
		$graph->Add($bplot);
		
		// Add some explanation textz�
		/*$txt = new Text('Nota: Total de acciones:'.number_format($_SESSION['parametro']['totalacciones']));
		$txt->SetPos(290,539,'center','bottom');
		$txt->SetFont(FF_COMIC,FS_NORMAL,8);
		$graph->Add($txt);
		*/
		// .. and stroke the graph
		$graph->Stroke();
	}
}
?>
