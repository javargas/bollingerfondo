<?php
/*
 * Mostrar listado de Opciones
 *
 * @author	John A. Vargas
 * @date Marzo 1 de 2004
 */
class votar extends Action
{
	/**
	 * Listar los temas
	 *
	 * @access	public
	 * @param	ActionMapping	$actionMapping
	 * @param	ActionForm		$actionForm
	 * @return	ActionForward
	 */
	function perform($actionMapping, $actionForm) {
		
		$votos = $actionForm->get('valor');
		foreach ($votos as $k => $v) {
			$votantes = new Votantes;
			$accionista = new Accionista;
			$accionista->get($k);
			$votantes->asamblea_id = $_SESSION['asamblea']['asamblea_id'];
			$votantes->tema_id = $_SESSION['tema']['tema_id'];
			$votantes->accionista_id = $k;
			$votantes->quorump = $accionista->cantidad;//Util::participacion($accionista->cantidad);
			$votantes->candidato_id = $v;
			$votantes->insert();
			
			$votacion = new Votacion;
			$votacion->candidato_id = $v;
			$votacion->asamblea_id = $_SESSION['asamblea']['asamblea_id'];
			$votacion->tema_id = $_SESSION['tema']['tema_id'];
			$votacion->find(true);
			$votacion->quorump += Util::participacion($accionista->cantidad);
			$votacion->votos += $accionista->cantidad;
			$votacion->update();
		}	
		
		unset($_SESSION['lista_opciones']);
		$votacion = new Votacion;
		$votacion->tema_id = $_SESSION['tema']['tema_id'];
		$votacion->orderBy('candidato_id');
		$votacion->find(); 
		$lista_opciones = new ArrayList();
		while ($votacion->fetch())
			$lista_opciones->add($votacion->toArray());
			
		$_SESSION['lista_opciones'] = $lista_opciones->toArray();
		$actionForward = $actionMapping->get('ingresar_codigo');
		
		return $actionForward;
	}
}
?>