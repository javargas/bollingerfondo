<?php
/*
 * Ingreso de un usuario a la sesi�n
 *
 * @author	John A. Vargas
 * @version 1.0
 */
class crear_accionistaForm extends ActionForm
{
	/**
	 * Validar que exista asistencia para el usuario a ingresar
	 *
	 * @access	public
	 * @return	boolean
	 */
	function validate()
	{
		$isValid = TRUE;
	
		$accionista = new Accionista; 
		$accionista->documento = $this->get('documento');
		if ($accionista->find(true)) {
			$_SESSION['mensaje'] = "Error: Ya existe un accionista con la identificaci�n ";
			$_SESSION['mensaje'] .= $this->get('documento');
			$isValid = FALSE;
		}
		
		$asistencia = new Asistencia; 
		$asistencia->codbarra_id = $this->get('codigo');
		if ($asistencia->find(true)) {
			$_SESSION['mensaje'] .= "Error: Ya existe un accionista con el c�digo ";
			$_SESSION['mensaje'] .= $this->get('codigo');
			$isValid = FALSE;
		}

		return $isValid;
	}
}
?>
