<?php
/*
 * Forma por defecto
 *
 * @author	John A. Vargas
 * @version	$Id: HelloForm.php,v 1.1.1.1 2002/11/19 16:47:09 arcano Exp $
 */
class crear_usuarioForm extends ActionForm
{
	/**
	 * formulario por defecto
	 *
	 * @access	public
	 * @return	boolean
	 */
	function validate()
	{
		$isValid = TRUE;
		$_SESSION['respuesta'] = array();
		
		$usuario = new Usuario;
		$usuario->login = $this->get('login');
		
		if ($usuario->find(true)) {
			$_SESSION['respuesta']['accion_mensaje'] = "Ya existe un usuario con el login <b>{$usuario->login}</b>.<br><p>Por favor escoja otro e intente de nuevo.</p>";
			$_SESSION['respuesta']['tipo_mensaje'] = "error";
			$isValid = FALSE;
		}
		
		return $isValid;
	}
}
?>
