<?php
/*
 * Ingreso de un usuario a la sesi�n
 *
 * @author	John A. Vargas
 * @version 1.0
 */
class delegarForm extends ActionForm
{
	/**
	 * Validar que exista asistencia para el usuario a ingresar
	 *
	 * @access	public
	 * @return	boolean
	 */
	function validate()
	{
		$isValid = TRUE;
		$_SESSION['respuesta'] = array();
		$_SESSION['respuesta']['alto_ventana'] = 130;
	
		$mensaje = '';
		$accionistas = $this->get('accionista');
		$accionistas_validos = array();
		
		foreach ($accionistas as $accionista_id) {
			$accionista = Accionista::staticGet($accionista_id);			
			if (($asistencia = Asistencia::staticGet('accionista_id', $accionista_id))) {
				$mensaje .= ' * El accionista '.$accionista->nombre.' con documento '.$accionista->documento.' ya esta registrador \n';
				$_SESSION['respuesta']['alto_ventana'] += 20;
				$isValid = false;
			} elseif (($representante = Representante::staticGet('accionista_id', $accionista_id))) {
				$mensaje .= '* El accionista '.$accionista->nombre.' con documento '.$accionista->documento.' ya esta siendo representado \n';
				
				$_SESSION['respuesta']['alto_ventana'] += 20;
				$isValid = false;
			} else {
				array_push($accionistas_validos, $accionista->toArray());
			}
			
			
		}
		
		if (!$isValid) {
			$mensaje = 'Se presentaron los siguientes errores al delegar: \n \n' . $mensaje;
	
			// Respuesta de la accion
			$_SESSION['respuesta']['accion_mensaje'] = $mensaje;
			$_SESSION['respuesta']['tipo_mensaje'] = 'error';
			$_SESSION['respuesta']['ancho_ventana'] = 500;
			
			// Accionistas que si son validos para delegar
			$_SESSION['respuesta']['accionistas_validos'] = $accionistas_validos;
		} else { 
			unset($_SESSION['accionista']); 
			unset($_SESSION['representante']);
			unset($_SESSION['representante_id']);
			unset($_SESSION['accionista_id']); 
		}

		return $isValid;
	}
}
?>
