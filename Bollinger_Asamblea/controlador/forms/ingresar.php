<?php
/*
 * Ingreso de un usuario a la sesi�n
 *
 * @author	John A. Vargas
 * @version 1.0
 */
class ingresarForm extends ActionForm
{
	/**
	 * Validar que exista asistencia para el usuario a ingresar
	 *
	 * @access	public
	 * @return	boolean
	 */
	function validate()
	{
		$isValid = TRUE;
	
		$asistencia = new Asistencia; 
        $asistencia->whereAdd("asamblea_id = '".$_SESSION['asamblea']['asamblea_id']."'");
        $asistencia->whereAdd("codbarra_id = '".$this->get('codigo')."'");
        
		if (!$asistencia->find(true)) {
			$_SESSION['mensaje_codigo'] = "No existe usuario registrado con el c&oacute;digo ";
			$_SESSION['mensaje_codigo'] .= $this->get('codigo');
			$_SESSION['tipo_mensaje'] = 'error';
			$isValid = FALSE;
		}

		return $isValid;
	}
}
?>
