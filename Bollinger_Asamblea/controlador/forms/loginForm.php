<?php

/*
 * Forma por defecto
 *
 * @author	John A. Vargas
 * @version	$Id: HelloForm.php,v 1.1.1.1 2002/11/19 16:47:09 arcano Exp $
 */
class loginForm extends ActionForm
{
	/**
	 * formulario por defecto
	 *
	 * @access	public
	 * @return	boolean
	 */
	function validate()
	{
		$usuario = new Usuarioasam;
		$usuario->login = $this->get('login');
		$usuario->passwd = md5($this->get('passwd')); 
		
		$isValid = $usuario->find(true);
        
        //echo "<pre"; print_r($usuario); echo "</pre>"; die();
		if (!$isValid) {
			$_SESSION['mensaje'] = "Posiblemente escribio mal el login y/o la contrase�a\\n";
			$_SESSION['mensaje'] .= "Por favor intente de nuevo, Gracias";
		}
		
		// Verificar que no haya tema en votacion, si lo hay el usuario
		// debe tener permisos para votar
		$tema = new Tema;
		$tema->estado = 'votacion';
		if ($tema->find() && !($usuario->perfil &VOTAR )) {
			$_SESSION['mensaje'] = "Error: Existe un tema en votaci�n, \\n ";
			$_SESSION['mensaje'] .= "S�lo puede ingresar un usuario con permiso para votar";
			$isValid = FALSE;			
		} 
		
		return $isValid;
	}
}
?>
