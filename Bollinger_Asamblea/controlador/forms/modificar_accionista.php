<?php
/*
 * Ingreso de un usuario a la sesi�n
 *
 * @author	John A. Vargas
 * @version 1.0
 */
class modificar_accionistaForm extends ActionForm
{
	/**
	 * Validar que exista asistencia para el usuario a ingresar
	 *
	 * @access	public
	 * @return	boolean
	 */
	function validate()
	{
		$isValid = TRUE;
	
		if ($this->get('codigo') != '') {
			$asistencia = new Asistencia; 
			
			$asistencia->asamblea_id = $_SESSION['asamblea']['asamblea_id'];
			$asistencia->codbarra_id = $this->get('codigo');
			
			$asistencia->whereAdd("accionista_id <> '".$this->get('accionista_id')."'");
		//$asistencia->find(true); print_r($asistencia); exit;
			return !$asistencia->find(true);
		}
		
		return $isValid;
	}
}
?>
