<?php
/*
 * Validar que exista un quorum m�nimo para poder votar por el tema a seleccionar
 *
 * @author	John A. Vargas
 * @version 1.0
 */
class seleccionar_temaForm extends ActionForm
{
	/**
	 * Validar que exista presencia de un accionista/delegado para votar.
	 *
	 * @access	public
	 * @return	boolean
	 */
	function validate()
	{
		$isValid = TRUE;
		
		// Requerimiento 12 de Abril 11 de 2008. No validar quorum para reporte
		if ($this->get('go') == 'imprimir_resultados' || $this->get('go') == 'imprimir_resumen') {
			return $isValid;
		}

		$tema = new Tema;
		$tema->get($this->get('tema_id'));
		
		/* Num de accionistas y cantidad de acciones de presentes */
		$sesion = new Hsesion;
		$sesion->estado = 'P';
		$sesion->selectAdd();
		$sesion->selectAdd('MAX(h_ingreso) as ingreso, accionista_id, estado');
		$sesion->groupBy('accionista_id');
		$sesion->find();
		$num_presentes = 0; $acciones_presentes = 0;
		while ($sesion->fetch()) { 
			$representante = new Representante;
			$representante->accionista_id = $sesion->accionista_id;
			if (!$representante->find(true)) {
				$accionista = new Accionista;
				$accionista->get($sesion->accionista_id);
				$acciones_presentes += $accionista->cantidad;
			}
			// La participacion de los que representa
			$representacion = new Representante;
			$representacion->selectAdd();
			$representacion->representante = $sesion->accionista_id;
			$representacion->selectAdd("COUNT(accionista_id) AS num_accionistas, SUM(cantidad) as total_cantidad");
			$representacion->groupBy('representante');
			$representacion->find(true); 
			$acciones_presentes += $representacion->total_cantidad;
		}
		$porcentaje = Util::participacion($acciones_presentes);
	
		if ($tema->quorum > $porcentaje) {
			$isValid = FALSE;
			$_SESSION['mensaje'] = 'No existe quorum suficiente para votar por el tema ' . $tema->nombre;
			$_SESSION['mensaje'] .= '\\n\\n El quorum para este tema es de ' . $tema->quorum . '% en acciones, hay ' . $porcentaje . ' en el momento.';
		}		

		return $isValid;
	}
}
?>
