<?php
/*
 * Validar que el usuario este presente para votar
 *
 * @author	John A. Vargas
 * @version 1.0
 */
class votarForm extends ActionForm
{
	/**
	 * Validar que exista presencia de un accionista/delegado para votar.
	 *
	 * @access	public
	 * @return	boolean
	 */
	function validate()
	{
		$isValid = TRUE;
		
		$asistencia = new Asistencia; 
		$asistencia->codbarra_id = $this->get('codigo');
		
		// Que haya asisitido
		if (!$asistencia->find(true)) {
			$_SESSION['mensaje'] = "Error: No existe usuario registrado con el codigo ";
			$_SESSION['mensaje'] .= $this->get('codigo');
			$isValid = FALSE;
		} else {
			$sesion = new Hsesion;
			$sesion->consecutivo = $asistencia->consecutivo;
			$sesion->accionista_id = $asistencia->accionista_id;
			
			// Que este presente
			if (!$sesion->find(true) || $sesion->estado != 'P') {
				$_SESSION['mensaje'] = "Error: El usuario con el codigo ";
				$_SESSION['mensaje'] .= $this->get('codigo');
				$_SESSION['mensaje'] .= " no se encuentra presente";
				$isValid = FALSE;
			} else {
				$votantes = new Votantes;
				$votantes->tema_id = $_SESSION['tema']['tema_id'];
				$votantes->accionista_id = $asistencia->accionista_id;
				$representante = new Representante;
				if ($representante->get('representante', $asistencia->accionista_id)) {
					$votantes->accionista_id = $representante->accionista_id;
				}
				
				if ($votantes->find(true)) {
					$_SESSION['mensaje'] = "Error: El usuario con el codigo ";
					$_SESSION['mensaje'] .= $this->get('codigo');
					$_SESSION['mensaje'] .= " ya vot� por este tema";
					$isValid = FALSE;
				}
			}
		}

		return $isValid;
	}
}
?>
