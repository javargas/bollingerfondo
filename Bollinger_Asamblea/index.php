<?php
error_reporting(E_ALL ^ E_STRICT ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);

include('config/config.inc.php');
session_start();

$estema = new Tema;
$estema->estado = 'votacion';
if ($estema->find()  && $_SESSION['modulo'] != 'votar' && $_SESSION['modulo'] != 'temas' && 
	$_GET[_VIEW] != 'vistas/login.html' && $_GET[_VIEW] != 'vistas/salir.html')  {
	$_SESSION['modulo'] = 'votar';
	echo "<script>alert('El sistema esta en estado de votaci�n');
					location = 'phrame.php?action=salir_sistema';
			</script>
			Bloqueado por votaci�n";
//	header('Location: phrame.php?action=salir_sistema');
	exit();
}

/** Obligar a un delegado recien creado, tener delegaciones asignadas **/
if (isset($_SESSION['accion']) && $_SESSION['accion'] == 'delegar' &&
   $_SESSION['action'] != 'Cancelar' && $_GET[_VIEW] != 'vistas/dialogos/pedir_codigo.html') {
	if (!isset($_SESSION['mensaje_delegacion'] ))
		$_SESSION['mensaje_delegacion'] .= 'Por favor asignele una delegaci�n.';
	if ($_GET['dialogo'] != 'mostrar_delegacion' && $_GET[_VIEW] != 'vistas/buscar_accionistas.html') {
	  header("Location: phrame.php?action=mostrar_delegacion&representante_id=" . $_SESSION['representante_id']);	
	}
}
/** Mostrar la vista asginada **/
$vista = (isset($_GET[_VIEW])) ? $_GET[_VIEW] : 'vistas/login.html';
include ($vista);
?>
