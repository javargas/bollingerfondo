<?php
error_reporting(E_ALL ^ E_STRICT ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);

include('config/config.inc.php');
session_start();

/** Instanciar el controlador **/
$controller = new ActionController($go_map->GetOptions());

$estema = new Tema;
$estema->estado = 'votacion';
if ($estema->find()  && $_SESSION['modulo'] != 'votar' && $_SESSION['modulo'] != 'temas' && 
	$_REQUEST['action'] != 'ingresar_sistema' && $_REQUEST['action'] != 'salir_sistema')  {
	$_SESSION['modulo'] = 'votar';
	echo "<script>alert('El sistema esta en estado de votaci�n');
					location = 'phrame.php?action=salir_sistema';
			</script>
			Bloqueado por votaci�n";
	//header('Location: phrame.php?action=salir_sistema');
	exit();
}

/** Procesar la accion a ejecutar **/
$controller->process($go_map->GetMappings(), $_REQUEST);
?>
