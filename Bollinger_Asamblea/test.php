<?php
include('config/config.inc.php');
require_once ('../modelo/Acciones.php');
session_start();

$accionista = new Accionista;

$accionista->find();

while ($accionista->fetch()) {
	$acciones = new Acciones;
	$acciones->accionista_id = $accionista->accionista_id;
	$acciones->selectAdd();
	$acciones->selectAdd("SUM(cantidad) AS total_cantidad");
	$acciones->find(true);
	
	$accionista->cantidad = $acciones->total_cantidad;
	$accionista->update();
} 

?>
