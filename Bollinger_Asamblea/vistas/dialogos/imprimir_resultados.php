<?php
// Clase para construir PDF
define('FPDF_FONTPATH', '../lib/font/');
require('../lib/fpdf.php');

// Cabecera del documento
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
header("Content-Type: application/force-download");
header("Content-Description: File Transfer");
header("Content-Disposition: attachment; filename=reporte_detalle_votacion.pdf"); 

$namefile = time().".pdf";
$padd = "20";
$num_pagina = 1;

$pdf=new FPDF();

$pdf->SetTitle("Bollinger Asamblea :: Reporte detallado de votaci�n");
$pdf->SetSubject("Reporte detalle de votaci�n");
$pdf->SetAuthor("Sociedad Portuaria Regional de Buenaventura :: Sistema Bollinger");
$pdf->SetLeftMargin($padd);
$pdf->AddPage(); 

// Contenido del documento PDF
$pdf->SetFont('Arial','B',10);

$pdf->Text($padd + 32, 22, 'FONDO DE EMPLEADOS DE LA SOCIEDAD PORTUARIA REGIONAL DE BUENAVENTURA S.A.');
$pdf->Text($padd + 50, 28, 'Asamblea ' . $_SESSION['asamblea_tmp']['nombre']);
$pdf->Text($padd + 66, 34, 'Reporte de votacion');
$pdf->setY(40); $pdf->Write(5, 'Tema: ');
$pdf->SetFont('Arial', '', 10); $pdf->Write(5, $_SESSION['tema']['nombre']);

$pdf->SetFont('Arial','B',10);
$pdf->Text($padd, 54, 'Opcion');
$pdf->Text($padd + 28, 54, 'Votos');
//$pdf->Text($padd + 58, 54, 'Participaci�n');
			
$y = 60;
$pdf->SetFont('Arial','',10);

$total_acciones = 0;
foreach ($_SESSION['lista_opciones'] as $k => $opc) {
	$pdf->Text($padd, $y, $opc['nombre']); // Nombre de la opcion
	$pdf->Text($padd + 28, $y, number_format($opc['votos'], 0, ',', '.')); // Votos para la opcion
//	$pdf->Text($padd + 58, $y, Util::participacion($opc['votos']) . '%'); // participacion de votos de la opcion
	
	$total_acciones += $opc['votos'];
	$y += 6;
	
	if ($y > 290) {
		$pdf->SetFont('Arial','',8);
		$pdf->Text($padd + 60, $y, "- P�gina {$num_pagina} -");
		$num_pagina++;
		$pdf->AddPage();
		$pdf->SetFont('Arial','',10);
		$y = 15;
	}
}

$y += 6; $pdf->setY($y);
$pdf->SetFont('Arial', 'B', 10); $pdf->Write(5, 'Total de acciones: ');
$pdf->SetFont('Arial', '', 10); $pdf->Write(5, number_format($_SESSION['parametro']['totalacciones']) ); 
/*
$y += 6; $pdf->setY($y);
$pdf->SetFont('Arial', 'B', 10); $pdf->Write(5, 'Acciones representadas por delegados: ');
$pdf->SetFont('Arial', '', 10); $pdf->Write(5, number_format($_SESSION['total_acciones_representadas'], 0, ',', '.')); */  // Comentado para cumplir a observacion 10 del 11 de abril de 2008

//$y += 6; $pdf->setY($y);
//$pdf->SetFont('Arial', 'B', 10); $pdf->Write(5, 'Acciones que participaron: '); 
//$pdf->SetFont('Arial', '', 10); $pdf->Write(5, number_format($total_acciones, 0, ',', '.')); 

//$y += 6; $pdf->setY($y);
//$pdf->SetFont('Arial', 'B', 10); $pdf->Write(5, 'Porcentaje de participaci�n: ');
//$pdf->SetFont('Arial', '', 10); $pdf->Write(5, Util::participacion($total_acciones)); 

$y += 6; $pdf->setY($y);
$pdf->SetFont('Arial', 'B', 10); $pdf->Write(5, 'Quorum minimo: ');
$pdf->SetFont('Arial', '', 10); $pdf->Write(5, $_SESSION['tema']['quorum']); 

$y += 12;
if ($_SESSION['tema']['curules'] != 0) {
	$cociente = $total_acciones / $_SESSION['tema']['curules'];
	
	if ($cociente != 0) {
		$pdf->Text($padd, $y, 'Cociente ' . number_format($cociente, 4, ',', '.'));
		$y += 6;

		$pdf->SetFont('Arial','B',10);
		$pdf->Text($padd, $y, 'Opci�n');
		$pdf->Text($padd + 38, $y, ' ');
		$pdf->Text($padd + 98, $y, 'Cociente');
		$pdf->Text($padd + 118, $y, 'Residuo');
					
		$y += 6;
		$pdf->SetFont('Arial','',10);
		
		$lista_ordenada = array();
		foreach ($_SESSION['lista_opciones'] as $k => $opc) {
			$lista_ordenada[$k] = $opc['votos'] - intval($opc['votos'] / $cociente) * $cociente;
		}
		
		arsort($lista_ordenada);
		
		foreach ($lista_ordenada as $k => $res) { 
			$pdf->Text($padd, $y, $_SESSION['lista_opciones'][$k]['nombre']); // Nombre de la opcion
			$pdf->Text($padd + 38, $y, number_format($_SESSION['lista_opciones'][$k]['votos'], 0, ',', '.') . '/' . number_format($cociente, 4, ',', '.') ); // Votos para la opcion
			$pdf->Text($padd + 98, $y, intval($_SESSION['lista_opciones'][$k]['votos'] / $cociente) ); // 
			$pdf->Text($padd + 118, $y, number_format($_SESSION['lista_opciones'][$k]['votos'] - intval($_SESSION['lista_opciones'][$k]['votos'] / $cociente) * $cociente, 4, ',', '.') ); // 
			
			$y += 6;
			
			if ($y > 290) {
				$pdf->SetFont('Arial','',8);
				$pdf->Text($padd + 60, $y, "- P�gina {$num_pagina} -");
				$num_pagina++;
				$pdf->AddPage();
				$pdf->SetFont('Arial','',10);
				$y = 15;
			}
		}
	}
}

$y += 6;
$pdf->SetFont('Arial','', 6); 
$pdf->Text($padd, $y, 'Usuario: ' . $_SESSION['usuario']['nombre']); $y += 4;
$pdf->Text($padd, $y, $dias[strftime("%w")] . ", " . $meses[date("n")-1] . date(" j") . " de " . date(" Y g:i a"));

$pdf->Output($namefile);

readfile($namefile);
@unlink($namefile);


?>
