<style type="text/css">
<!--
.style1 {font-size: x-small}
.style2 {font-weight: bold}
-->
</style>
<h1 align="center">Sociedad Portuaria de Buenaventura</h1>
<h2 align="center">Asamblea No. <?= $_SESSION['asamblea']['asamblea_id'] ?></h2>
<h3 align="center">Reporte de votaci&oacute;n </h3>
<p><br>
  <b> Tema: </b><?= $_SESSION['tema']['nombre']?>
</p>
<table  border="0" cellpadding="7" cellspacing="1" bgcolor="#000000">
  <tr align="left" bgcolor="#FFFFFF" class="txtablas">
    <td width="187" scope="col">Opci&oacute;n</td>
    <td width="113" nowrap scope="col">Acciones&nbsp;&nbsp;</td>
    <td width="109" scope="col">Participaci&oacute;n</td>
  </tr>
  <?  
  $total_acciones = 0;
  foreach ($_SESSION['lista_opciones'] as $k => $opc) { ?>
  <tr bgcolor="#FFFFFF" >
    <td align="left"> 
        <?= $opc['nombre'] ?>
    </td>
    <td align="right"><?= number_format($opc['votos'], 0, ',', '.') ?>
&nbsp;&nbsp;&nbsp;</td>
    <td align="right"><?= $opc['quorump'] ?>
      %&nbsp; </td>
  </tr>
  <?
  $total_acciones += $opc['votos'];
   } ?>
</table>
<br>
<b>Total de acciones: </b><?= number_format($_SESSION['parametro']['totalacciones']) ?><br />
<b>Acciones representadas por delegados:</b> <?= number_format($_SESSION['total_acciones_representadas'], 0, ',', '.') ?><br />
<b>Acciones que participaron:</b> <?= number_format($total_acciones, 0, ',', '.') ?><br>
<b>Porcentaje de participaci&oacute;n: </b><?= Util::participacion($total_acciones)?>%<br />
<strong>Quorum m&iacute;nimo:</strong> <?= $_SESSION['tema']['quorum'] ?> %<br>
<p><br>
<?  
if ($_SESSION['tema']['curules'] != 0) {
	$cociente = $total_acciones / $_SESSION['tema']['curules'];
	
	if ($cociente != 0) {
		?> 
	<p>Cociente <?= number_format($cociente, 4, ',', '.') ?>
	<table  border="0" cellpadding="7" cellspacing="1" bgcolor="#000000">
		  <tr align="left" bgcolor="#FFFFFF" class="txtablas">
			<th width="355" scope="col">Opci&oacute;n</th>
			<th width="81" align="center" nowrap scope="col">&nbsp;</th>
			<th width="81" align="center" nowrap scope="col">Cociente</th>
			<th width="81" align="center" nowrap scope="col">Residuo</th>
		  </tr>
		  <?  
		  $lista_ordenada = array();
		  foreach ($_SESSION['lista_opciones'] as $k => $opc) {
			$lista_ordenada[$k] = $opc['votos'] - intval($opc['votos'] / $cociente) * $cociente;
		  }
		  
		  arsort($lista_ordenada);
		  
		  foreach ($lista_ordenada as $k => $res) { ?>
		  <tr bgcolor="#FFFFFF" >
			<td align="left"> 
				<?= $_SESSION['lista_opciones'][$k]['nombre'] ?>
			</td>
			<td align="center" nowrap><span class="style2">
			  <?= number_format($_SESSION['lista_opciones'][$k]['votos'], 0, ',', '.') . '/' . number_format($cociente, 4, ',', '.') ?>
			</span></td>
			<td align="center" nowrap><span class="style2">
			  <?= intval($_SESSION['lista_opciones'][$k]['votos'] / $cociente) ?>
			</span></td>
			<td align="center" nowrap> <span class="style2">
			  <?= number_format($_SESSION['lista_opciones'][$k]['votos'] - intval($_SESSION['lista_opciones'][$k]['votos'] / $cociente) * $cociente, 4, ',', '.') ?>
			</span> </td>
		  </tr>
		  <? } ?>
	</table>
	</p>
<? 	}
  } ?>
<p class="style1">
Usuario: <?= $_SESSION['usuario']['nombre']?>
<br>
<?= $dias[strftime("%w")] . ", " . $meses[date("n")-1] . date(" j") . " de " . date(" Y g:i a") ?></p>
<script>print()</script>
              <br>
<br>
