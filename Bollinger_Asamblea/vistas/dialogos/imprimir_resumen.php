
<h1 align="center">Fondo de Empleados de la Sociedad Portuaria de Buenaventura</h1>
<h2 align="center">Asamblea No. <?= $_SESSION['asamblea_tmp']['asamblea_id'] ?></h2>
<h3 align="center">Reporte resumen de votaci&oacute;n </h3>
<p><br>
  <b> Tema: </b><?= $_SESSION['tema']['nombre']?>
</p>

<?  
/** Calculo nuevamente del cociente y residuo para eleccion de personas por plancha */
  $total_acciones = 0;
  foreach ($_SESSION['lista_opciones'] as $k => $opc) { 
	  $total_acciones += $opc['votos'];
   }
   
if ($_SESSION['tema']['curules'] != 0) {
	$cociente = $total_acciones / $_SESSION['tema']['curules'];
	
	if ($cociente != 0) { ?>
<table  border="0" cellpadding="7" cellspacing="1" bgcolor="#000000">
  <tr align="left" bgcolor="#FFFFFF" class="txtablas">
    <th width="187">Opci&oacute;n</td>
    <th width="113" nowrap>Item descripci&oacute;n</td>
    <th width="109">Elegido por</td>
    <th width="109"></td>
  </tr>
	<?  // Lista ordenada por residuo
		$lista_ordenada_residuo = array();
		  foreach ($_SESSION['lista_opciones'] as $k => $opc) {
			$lista_ordenada_residuo[$k] = $opc['votos'] - intval($opc['votos'] / $cociente) * $cociente;
		  }
		  
		  arsort($lista_ordenada_residuo);
		  
		  // Lista ordenada por cociente
		   $lista_ordenada_cociente = array();
		  foreach ($_SESSION['lista_opciones'] as $k => $opc) {
			$lista_ordenada_cociente[$k] = intval($opc['votos'] / $cociente);
		  }
		  
		  arsort($lista_ordenada_cociente);
		  
		  foreach ($_SESSION['lista_opciones'] as $k => $arreglo) {
		  	$lista_desc[$k] = explode("\n", $arreglo['descripcion']);
		  }
		  
		  $num_elegidos = 0;
		  // Elegidos por cociente
		  foreach ($lista_ordenada_cociente as $k => $res) { 
		  				
		  	for ($i = 0; $i < $res; $i++) {
		  ?>
		  <tr bgcolor="#FFFFFF">
			<td align="left"><?= $_SESSION['lista_opciones'][$k]['nombre'] ?></td>
			<td align="left" nowrap><?= current($lista_desc[$k]) ?></td>
			<td align="center" nowrap>Cociente</td>
			<td align="center" nowrap></td>
		  </tr>
		 <?  $num_elegidos++;
		 next($lista_desc[$k]);
		  } } 
		 
		 // Elegidos por residuo
		 foreach ($lista_ordenada_residuo as $k => $res) {
			 if ($_SESSION['tema']['curules'] == $num_elegidos++) break;
			 
			// $i = 0;
		//	$lista_desc = explode("\n", $_SESSION['lista_opciones'][$k]['descripcion']);
		  ?>
		  <tr bgcolor="#FFFFFF">
			<td align="left"><?= $_SESSION['lista_opciones'][$k]['nombre'] ?></td>
			<td align="left" nowrap><?= current($lista_desc[$k]) ?></td>
			<td align="center" nowrap>Residuo</td>
			<td align="center" nowrap><?= number_format($res, 4, ',', '.') ?></td>
		  </tr>
		 <? 
		 next($lista_desc[$k]);
		  } // fin de foreach residuos		   ?>
</table>
 <?	} // fin if cociente
  } // fin de curules
 ?>


<p class="style1">
Usuario: <?= $_SESSION['usuario']['nombre']?>
<br>
<?= $dias[strftime("%w")] . ", " . $meses[date("n")-1] . date(" j") . " de " . date(" Y g:i a") ?></p>
<script language="javascript" type="text/javascript">
<!-- 
print()
--></script>
              <br>
<br>
