<?
// Clase para construir PDF
define('FPDF_FONTPATH', '../lib/font/');
require('../lib/fpdf.php');

// Cabecera del documento
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
header("Content-Type: application/force-download");
header("Content-Description: File Transfer");
header("Content-Disposition: attachment; filename=reporte_votantes_imprimir.pdf"); 

$namefile = time().".pdf";
$padd = "10";
$num_pagina = 1;

$pdf=new FPDF();

global $meses, $dias;

$pdf=new FPDF();
$pdf->SetTitle("Bollinger Asambleas :: Reporte de Presentes");
$pdf->SetSubject("Reporte de votantes");
$pdf->SetAuthor("John A. Vargas <javargas@gmail.com>");
$pdf->AddPage();

// Contenido del documento PDF
$pdf->SetFont('Arial','B',10);

$pdf->Text($padd + 32, 32, NOMBRE_EMPRESA);

$pdf->SetFont('Arial','',10);
$pdf->Text($padd + 8, 44, 'Buenaventura, ' . date("j") . ' de '. $meses[date("n")-1] . ' de ' . date("Y"));
$pdf->Text($padd + 8, 50, 'Reporte de votos ' . $_SESSION['asamblea']['nombre']);

$pdf->SetFont('Arial','B',10);
$pdf->Text($padd, 62, 'Tema');
$pdf->Text($padd + 40, 62, 'Asociados');
$pdf->Text($padd + 120, 62, 'Candidato');
$pdf->Text($padd + 150, 62, 'Cantidad');
//$pdf->Text($padd + 170, 62, 'Participaci�n');

$y = 68;
$pdf->SetFont('Arial','',10);

$total_acciones = 0;

foreach ($_SESSION['lista'] as $k => $voto) {  
	$pdf->Text($padd, $y, $voto->tem_nombre);
	$pdf->Text($padd + 40, $y, $voto->acc_nombre);
	$pdf->Text($padd + 120, $y, $voto->vot_nombre);
	//$pdf->Text($padd + 150, $y, number_format($voto->acc_cantidad, 0, ',', '.'));
	//$pdf->Text($padd + 170, $y, Util::participacion($voto->acc_cantidad).' %');
	$y += 6;
	
	$total_acciones += $voto->acc_cantidad;
	
	if ($y > 290) {
		$pdf->SetFont('Arial','',8);
		$pdf->Text($padd + 60, $y, "- Pagina{$num_pagina} -");
		$num_pagina++;
		$pdf->AddPage();
		$pdf->SetFont('Arial','',10);
		$y = 15;
	}
}

$y += 6;
//$pdf->Text($padd + 8, $y, 'Total acciones: ' . number_format($total_acciones, 0, ',', '.') );
$y += 6;
//$pdf->Text($padd + 8, $y, 'Total participaci�n: ' . Util::participacion($total_acciones) . ' %' );

$y += 12;
$pdf->SetFont('Arial','',8);
$pdf->Text($padd + 8, $y, "Usuario: ".$_SESSION['usuario']['nombre']);
$y += 3;
$pdf->Text($padd + 8, $y, $dias[strftime("%w")] . ", " . $meses[date("n")-1] . date(" j") . " de " . date(" Y g:i a"));

$pdf->Output($namefile);

readfile($namefile);
@unlink($namefile);

?>