<?php
// Clase para construir PDF
define('FPDF_FONTPATH', '../lib/font/');
require('../lib/fpdf.php');

// Cabecera del documento
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
header("Content-Type: application/force-download");
header("Content-Description: File Transfer");
header("Content-Disposition: attachment; filename=reporte_asistencias.pdf"); 

$namefile = time().".pdf";
$padd = "15";
$num_pagina = 1;

$pdf=new FPDF();

$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto",
"Septiembre","Octubre","Noviembre","Diciembre");


$pdf=new FPDF();
$pdf->SetTitle("Bollinger Asambleas :: Reporte de Asistencias");
$pdf->SetSubject("Reporte de Asistencias");
$pdf->SetAuthor("John A. Vargas <javargas@gmail.com>");
$pdf->AddPage();

// Contenido del documento PDF
$pdf->SetFont('Arial','B',10);

$pdf->Text($padd + 32, 32, 'FONDO DE EMPLEADOS DE LA SOCIEDAD PORTUARIA REGIONAL DE BUENAVENTURA S.A.');

$pdf->SetFont('Arial','',10);
$pdf->Text($padd + 8, 44, 'Buenaventura, ' . date("j") . ' de '. $meses[date("n")-1] . ' de ' . date("Y"));
$pdf->Text($padd + 8, 50, 'Reporte de Asistentes a la asamblea ' . $asamblea->nombre);

$pdf->SetFont('Arial','B',10);
$pdf->Text($padd + 8, 62, 'Asociados');
$pdf->Text($padd + 8, 68, 'Identificacion');
$pdf->Text($padd + 40, 68, 'Codigo');
$pdf->Text($padd + 60, 68, 'Nombre');
$pdf->Text($padd + 140, 68, 'Hora ingreso');
//$pdf->Text($padd + 160, 68, 'Participacion');

$y = 76;
$pdf->SetFont('Arial','',10);

foreach ($lista_accionistas as $k => $acc) {  
	$pdf->Text($padd + 8, $y, $acc->documento);
	$pdf->Text($padd + 40, $y, $acc->codbarra_id);
	$pdf->Text($padd + 60, $y, substr($acc->nombre, 0, 35));
    
    $sesion = new Hsesion;
    $sesion->accionista_id = $acc->accionista_id;
    $sesion->asamblea_id = $asamblea->asamblea_id;
    $sesion->find(true);
    $hora = date('H:s:i', strtotime($sesion->h_ingreso.' + 4 hours'));
    
    $pdf->Text($padd + 140, $y, $hora);
	//$pdf->Text($padd + 160, $y, Util::participacion(($acc->cantidad).' %'));
 


 
	$y += 6;
 
 $total_acciones += ($acc->cantidad);

 

	if ($y > 290) {
		$pdf->SetFont('Arial','',8);
		$pdf->Text($padd + 60, $y, "- Pagina {$num_pagina} -");
		$num_pagina++;
		$pdf->AddPage();
		$pdf->SetFont('Arial','',10);
		$y = 15;
	}
}

$y += 6;
$pdf->SetFont('Arial','B',10);
$pdf->Text($padd + 8, $y, 'Delegados');
$y += 6;

$pdf->Text($padd + 8, $y, 'Identificacion');
$pdf->Text($padd + 40, $y, 'Codigo');
$pdf->Text($padd + 60, $y, 'Nombre');
//$pdf->Text($padd + 140, $y, 'Acciones');
//$pdf->Text($padd + 160, $y, 'Participacion');

$y += 6;
$pdf->SetFont('Arial','',10);

foreach ($lista_representantes as $k => $del) {  
	$pdf->Text($padd + 8, $y, $del->documento);
	$pdf->Text($padd + 40, $y, $del->codbarra_id);
	$pdf->Text($padd + 60, $y, substr($del->nombre, 0, 35));
   // $pdf->Text($padd + 140, $y, number_format(($del->cantidad), 0, ',', '.'));
	//$pdf->Text($padd + 160, $y, Util::participacion(($del->cantidad).' %'));

	$y += 6;
 
 $total_acciones += ($del->cantidad);
 
	if ($y > 290) {
		$pdf->SetFont('Arial','',8);
		$pdf->Text($padd + 60, $y, "- Pagina {$num_pagina} -");
		$num_pagina++;
		$pdf->AddPage();
		$pdf->SetFont('Arial','',10);
		$y = 15;
	}
}

$y += 6;
$pdf->Text($padd + 8, $y, 'Total asistentes: ' . (sizeof($lista_accionistas) + sizeof($lista_representantes)) );
$y += 6;
//$pdf->Text($padd + 8, $y, 'Total acciones: ' . number_format($total_acciones, 0, ',', '.') );
$y += 6;
//$pdf->Text($padd + 8, $y, 'Total participacion: ' . Util::participacion($total_acciones) . ' %' );


$pdf->Output($namefile);

readfile($namefile);
@unlink($namefile);

?>
