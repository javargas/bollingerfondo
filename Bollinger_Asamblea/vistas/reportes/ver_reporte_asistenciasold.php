<?
// Clase para construir PDF
define('FPDF_FONTPATH', 'lib/font/');
require('lib/fpdf.php');

// Cabecera del documento
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
header("Content-Type: application/force-download");
header("Content-Description: File Transfer");
header("Content-Disposition: attachment; filename=reporte_asistencias.pdf"); 

$namefile = time().".pdf";
$padd = "30";
$num_pagina = 1;

$pdf=new FPDF();

$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto",
"Septiembre","Octubre","Noviembre","Diciembre");


$pdf=new FPDF();
$pdf->SetTitle("Bollinger Asambleas :: Reporte de Asistencias");
$pdf->SetSubject("Reporte de Asistencias");
$pdf->SetAuthor("John A. Vargas <javargas@gmail.com>");
$pdf->AddPage();

// Contenido del documento PDF
$pdf->SetFont('Arial','B',10);

$pdf->Text($padd + 32, 32, 'SOCIEDAD PORTUARIA REGIONAL DE BUENAVENTURA S.A.');

$pdf->SetFont('Arial','',10);
$pdf->Text($padd + 8, 44, 'Buenaventura, ' . date("j") . ' de '. $meses[date("n")-1] . ' de ' . date("Y"));
$pdf->Text($padd + 8, 50, 'Reporte de Asistentes a la asamblea ' . $_SESSION['asamblea']['nombre']);

$pdf->SetFont('Arial','B',10);
$pdf->Text($padd + 8, 62, 'Accionistas');
$pdf->Text($padd + 8, 68, 'Identificaci�n');
$pdf->Text($padd + 40, 68, 'C�digo');
$pdf->Text($padd + 60, 68, 'Nombre');

$y = 76;
$pdf->SetFont('Arial','',10);

foreach ($lista_accionistas as $k => $acc) {  
	$pdf->Text($padd + 8, $y, $acc->documento);
	$pdf->Text($padd + 40, $y, $acc->codbarra_id);
	$pdf->Text($padd + 60, $y, $acc->nombre);
	$y += 6;
	
	if ($y > 290) {
		$pdf->SetFont('Arial','',8);
		$pdf->Text($padd + 60, $y, "- P�gina {$num_pagina} -");
		$num_pagina++;
		$pdf->AddPage();
		$pdf->SetFont('Arial','',10);
		$y = 15;
	}
}

$y += 6;
$pdf->SetFont('Arial','B',10);
$pdf->Text($padd + 8, $y, 'Delegados');
$y += 6;

$pdf->Text($padd + 8, $y, 'Identificaci�n');
$pdf->Text($padd + 40, $y, 'C�digo');
$pdf->Text($padd + 60, $y, 'Nombre');

$y += 6;
$pdf->SetFont('Arial','',10);

foreach ($lista_representantes as $k => $del) {  
	$pdf->Text($padd + 8, $y, $del->documento);
	$pdf->Text($padd + 40, $y, $del->codbarra_id);
	$pdf->Text($padd + 60, $y, $del->nombre);
	$y += 6;
	
	if ($y > 290) {
		$pdf->SetFont('Arial','',8);
		$pdf->Text($padd + 60, $y, "- P�gina {$num_pagina} -");
		$num_pagina++;
		$pdf->AddPage();
		$pdf->SetFont('Arial','',10);
		$y = 15;
	}
}

$y += 6;
$pdf->Text($padd + 8, $y, 'Total asistentes: ' . (sizeof($lista_accionistas) + sizeof($lista_representantes)) );

$pdf->Output($namefile);

readfile($namefile);
@unlink($namefile);

?>