<?
// Clase para construir PDF
define('FPDF_FONTPATH', '../lib/font/');
require('../lib/fpdf.php');

// Cabecera del documento
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
header("Content-Type: application/force-download");
header("Content-Description: File Transfer");
header("Content-Disposition: attachment; filename=reporte_poderes.pdf"); 

$namefile = time().".pdf";
$padd = "30";
$num_pagina = 1;

$pdf=new FPDF();

$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto",
"Septiembre","Octubre","Noviembre","Diciembre");


$pdf=new FPDF();
$pdf->SetTitle("Bollinger Asambleas :: Reporte de Asistencias");
$pdf->SetSubject("Reporte de Asistencias");
$pdf->SetAuthor("John A. Vargas <javargas@gmail.com>");
$pdf->AddPage();

// Contenido del documento PDF
$pdf->SetFont('Arial','B',10);

$pdf->Text($padd + 32, 32, 'FONDO DE EMPLEADOS DE LA SOCIEDAD PORTUARIA REGIONAL DE BUENAVENTURA S.A.');

$pdf->SetFont('Arial','',10);
$pdf->Text($padd + 8, 44, 'Buenaventura, ' . date("j") . ' de '. $meses[date("n")-1] . ' de ' . date("Y"));
$pdf->Text($padd + 8, 50, 'Reporte de poderes de registrados a la asamblea: ' . $_SESSION['asamblea']['nombre']);

$pdf->SetFont('Arial','B',10);
$pdf->Text($padd + 8, 62, 'Asociados');
$pdf->Text($padd + 8, 68, 'Identificacion');
$pdf->Text($padd + 40, 68, 'Codigo');
$pdf->Text($padd + 60, 68, 'Nombre');

$y = 76;
$pdf->SetFont('Arial','',10);

foreach ($lista_delegados as $k => $rep) {  
	$pdf->Text($padd + 8, $y, $rep['delegado']->documento);
	$pdf->Text($padd + 40, $y, $rep['delegado']->codbarra_id);
	$pdf->Text($padd + 60, $y, $rep['delegado']->nombre);
	$y += 6;
	
	 foreach ($rep['afiliados'] as $afl) { 
		$pdf->Text($padd + 40, $y, $afl->documento);
		$pdf->Text($padd + 60, $y, $afl->nombre);
		$y += 6;
	
		if ($y > 285) {
			$pdf->SetFont('Arial','',8);
			$pdf->Text($padd + 60, $y, "- Pagina {$num_pagina} -");
			$num_pagina++;
			$pdf->AddPage();
			$pdf->SetFont('Arial','',10);
			$y = 15;
		}
	}
	
	if ($y > 285) {
		$pdf->SetFont('Arial','',8);
		$pdf->Text($padd + 60, $y, "- Pagina{$num_pagina} -");
		$num_pagina++;
		$pdf->AddPage();
		$pdf->SetFont('Arial','',10);
		$y = 15;
	}
	
	$y += 6;
}

$pdf->Output($namefile);

readfile($namefile);
@unlink($namefile);

?> 