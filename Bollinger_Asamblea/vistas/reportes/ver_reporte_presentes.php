<?
// Clase para construir PDF
define('FPDF_FONTPATH', '../lib/font/');
require('../lib/fpdf.php');

// Cabecera del documento
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
header("Content-Type: application/force-download");
header("Content-Description: File Transfer");
header("Content-Disposition: attachment; filename=reporte_presentes.pdf"); 

$namefile = time().".pdf";
$padd = "10";
$num_pagina = 1;

$pdf=new FPDF();

$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto",
"Septiembre","Octubre","Noviembre","Diciembre");


$pdf=new FPDF();
$pdf->SetTitle("Bollinger Asambleas :: Reporte de Presentes");
$pdf->SetSubject("Reporte de Asistencias");
$pdf->SetAuthor("John A. Vargas <javargas@gmail.com>");
$pdf->AddPage();

// Contenido del documento PDF
$pdf->SetFont('Arial','B',10);

$pdf->Text($padd + 32, 32, NOMBRE_EMPRESA);

$pdf->SetFont('Arial','',10);
$pdf->Text($padd + 8, 44, 'Buenaventura, ' . date("j") . ' de '. $meses[date("n")-1] . ' de ' . date("Y"));
$pdf->Text($padd + 8, 50, 'Reporte de presentes en sesion a la asamblea ' . $_SESSION['asamblea']['nombre']);

$pdf->SetFont('Arial','B',10);
$pdf->Text($padd, 62, 'Nombre');
$pdf->Text($padd + 110, 62, 'Identificacion');
//$pdf->Text($padd + 140, 62, 'Acciones');
//$pdf->Text($padd + 160, 62, 'Hora de ingreso');
//$pdf->Text($padd + 160, 62, 'Participacion');

$y = 68;
$pdf->SetFont('Arial','',10);

$total_acciones = 0;

foreach ($lista_presentes as $k => $acc) {  
	$pdf->Text($padd, $y, $acc['nombre']);
	$pdf->Text($padd + 110, $y, number_format($acc['documento'], 0, ',', '.'));
	//$pdf->Text($padd + 140, $y, number_format($acc['cantidad'], 0, ',', '.'));
	//$pdf->Text($padd + 160, $y, Util::participacion($acc['cantidad']).' %');
	$y += 6;
	
	$total_acciones += $acc['cantidad'];
	
	if ($y > 290) {
		$pdf->SetFont('Arial','',8);
		$pdf->Text($padd + 60, $y, "- Pagina {$num_pagina} -");
		$num_pagina++;
		$pdf->AddPage();
		$pdf->SetFont('Arial','',10);
		$y = 15;
	}
}

$y += 6;
$pdf->Text($padd + 8, $y, 'Total presentes: ' . sizeof($lista_presentes) );

	if ($y > 290) {
		$pdf->SetFont('Arial', '', 8);
		$pdf->Text($padd + 60, $y, "- Pagina {$num_pagina} -");
		$num_pagina++;
		$pdf->AddPage();
		$pdf->SetFont('Arial', '', 10);
		$y = 15;
	}

//$y += 6;
//$pdf->Text($padd + 8, $y, 'Total acciones: ' . number_format($total_acciones, 0, ',', '.') );


	if ($y > 290) {
		$pdf->SetFont('Arial', '', 8);
		$pdf->Text($padd + 60, $y, "- Pagina {$num_pagina} -");
		$num_pagina++;
		$pdf->AddPage();
		$pdf->SetFont('Arial', '', 10);
		$y = 15;
	}

$y += 6;
$pdf->Text($padd + 8, $y, 'Total participacion: ' . Util::participacion($total_acciones) . ' %' );

	if ($y > 290) {
		$pdf->SetFont('Arial', '', 8);
		$pdf->Text($padd + 60, $y, "- Pagina {$num_pagina} -");
		$num_pagina++;
		$pdf->AddPage();
		$pdf->SetFont('Arial', '', 10);
		$y = 15;
	}

$y += 12;
$pdf->SetFont('Arial', '', 8);
$pdf->Text($padd + 8, $y, "Usuario: " . $_SESSION['usuario']['nombre']);
$y += 3;
$pdf->Text($padd + 8, $y, $dias[strftime("%w")] . ", " . $meses[date("n")-1] . date(" j") . " de " . date(" Y g:i a"));

$pdf->Output($namefile);

readfile($namefile);
@unlink($namefile);

?>