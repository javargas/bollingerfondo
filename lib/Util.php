<?php
// Util.php
/*
 * Clase con funciones de utilidad
 * 
 * Manipula los metodos generales para la 
 * administracion de la asamblea
 *
 * @author John A. Vargas <javargas@gmail.com>
 * @date Marzo 3 de 2004
 */
class Util
{
	/*
	 * Calcula el porcentaje de participaci�n dada la cantidad y el total de acciones
	 *
	 * @param int $total Cantidad total de acciones
	 *        int $cantidad Cantidad de acciones.
	 * @return int Porcentaje de acciones
	 */

	function participacion($cantidad)
	{
	  
    $accionista = new Accionista;
    $accionista->clase = 'A';
    $accionista->find(true);
    
    $total = $accionista->N; 
     	
		$participacion = round(100 * $cantidad / $total , 2);
		return number_format($participacion, 2);
	}
	
	/*
	 * Menciona la el n�mero de items encontrados y 
	 * desde donde hasta donde muestra los registros.
	 *
	 * @param array Hash con los datos del paginador 
	 * @return string Cadena con el mensaje cabecera de la lista
	 */
	function encontrados($data)
	{
		if (!$data)
			$mensaje = "No se encontraron registros!!<br>";
		else {
			$mensaje =  '<br>Encontrados <b>'.$data['numrows'].'</b> registros <br>';
			$mensaje .= 'Mostrando desde <b>'.$data['from'].'</b> ';
			$mensaje .= 'hasta <b>'.$data['to'].'</b><br>';
		}
		
		return $mensaje;
	}
	
	/*
	 * Menciona la lista de p�ginas para mostrar los
	 * demas registros encontrados.
	 *
	 * @param array Hash con los datos del paginador 
	 *        string Cadena donde debe retornar el link de p�gina
	 *        int P�gina en la que se encuentra la lista.
	 * @return string Cadena con la lista de links para las p�ginas.
	 */
	function paginar($data, $url, $from)
	{
		if ($data && is_array($data)) {
			$link .= '<a href="'.$url.'&from='.$data['prev'].'" class="ident">&lt;</a>&nbsp;&nbsp;';
	
			foreach($data['pages'] as $i => $from) { 
				$link .= '&nbsp;<a href="'.$url.'&from='.$from.'" class="ident">';
				$link .= ($i == $data['current']) ? '<b><font color="red" size="2">'.$i.'</b>' : $i ;
				$link .= '</a>&nbsp;';
			 } 
			 
			$link .= '&nbsp;&nbsp;<a href="'.$url.'&from='.$data['next'].'" class="ident">&gt;</a>';
		} else
			$link = "";
		
		return $link;
	}
	
	function mostrar_pastel($data, $legends)
	{
		
		// Create the Pie Graph.
		$graph = new PieGraph(350,200,"auto");
		$graph->SetShadow();
		
		// Set A title for the plot
		$graph->title->Set("Example 1 3D Pie plot");
		$graph->title->SetFont(FF_VERDANA,FS_BOLD,18); 
		$graph->title->SetColor("darkblue");
		$graph->legend->Pos(0.1,0.2);
		
		// Create pie plot
		$p1 = new PiePlot3d($data);
		$p1->SetTheme("sand");
		$p1->SetCenter(0.4);
		$p1->SetAngle(30);
		$p1->value->SetFont(FF_ARIAL,FS_NORMAL,12);
		$p1->SetLegends($legends);
		
		$graph->Add($p1);
		$graph->Stroke();
	}
	/*
}

class Monto{*/

function unidad($numuero){
	switch ($numuero)
	{
		case 9:
		{
		$numu = "NUEVE";
		break;
		}
		case 8:
		{
		$numu = "OCHO";
		break;
		}
		case 7:
		{
		$numu = "SIETE";
		break;
		} 
		case 6:
		{
		$numu = "SEIS";
		break;
		} 
		case 5:
		{
		$numu = "CINCO";
		break;
		} 
		case 4:
		{
		$numu = "CUATRO";
		break;
		} 
		case 3:
		{
		$numu = "TRES";
		break;
		} 
		case 2:
		{
		$numu = "DOS";
		break;
		} 
		case 1:
		{
		$numu = "UN";
		break;
		} 
		case 0:
		{
		$numu = "";
		break;
		} 
	}
	return $numu; 
	}

function decena($numdero){

	if ($numdero >= 90 && $numdero <= 99)
	{
	$numd = "NOVENTA ";
	if ($numdero > 90)
	$numd = $numd."Y ".(Util::unidad($numdero - 90));
	}
	else if ($numdero >= 80 && $numdero <= 89)
	{
	$numd = "OCHENTA ";
	if ($numdero > 80)
	$numd = $numd."Y ".(Util::unidad($numdero - 80));
	}
	else if ($numdero >= 70 && $numdero <= 79)
	{
	$numd = "SETENTA ";
	if ($numdero > 70)
	$numd = $numd."Y ".(Util::unidad($numdero - 70));
	}
	else if ($numdero >= 60 && $numdero <= 69)
	{
	$numd = "SESENTA ";
	if ($numdero > 60)
	$numd = $numd."Y ".(Util::unidad($numdero - 60));
	}
	else if ($numdero >= 50 && $numdero <= 59)
	{
	$numd = "CINCUENTA ";
	if ($numdero > 50)
	$numd = $numd."Y ".(Util::unidad($numdero - 50));
	}
	else if ($numdero >= 40 && $numdero <= 49)
	{
	$numd = "CUARENTA ";
	if ($numdero > 40)
	$numd = $numd."Y ".(Util::unidad($numdero - 40));
	}
	else if ($numdero >= 30 && $numdero <= 39)
	{
	$numd = "TREINTA ";
	if ($numdero > 30)
	$numd = $numd."Y ".(Util::unidad($numdero - 30));
	}
	else if ($numdero >= 20 && $numdero <= 29)
	{
	if ($numdero == 20)
	$numd = "VEINTE ";
	else
	$numd = "VEINTI".(Util::unidad($numdero - 20));
	}
	else if ($numdero >= 10 && $numdero <= 19)
	{
		switch ($numdero){
			case 10:
			{
			$numd = "DIEZ ";
			break;
			}
			case 11:
			{ 
			$numd = "ONCE ";
			break;
			}
			case 12:
			{
			$numd = "DOCE ";
			break;
			}
			case 13:
			{
			$numd = "TRECE ";
			break;
			}
			case 14:
			{
			$numd = "CATORCE ";
			break;
			}
			case 15:
			{
			$numd = "QUINCE ";
			break;
			}
			case 16:
			{
			$numd = "DIECISEIS ";
			break;
			}
			case 17:
			{
			$numd = "DIECISIETE ";
			break;
			}
			case 18:
			{
			$numd = "DIECIOCHO ";
			break;
			}
			case 19:
			{
			$numd = "DIECINUEVE ";
			break;
			}
		} 
	}
	else	
	$numd = Util::unidad($numdero);
	return $numd;
}

function centena($numc){
	if ($numc >= 100)
	{
	if ($numc >= 900 && $numc <= 999)
	{
	$numce = "NOVECIENTOS ";
	if ($numc > 900)
	$numce = $numce.(Util::decena($numc - 900));
	}
	else if ($numc >= 800 && $numc <= 899)
	{
	$numce = "OCHOCIENTOS ";
	if ($numc > 800)
	$numce = $numce.(Util::decena($numc - 800));
	}
	else if ($numc >= 700 && $numc <= 799)
	{
	$numce = "SETECIENTOS ";
	if ($numc > 700)
	$numce = $numce.(Util::decena($numc - 700));
	}
	else if ($numc >= 600 && $numc <= 699)
	{
	$numce = "SEISCIENTOS ";
	if ($numc > 600)
	$numce = $numce.(Util::decena($numc - 600));
	}
	else if ($numc >= 500 && $numc <= 599)
	{
	$numce = "QUINIENTOS ";
	if ($numc > 500)
	$numce = $numce.(Util::decena($numc - 500));
	}
	else if ($numc >= 400 && $numc <= 499)
	{
	$numce = "CUATROCIENTOS ";
	if ($numc > 400)
	$numce = $numce.(Util::decena($numc - 400));
	}
	else if ($numc >= 300 && $numc <= 399)
	{
	$numce = "TRESCIENTOS ";
	if ($numc > 300)
	$numce = $numce.(Util::decena($numc - 300));
	}
	else if ($numc >= 200 && $numc <= 299)
	{
	$numce = "DOSCIENTOS ";
	if ($numc > 200)
	$numce = $numce.(Util::decena($numc - 200));
	}
	else if ($numc >= 100 && $numc <= 199)
	{
	if ($numc == 100)
	$numce = "CIEN ";
	else
	$numce = "CIENTO ".(Util::decena($numc - 100));
	}
	}
	else
	$numce = Util::decena($numc);
	
	return $numce; 
}

function miles($nummero){
	if ($nummero >= 1000 && $nummero < 2000){
		$numm = "MIL ".(Util::centena($nummero%1000));
	}
	if ($nummero >= 2000 && $nummero <10000){
		$numm = Util::unidad(Floor($nummero/1000))." MIL ".(Util::centena($nummero%1000));
	}
	if ($nummero < 1000)
		$numm = Util::centena($nummero);
	
	return $numm;
}

function decmiles($numdmero){
	if ($numdmero == 10000)
	$numde = "DIEZ MIL";
	if ($numdmero > 10000 && $numdmero <20000){
	$numde = Util::decena(Floor($numdmero/1000))."MIL ".(Util::centena($numdmero%1000)); 
	}
	if ($numdmero >= 20000 && $numdmero <100000){
	$numde = Util::decena(Floor($numdmero/1000))." MIL ".(Util::miles($numdmero%1000)); 
	} 
	
	if ($numdmero < 10000)
	$numde = Util::miles($numdmero);
	
	return $numde;
} 

function cienmiles($numcmero){
	if ($numcmero == 100000)
	$num_letracm = "CIEN MIL";
	if ($numcmero >= 100000 && $numcmero <1000000){
	$num_letracm = Util::centena(Floor($numcmero/1000))." MIL ".(Util::centena($numcmero%1000)); 
	}
	if ($numcmero < 100000)
	$num_letracm = Util::decmiles($numcmero);
	return $num_letracm;
} 

function millon($nummiero){
	if ($nummiero >= 1000000 && $nummiero <2000000){
	$num_letramm = "UN MILLON ".(Util::cienmiles($nummiero%1000000));
	}
	if ($nummiero >= 2000000 && $nummiero <10000000){
	$num_letramm = Util::unidad(Floor($nummiero/1000000))." MILLONES ".(Util::cienmiles($nummiero%1000000));
	}
	if ($nummiero < 1000000)
	$num_letramm = Util::cienmiles($nummiero);
	
	return $num_letramm;
} 

function decmillon($numerodm){
	if ($numerodm == 10000000)
	$num_letradmm = "DIEZ MILLONES";
	if ($numerodm > 10000000 && $numerodm <20000000){
	$num_letradmm = Util::decena(Floor($numerodm/1000000))."MILLONES ".(Util::cienmiles($numerodm%1000000)); 
	}
	if ($numerodm >= 20000000 && $numerodm <100000000){
	$num_letradmm = Util::decena(Floor($numerodm/1000000))." MILLONES ".(Util::millon($numerodm%1000000)); 
	}
	if ($numerodm < 10000000)
	$num_letradmm = Util::millon($numerodm);
	
	return $num_letradmm;
}

function cienmillon($numcmeros){
	if ($numcmeros == 100000000)
	$num_letracms = "CIEN MILLONES";
	if ($numcmeros >= 100000000 && $numcmeros <1000000000){
	$num_letracms = Util::centena(Floor($numcmeros/1000000))." MILLONES ".(Util::millon($numcmeros%1000000)); 
	}
	if ($numcmeros < 100000000)
	$num_letracms = Util::decmillon($numcmeros);
	return $num_letracms;
} 

function milmillon($nummierod){
	if ($nummierod >= 1000000000 && $nummierod <2000000000){
	$num_letrammd = "MIL ".(Util::cienmillon($nummierod%1000000000));
	}
	if ($nummierod >= 2000000000 && $nummierod <10000000000){
	$num_letrammd = Util::unidad(Floor($nummierod/1000000000))." MIL ".(Util::cienmillon($nummierod%1000000000));
	}
	if ($nummierod < 1000000000)
	$num_letrammd = Util::cienmillon($nummierod);
	
	return $num_letrammd;
} 

function numero_a_letras($numero){
	if ($numero > 0)
		$numf = Util::milmillon($numero);
	else
		$numf = "CERO";
	return $numf;
}
}//fin de la clase util
?>