// JavaScript Document

/* Funciones fachada para la biblioteca de ajax */
// Instaciacion y funcion para desplegar mensaje de alerta o error

messageObj = new DHTML_modalMessage();	// We only create one object of this class
messageObj.setShadowOffset(5);	// Large shadow


function displayMessage(url, width, height)
{
	
	messageObj.setSource(url);
	messageObj.setCssClassMessageBox(false);
	messageObj.setSize(width, height);
	messageObj.setShadowDivVisible(true);	// Enable shadow for these boxes
	messageObj.display();
}


function closeMessage()
{
	messageObj.close();	
}
