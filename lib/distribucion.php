<?
/**
 * Clase Distribución:
 * Representa el cromosoma de uan distribución.
 *
 * @author 
 * @date Octubre 25 de 2004
 */
 class Distribución
 {
 	/* Atributos */
	var $lista_ofertas;     // array
	var $cantidad_orecida;  // integer 
	var $menor;
	var $mayor;
	
	/*
	 * Método contructor
	 *
	 * @param array Lista de ofertas iniciales.
	 */
	 function Distribucion($ofertas)
	 {
	 	$this->lista_ofertas = new ArrayList($ofertas);
		$this->cantidad_ofrecida = array_sum($ofertas);
	 }
	 
	 /*
	  * Función objetivo
	  *
	  * @return boolean Verdadero si es apto, falso si no.
	  */
	  function objetivo()
	  {
	  	// Condición I: Todos las ofertas deben ser enteros.
		foreach ($this->ofertas as $oferta)
			if (!is_int($oferta))return false;
			
		// Condición II: La suma de todas las ofertas debe ser igual a lo ofrecido.
		return (array_sum($this->ofertas) === $this->cantidad_ofrecida);	
	  }
	  
	  /*
	   * Clasificación de la distribución en dos grupos:
	   * - Mayor: Ofertas de accionistas con accionies mayores o iguales a uno.
	   * - Menor: Ofertas de accionistas con acciones menores que uno.
	   */
	  function clasificar()
	  {
	  	$this->mayor = array();
		$this->menor = array();
	  	foreach ($this->ofertas as $k => $v) {
			if ($v >= 1)
				$this->mayor[$k] = $v - intval($v);
			else
				$this->menor[$k] = $v;
		}
		
		return;
	  }
	  
	  /*
	   * Obtener Descendencia.
	   * Primera población de los encajes entre Mayor y Menor.
	   */
	   function obtenerDescendencia()
	   {
		   	$poblacion = array();
			$listos = array();
	   		foreach ($this->mayor as $k => $maximo) {
				$poblacion[$k] = array();
				array_push($listos, $k);
				foreach ($this->menor as $m => $minimo)
					if ($maximo + $minimo == 1) {
	  					array_push($poblacion[$k], $m);
						array_push($listos, $m);
					}
			}
			/*
			foreach ($poblacion as $k => $individuo) {
				
			}	*/
		}   	   
 }
?>