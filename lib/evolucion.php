<?
/**
 * Clase Evolución:
 * Reliza cruces, mutaciones y selecciona el mejor de sus generaciones.
 *
 * @author 
 * @date Octubre 25 de 2004
 */
 class Evolucion
 {
 	/* Atributos */
	var $num_generacion;     // int
	var $con_entero;		 // array Conjunto de oferenter con acciones mayor o igual a uno.
	var $sin_entero;		 //  array
	
	/*
	 * Método contructor
	 *
	 * @param array Lista de ofertas iniciales.
	 */
	 function Evolucion()
	 {
	 	// Inicialización de los atributos del proceso de evolución.
	 	$this->num_generacion = 0;
	 	$this->con_entero = array();
	 	$this->sin_entero = array();
	 }
	 
	 function obtenerOptimo($datos)
	 {
	 	$distribucion = new Distribucion($datos);
		
		$distribucion->clasificar();
	 }
 }
?>