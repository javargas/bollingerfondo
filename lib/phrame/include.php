<?php
include(PATH_PHRAME . 'util/Object.php');
include(PATH_PHRAME . 'util/ArrayList.php');
include(PATH_PHRAME . 'util/HashMap.php');
include(PATH_PHRAME . 'util/Stack.php');
include(PATH_PHRAME . 'util/ListIterator.php');
include(PATH_PHRAME . 'ext/Xml.php');
include(PATH_PHRAME . 'Constants.php');
include(PATH_PHRAME . 'Action.php');
include(PATH_PHRAME . 'ActionController.php');
include(PATH_PHRAME . 'ActionForm.php');
include(PATH_PHRAME . 'ActionForward.php');
include(PATH_PHRAME . 'ActionMapping.php');
include(PATH_PHRAME . 'MappingManager.php');
?>
