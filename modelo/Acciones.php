<?php
/**
 * Table Definition for acciones
 */
require_once '../lib/DataObject.php';

class Acciones extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'acciones';                        // table name
    var $accionista_id;                   // string(15)  not_null primary_key
    var $estadoacciones_id;               // string(2)  not_null primary_key
    var $cantidad;                        // real(17)  

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Acciones',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>