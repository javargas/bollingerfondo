<?php
/**
 * Table Definition for acciones_libres
 */
require_once '../lib/DataObject.php';

class Acciones_libres extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'acciones_libres';                 // table name
    var $id_reg_acc_libre;                // int(50)  not_null primary_key auto_increment
    var $periodo_id;                      // string(6)  not_null
    var $fecha;                           // date(10)  
    var $cantidad_disp;                   // real(17)  
    var $valor;                           // real(17)  
    var $numdocumento;                    // string(10)  
    var $accionista_id;                   // string(15)  not_null
    var $comprador_id;                    // string(15)  not_null
    var $observacion;                     // string(80)  
    var $cant_acept;                      // real(17)  

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Acciones_libres',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>