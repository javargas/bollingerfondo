<?php
/**
 * Table Definition for accionista
 */
require_once '../lib/DataObject.php';

class Accionista extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'accionista';                      // table name
    var $accionista_id;                   // int(20)  not_null primary_key unique_key auto_increment
    var $nombre;                          // string(50)  
    var $direccion;                       // string(50)  
    var $telefono;                        // string(20)  
    var $fax;                             // string(15)  
    var $email;                           // string(60)  
    var $cantidad;                        // int(11)  
    var $enrepresentacion;                // int(11)  
    var $participacion;                   // real(12)  
    var $clase;                           // string(1)  
    var $estado;                          // string(1)  
    var $documento;                       // string(50)  
    var $tipo_documento;                  // string(2)  
    var $pedir_actualizacion;             // int(3)  not_null unsigned
    var $representante_legal;             // string(255)  
    var $cedula_representante_legal;      // string(10)  
    var $razon_social;                    // string(255)  
    var $nit;                             // string(12)  
    var $representar;                     // int(10)  unsigned
    var $empresa;                         // string(255)  
    var $ciudad;                          // string(50)  
	var $referencia;                      // string(11)  
	var $folio;                      // string(11)  
	var $libro;                      // string(11)  
	
    /* ZE2 compatibility trick*/
    function __clone() { return $this; }
    
    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Accionista',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>