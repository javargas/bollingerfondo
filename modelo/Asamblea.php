<?php
/**
 * Table Definition for asamblea
 */
require_once '../lib/DataObject.php';

class Asamblea extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'asamblea';                        // table name
    var $asamblea_id;                     // int(11)  not_null primary_key unique_key
    var $nombre;                          // string(50)  
    var $estado;                          // int(1)  unsigned
	var $dividendos;
	var $fecha;
	var $acta;

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Asamblea',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>