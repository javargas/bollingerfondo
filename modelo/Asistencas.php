<?php
/**
 * Table Definition for asistencas
 */
require_once '../lib/DataObject.php';

class Asistencas extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'asistencas';                      // table name
    var $asamblea_id;                     // int(11)  not_null
    var $accionista_id;                   // string(15)  not_null
    var $codbarra_id;                     // string(20)  
    var $consecutivo;                     // real(6)  

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Asistencas',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>