<?php
/**
 * Table Definition for asistencia
 */
require_once '../lib/DataObject.php';

class Asistencia extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'asistencia';                      // table name
    var $asamblea_id;                     // int(11)  not_null primary_key multiple_key
    var $accionista_id;                   // string(15)  not_null primary_key
    var $codbarra_id;                     // string(20)  
    var $consecutivo;                     // real(6)  

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Asistencia',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>