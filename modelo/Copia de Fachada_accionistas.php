<?
// Clase del patron Fachada

/*
 * Fachada_asamblea
 * 
 * Manipula los metodos generales para la 
 * administracion de la asamblea
 *
 * @author John A. Vargas <javargas@gmail.com>
 * @date Marzo 3 de 2004
 */
class Fachada_accionistas
{

	/* 
	 * Lista los accionistas del sistema 
	 *
	 * @param char $clase Clase de accionista (R => Representante, A => Accionista, V => Visitante)
	 *        string $nombre Nombre del accionista a buscar
	 * @return array Listado de los accionistas
	 * @static
	 */
	function listar_accionistas($clase, $from, $order ,$nombre, $delegar = false,
								 $documento, $codigo, $seccion = "ofertas")
	{
		$accionista = new Accionista;
		
		if ($nombre != '')
			$accionista->whereAdd("LOWER(accionista.nombre) LIKE LOWER('%$nombre%')");
			
		if ($codigo != '')
			$accionista->whereAdd("accionista.codigo = '$codigo'");
			
		if ($documento != '')
			$accionista->whereAdd("accionista.documento = '$documento'");
			
		if ($delegar) {
			$representacion = new Representante;
			$accionista->joinAdd($representacion, 'LEFT');
			$accionista->selectAs();
			$accionista->whereAdd("representante.representante is NULL");
		}
			
		if ($clase != '')
			$accionista->whereAdd("accionista.clase = '$clase'");
		
//		$accionista->whereAdd("accionista.cantidad <> 0");	
			
		$accionista->find(); //echo $accionista->print_query();exit;
	    $data = DB_Pager::getData($from, LIMIT, $accionista->N, MAXPAGES);
		
		if ($order == '') $order = 'accionista.nombre ASC';

		// Si se hace la busqueda en la parte de asamblea se saca como cantidad del 
		// accionista el total de acciones que representa
		if ($seccion == "asamblea") {			
			// dinamicamente crear enlaces entre tablas para poder realizar un join
	        global $_DB_DATAOBJECT;
			$_DB_DATAOBJECT['LINKS'][$accionista->_database]['representante']['representante'] = 'accionista:accionista_id';
			unset($_DB_DATAOBJECT['LINKS'][$accionista->_database]['representante']['accionista_id']);

			$representacion = new Representante;
			$accionista->joinAdd($representacion, 'LEFT');
			$accionista->selectAs();
			$data_select = $accionista->_query['data_select'];
			$accionista->selectAdd("SUM(representante.cantidad) AS cantidad_representada");
			$accionista->groupBy(implode(",", array_keys($accionista->table())));
		}
		
		$accionista->orderBy($order);
		$accionista->limit($from, LIMIT);
		$accionista->find();  //echo $accionista->print_query(); exit;
		
		$lista_accionistas = array();
		while ($accionista->fetch()) {				
			if ($seccion == "asamblea") {	
				$accionista->cantidad += $accionista->cantidad_representada;	
			} 
			
			if ($accionista->cantidad != 0)
				array_push($lista_accionistas, $accionista->toArray());
		}
//		print_r($lista_accionistas); exit;
		return array("lista" => $lista_accionistas, "data" => $data);
	}

	/* 
	 * Adiciona un accionista a la asamblea
	 *
	 * @return array Listado de los accionistas
	 * @static
	 */
	function adicionar_accionista($documento, $tipo_documento, $nombre,
								  $direccion, $telefono, $fax, $clase, $cantidad = null,
								  $representante_legal, $cedula, $razon_social, $nit, $ciudad,
								  $empresa, $email, $referencia)
	{
	
		/* Verificacion de los accionistas duplicados */
		/*$duplicado = 0;
		$acci_dupli = new Accionista;
		$acci_dupli->documento = $documento;
		if ($acci_dupli->find()){
			$_SESSION['mensaje'] = "El accionista ya existe. ";
			$duplicado = 1;
		}*/
		/* Verificacion de los accionistas duplicados */	
	
		$accionista = new Accionista;
		
		$accionista->documento = $documento;
		$accionista->tipo_documento = $tipo_documento;
		$accionista->nombre = $nombre;
		$accionista->direccion = $direccion;
		$accionista->telefono = $telefono;
		$accionista->fax = $fax;
		$accionista->cantidad = $cantidad;	
		$accionista->clase = $clase;
		$accionista->estado = 'a';	
		$accionista->representante_legal = $representante_legal;
		$accionista->cedula_representante_legal = $cedula;	
		$accionista->razon_social = $razon_social;
		$accionista->nit = $nit;
		$accionista->ciudad = $ciudad;
		$accionista->empresa = $empresa;
		$accionista->email = $email;
		$accionista->referencia = $referencia;

		if ($cantidad != null)
			$accionista->participacion = Util::participacion($cantidad);
		
		
		//if(!$duplicado)		/*verificacion del accionista duplicado*/
			$accionista->insert(); 
		
		return $accionista->accionista_id;
	}
	
	/* 
	 * Modifica un accionista del sistema
	 *
	 * @return array Listado de los accionistas
	 * @static
	 */
	function modificar_accionistas($accionista_id, $documento, $nombre, $direccion,
								 $telefono, $fax, $estado, $codigo, $cantidad = null,
								  $representante_legal, $cedula, $razon_social, $nit, 
								  $ciudad, $empresa, $email, $referencia, $folio, $libro, $representar)
	{
		$accionista = new Accionista;
		
		$logs = array();

		$accionista->get($accionista_id);
		
		if ($nombre != $accionista->nombre && $nombre != '') {
			$logs[] = array('campo' => 'nombre', 'nuevo' => $nombre, 'anterior' => $accionista->nombre);
		}
		
		if ($direccion != $accionista->direccion && $direccion != '') {
			$logs[] = array('campo' => 'direccion', 'nuevo' => $direccion, 'anterior' => $accionista->direccion);
		}
		
		if ($telefono != $accionista->telefono && $telefono != '') {
			$logs[] = array('campo' => 'telefono', 'nuevo' => $telefono, 'anterior' => $accionista->telefono);
		}
		
		if ($fax != $accionista->fax && $fax != '') {
			$logs[] = array('campo' => 'fax', 'nuevo' => $fax, 'anterior' => $accionista->fax);
		}
		
		if ($estado != $accionista->estado && $estado != '') {
			$logs[] = array('campo' => 'estado', 'nuevo' => $estado, 'anterior' => $accionista->estado);
		}
		
		if ($representante_legal != $accionista->representante_legal && $representante_legal != '') {
			$logs[] = array('campo' => 'representante_legal', 'nuevo' => $representante_legal, 'anterior' => $accionista->representante_legal);
		}
		
		if ($cedula_representante_legal != $accionista->cedula_representante_legal && $cedula_representante_legal != '') {
			$logs[] = array('campo' => 'cedula_representante_legal', 'nuevo' => $cedula_representante_legal, 'anterior' => $accionista->cedula_representante_legal);
		}
		
		if ($razon_social != $accionista->razon_social && $razon_social != '') {
			$logs[] = array('campo' => 'razon_social', 'nuevo' => $razon_social, 'anterior' => $accionista->razon_social);
		}
		
		if ($nit != $accionista->nit && $nit != '') {
			$logs[] = array('campo' => 'nit', 'nuevo' => $nit, 'anterior' => $accionista->nit);
		}
		
		if ($ciudad != $accionista->ciudad && $ciudad != '') {
			$logs[] = array('campo' => 'ciudad', 'nuevo' => $ciudad, 'anterior' => $accionista->ciudad);
		}
		
		if ($empresa != $accionista->empresa && $empresa != '') {
			$logs[] = array('campo' => 'empresa', 'nuevo' => $empresa, 'anterior' => $accionista->empresa);
		}
		
		if ($email != $accionista->email && $email != '') {
			$logs[] = array('campo' => 'email', 'nuevo' => $email, 'anterior' => $accionista->email);
		}
		
		if ($referencia != $accionista->referencia && $referencia != '') {
			$logs[] = array('campo' => 'referencia', 'nuevo' => $referencia, 'anterior' => $accionista->referencia);
		}
		
		if ($folio != $accionista->folio && $folio != '') {
			$logs[] = array('campo' => 'folio', 'nuevo' => $folio, 'anterior' => $accionista->folio);
		}
		
		if ($libro != $accionista->libro && $libro != '') {
			$logs[] = array('campo' => 'libro', 'nuevo' => $libro, 'anterior' => $accionista->libro);
		}
		
		$accionista->nombre = $nombre;
		$accionista->direccion = $direccion;
		$accionista->telefono = $telefono;
		$accionista->fax = $fax;
		$accionista->cantidad = $cantidad;
		$accionista->estado = $estado;	
		$accionista->representante_legal = $representante_legal;
		$accionista->cedula_representante_legal = $cedula;	
		$accionista->razon_social = $razon_social;
		$accionista->nit = $nit;
		$accionista->ciudad = $ciudad;
		$accionista->empresa = $empresa;
		$accionista->email = $email;
		$accionista->referencia = $referencia;
		$accionista->folio = $folio;
		$accionista->libro = $libro;
		$accionista->representar = $representar;
		
		//$_SESSION['mensaje'] = "El accionista fue actualizado con �xito!!".$estado;
		if ($cantidad != null)
			$accionista->participacion = Util::participacion($cantidad);

		if ($accionista->update()) { //print_r($accionista);
			/* Rastreo para logs de auditoria */
			if ($_SESSION['log_rastreo'] & 2) { 
				$log = new Historicos;
				$log->fecha_hora = date("Y-m-d H:i:s");
				$log->usuario = $_SESSION['usuario']['nombre'];
				$log->tipo_transaccion = "Modificar accionistas";
				$log->accionista_id = $accionista_id;
				
				foreach ($logs as $cambio) {
					$log->campo_modificado = $cambio['campo'];
					$log->nuevo_valor = $cambio['nuevo'];
					$log->valor_anterior = $cambio['anterior'];
					$log->insert(); //print_r($log); exit;
				}
			} 
			/* Fin de rastreo de log */
		}
		
		/** Asignar codigo de barra **/
		if ($codigo != '') {
			$asistencia = new Asistencia;
			$asistencia->accionista_id = $accionista_id;
			$asistencia->asamblea_id = $_SESSION['asamblea']['asamblea_id'];
			
			if ($asistencia->find(true)) {
				$asistencia->codbarra_id = $codigo;
				$asistencia->update();
			} else {
				$asistencia->accionista_id = $accionista_id;
				$asistencia->asamblea_id = $_SESSION['asamblea']['asamblea_id'];
				$asistencia->codbarra_id = $codigo;
				$asistencia->consecutivo = 0;
				$asistencia->insert();
			}
		} 
			
	}
	
	/* 
	 * Verifica si un accionista es delegado o no
	 *
	 * @return bool True si es delegado false en caso contrario
	 * @static
	 */
	function delegado($accionista_id)
	{
		$representante = new Representante;
		$representante->accionista_id = $accionista_id;
		
		return $representante->find(true);
	}
}
?>