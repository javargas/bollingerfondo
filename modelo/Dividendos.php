<?php
/**
 * Table Definition for dividendos
 */
require_once '../lib/DataObject.php';

class Dividendos extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'dividendos';                  // table name
    var $dividendos_id;               // int(11)  not_null primary_key auto_increment
    var $valor_dividendo;
    var $valor_nominal;
    var $valor_intrinseco;
    var $valorizacion;
    var $asamblea_id; 

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Dividendos',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>