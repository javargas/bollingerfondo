<?php
/**
 * Table Definition for estadoacciones
 */
require_once '../lib/DataObject.php';

class Estadoacciones extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'estadoacciones';                  // table name
    var $estadoacciones_id;               // int(11)  not_null primary_key auto_increment
    var $nombre;                          // string(35)  
    var $vender;                          // string(1)  

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Estadoacciones',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>