<?php
/**
 * Table Definition for Historico de acciones
 * 
 * En esta tabla se ingresan registros al inicio de cada agno. * 
 * Se utiliza para generar el certificado de accionistas 
 * 
 * @author John A. Vargas <javargas@gmail.com>
 * @date 2011-02-13
 */
require_once '../lib/DataObject.php';

class Historico_acciones extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'historico_acciones';                         // table name
    var $historico_id;                     
    var $accionista_id;                   
    var $cantidad;
    var $agno;

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Historico_asamblea',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>