<?php
/**
 * Table Definition for historicos
 */
require_once '../lib/DataObject.php';

class Historicos extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'historicos';                      // table name
	var $id_historico;
	var $usuario;
	var $fecha_hora;
	var $accionista_id;
	var $tipo_transaccion;
	var $campo_modificado;
	var $nuevo_valor;
	var $valor_anterior;

    /* Static get */
    function __clone() { return $this;}
	
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Historicos',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
