<?php
/**
 * Table Definition for movimiento
 */
require_once '../lib/DataObject.php';

class Movimiento extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'movimiento';                      // table name
    var $id;                              // int(50)  not_null primary_key auto_increment
    var $periodo_id;                      // string(6)  not_null
    var $consecutivo;                     // int(11)  not_null
    var $fecha;                           // date(10)  
    var $cantidad;                        // real(17)  
    var $valor;                           // real(17)  
    var $numdocumento;                    // string(10)  
    var $num_oferta;                      // int(11)  not_null
    var $accionista_id;                   // string(15)  not_null
    var $comprador;                       // string(15)  not_null
    var $observacion;                     // string(80)  
    var $cant_acept;                      // real(17)  
    var $acepta_ofrec;                    // string(2)  not_null

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Movimiento',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>