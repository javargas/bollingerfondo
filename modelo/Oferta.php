<?php
/**
 * Table Definition for oferta
 */
require_once '../lib/DataObject.php';

class Oferta extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'oferta';                          // table name
    var $num_oferta;                      // int(11)  not_null primary_key auto_increment
    var $periodo_id;                      // string(6)  not_null
    var $accionista_id;                   // int(20)  not_null
    var $fechaini;                        // date(10)  
    var $fechafin;                        // date(10)  
    var $cantidad;                        // real(17)  
    var $valor;                           // real(17)  
    var $tipo_pago;                       // int(11)  
    var $activo;                          // string(1)  

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Oferta',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>