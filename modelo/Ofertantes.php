<?php
/**
 * Table Definition for ofertantes
 */
require_once '../lib/DataObject.php';

class Ofertantes extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'ofertantes';                      // table name
    var $num_oferta;                      // int(11)  not_null primary_key
    var $accionista_id;                   // string(15)  not_null primary_key
    var $cantidad;                        // real(17)  
    var $valor;                           // real(17)  
    var $interesado;                      // string(1)  
    var $can_ofertada;                    // real(17)  

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Ofertantes',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>