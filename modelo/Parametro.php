<?php
/**
 * Table Definition for parametro
 */
require_once '../lib/DataObject.php';

class Parametro extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'parametro';     // table name
    var $parametro_id;              // int(11)  not_null primary_key auto_increment
    var $totalacciones;             // int(11)  not_null
    var $cuenta_credito_cd;         // int(11)  
    var $cuenta_debito_cd;          // string(6)  
    var $cuenta_credito_cg;         // int(11)  
    var $cuenta_debito_cg;          // string(100)  

    /* ZE2 compatibility trick*/
    function __clone() { return $this; }

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Parametro',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>