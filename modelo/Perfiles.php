<?php
/**
 * Table Definition for perfiles
 */
require_once '../lib/DataObject.php';

class Perfiles extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'perfiles';                        // table name
    var $id_perfil;                       // int(20)  not_null primary_key unsigned auto_increment
    var $nombre;                          // string(50)  
    var $permiso;                         // int(20)  unsigned

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Perfiles',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>