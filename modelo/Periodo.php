<?php
/**
 * Table Definition for periodo
 */
require_once '../lib/DataObject.php';

class Periodo extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'periodo';                         // table name
    var $periodo_id;                      // string(6)  not_null primary_key
    var $activo;                          // string(1)  
    var $estado;                          // string(1)  
    var $num_venta;                       // string(50)  
    var $radicacion_oferta;               // string(50)  
    var $radicacion_venta;                // string(50)  
    var $observaciones;                   // string(255) 
	var $fecha_inicio;
	var $fecha_fin; 

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Periodo',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>