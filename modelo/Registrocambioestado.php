<?php
/**
 * Table Definition for registrocambioestado
 */
require_once '../lib/DataObject.php';

class Registrocambioestado extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'registrocambioestado';            // table name
    var $fecha_registro;                  // date(10)  not_null
    var $fecha_documento;                 // date(10)  not_null
    var $entidad;                         // string(255)  not_null
    var $identificador_documento;         // string(50)  not_null
    var $demandante;                      // string(255)  not_null
    var $demandado_accionista;            // string(20)  not_null
    var $tipo_cambio_estado;              // int(11)  not_null
    var $cantidad_acciones;               // real(17)  not_null
    var $observaciones;                   // blob(16777215)  not_null blob
    var $id_registro;                     // int(11)  not_null primary_key auto_increment
    var $num_cedula;
    var $num_cuenta;
    var $nombre_banco;
    var $cuantia;
	var $login;

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Registrocambioestado',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>