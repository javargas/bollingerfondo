<?php
/**
 * Table Definition for representante
 */
require_once '../lib/DataObject.php';

class Representante extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'representante';                   // table name
    var $asamblea_id;                     // int(11)  not_null primary_key multiple_key
    var $representante;                   // string(15)  not_null primary_key
    var $accionista_id;                   // string(15)  not_null primary_key
    var $cantidad;                        // int(11)  
    var $participacion;                   // real(12)  

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Representante',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>