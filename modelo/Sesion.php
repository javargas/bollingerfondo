<?php
/**
 * Table Definition for sesion
 */
require_once '../lib/DataObject.php';

class Sesion extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'sesion';                          // table name
    var $asamblea_id;                     // int(11)  not_null primary_key
    var $accionista_id;                   // string(15)  not_null primary_key
    var $consecutivo;                     // real(6)  not_null primary_key multiple_key
    var $f_ingreso;                       // string(10)  
    var $h_ingreso;                       // string(12)  
    var $f_salida;                        // string(10)  
    var $h_salida;                        // string(12)  
    var $estado;                          // string(1)  

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Sesion',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>