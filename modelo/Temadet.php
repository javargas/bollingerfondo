<?php
/**
 * Table Definition for temadet
 */
require_once '../lib/DataObject.php';

class Temadet extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'temadet';                         // table name
    var $asamblea_id;                     // int(11)  not_null primary_key multiple_key
    var $tema_id;                         // int(20)  not_null primary_key
    var $can_candidatos;                  // real(5)  
    var $quorum;                          // real(12)  
    var $observacion;                     // string(100)  
    var $estado;                          // string(1)  

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Temadet',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>