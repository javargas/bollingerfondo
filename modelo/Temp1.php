<?php
/**
 * Table Definition for temp1
 */
require_once 'DB/DataObject.php';

class Temp1 extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'temp1';                           // table name

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Temp1',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
