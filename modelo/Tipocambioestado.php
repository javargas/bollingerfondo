<?php
/**
 * Table Definition for tipocambioestado
 */
require_once '../lib/DataObject.php';

class Tipocambioestado extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'tipocambioestado';                // table name
    var $tipo_cambio_estado_id;           // int(11)  not_null primary_key unsigned auto_increment
    var $tipo_cambio_estado;              // string(50)  not_null
    var $descripcion;                     // string(80)  
    var $resta_estado_accion;             // int(4)  not_null
    var $suma_estado_accion;              // int(4)  not_null

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Tipocambioestado',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>