<?php
/**
 * Table Definition for titulos
 */
require_once '../lib/DataObject.php';

class Titulos extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'titulos';                         // table name
    var $titulo_id;                       // int(20)  not_null primary_key auto_increment
    var $accionista_id;                   // string(50)  not_null
    var $numero_titulo;                   // string(50)  multiple_key
    var $cantidad;                        // real(13)  not_null
    var $fecha_emision;                   // date(10)  
    var $fecha_impresion;                 // date(10)  
    var $fecha_anulacion;                 // date(10)  
    var $estado;                          // string(1)  
    var $observaciones;                   // blob(65535)  blob
	var $periodo_id;                   	  // string(6)
	var $motivo;
	var $impreso;
	
    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Titulos',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>