<?php
/**
 * Table Definition for tmp
 */
require_once 'DB/DataObject.php';

class Tmp extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'tmp';                             // table name

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Tmp',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
