<?php
/**
 * Table Definition for usuario
 */
require_once '../lib/DataObject.php';

class Usuarioasam extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'usuarioasam';                         // table name
    var $id_usuario;                      // int(20)  not_null primary_key unsigned auto_increment
    var $login;                           // string(50)  
    var $passwd;                          // string(255)  
    var $nombre;                          // string(255)  
    var $perfil;                          // int(11)  

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Usuario',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>