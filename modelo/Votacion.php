<?php
/**
 * Table Definition for votacion
 */
require_once '../lib/DataObject.php';

class Votacion extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'votacion';                        // table name
    var $asamblea_id;                     // int(11)  not_null primary_key
    var $tema_id;                         // string(6)  not_null primary_key
    var $candidato_id;                    // int(20)  not_null primary_key multiple_key auto_increment
    var $nombre;                          // string(50)  
    var $votos;                           // int(11)  
    var $quorump;                         // real(12)  
    var $descripcion;                     // blob(65535)  blob

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Votacion',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>