<?php
/**
 * Table Definition for votantes
 */
require_once '../lib/DataObject.php';

class Votantes extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'votantes';                        // table name
    var $asamblea_id;                     // int(11)  not_null primary_key multiple_key
    var $tema_id;                         // string(6)  not_null primary_key
    var $accionista_id;                   // string(15)  not_null primary_key
    var $quorump;                         // real(12)  
    var $candidato_id;                    // int(20)  not_null unsigned

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('Votantes',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
?>