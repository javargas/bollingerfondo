-- Modificar el tamaño del campo direccion en la tabla accionista
-- Autor: John Vargas
-- Darte: Feb-21-2015

ALTER TABLE accionista MODIFY COLUMN direccion VARCHAR(500);

-- Permitir un quorum con dos numero enteros Ej: 50.2
ALTER TABLE tema MODIFY quorum DECIMAL(10,8);

DELETE FROM accionista WHERE clase = 'R';
DELETE FROM representante;

-- Mar 3 Crear la tabla de la cual se obtienen los ids para las asambleas.
CREATE TABLE asamblea_seq (
	id int(20) primary key  auto_increment
);